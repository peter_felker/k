using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace K
{
    /// <summary>
    /// A struktogram c�m�t megval�s�t� oszt�ly.
    /// </summary>
    public class Title : Element
    {

    #region Property-k, konstruktorok, (plusz a hozz�juk tartoz� f�ggv�nyek)

        /// <summary>
        /// Megadja, hogy ki van-e jel�lve a c�m, de val�j�ban
        /// a c�met nem lehet kijel�lni, �s pont ez�rt csak false �rt�kkel t�r vissza.
        /// </summary>
        public override bool Selected
        {
            get { return false; }
        }

        /// <summary>
        /// A Title oszt�ly konstruktora.
        /// </summary>
        public Title() : base()
        {
            defaultProperties();
        }

        /// <summary>
        /// Ezt a konstruktor szerializ�l�skor h�v�dik meg.
        /// </summary>
        /// <param name="text"> Az elem kezd� Text-je. </param>
        /// <param name="size"> Az elem kezd�m�rete. </param>
        /// <param name="location"> Az elem kezd�poz�ci�ja. </param>
        public Title(String text, Size size, Point location) : base(text, size, location)
        {
            defaultProperties();
        }

        /// <summary>
        /// A Title konstruktorainak k�z�s tulajdons�gait �ll�tja be ez az elj�r�s.
        /// </summary>
        private void defaultProperties()
        {
            this.TextChanged += new EventHandler(Title_TextChanged);
            this.MouseClick += new MouseEventHandler(Title_MouseClick);

            TabStop = false;
            Multiline = false;
            BorderStyle = BorderStyle.None;
            Margin = new Padding(3, 0, 3, 0);
        }

    #endregion

    #region M�retet megad� met�dusok

        /// <summary>
        /// A struktogram c�m�nek aj�nlott m�rete.
        /// </summary>
        public override Size RecommendedSize
        {
            get
            {
                Size preferredSize = this.PreferredSize;
                int prefAndMargWidth = preferredSize.Width + LeftRightMargin,
                    minWidth = Convert.ToInt32(Math.Round(Convert.ToInt32(this.Font.Size) * 7.5));

                return new Size(prefAndMargWidth < minWidth ? minWidth : prefAndMargWidth, preferredSize.Height);
            }
        }

    #endregion

    #region Egy�b esem�nykezel�k : TextChanged, MouseClick

        /// <summary>
        /// A c�m m�ret�t automatikusan megn�veli, ha nem f�r bele a sz�veg.
        /// </summary>
        protected void Title_TextChanged(object sender, EventArgs e)
        {
            Size recommendedSize = this.RecommendedSize;

            // M�retv�ltoz�sn�l �jrarendezz�k a struktogrammot
            if (recommendedSize != this.Size)
            {
                this.Size = new System.Drawing.Size(recommendedSize.Width, recommendedSize.Height);
                MainWindow.Stm.Alignment(this);
            }

            // Mikor besz�runk egy hosszab sz�veget a c�mbe, akkor a title csak egy r�sz�t
            // jelen�ti meg. Amint beleklikkel�nk, vagy b�rmi m�st tesz�nk, l�that�v� v�lik
            // az eg�sz sz�veg. Teh�t gyakorlatilag ez�rt van itt ez az egy jelent�ktelen utas�t�s.
            this.SelectionStart = this.SelectionStart;

            MainWindow.Stm.SaveState();
        }

        /// <summary>
        /// Ha eg�rrel a c�mre klikkel�nk, akkor vissza�ll�tja az
        /// �llapotot Normal-ra �s a kijel�l�seket visszavonja.
        /// </summary>
        private void Title_MouseClick(object sender, MouseEventArgs e)
        {
            MainWindow.State = MainWindowState.Normal;
            MainWindow.Stm.DeselectAll();
        }

    #endregion

    }
}
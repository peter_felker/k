using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace K
{
    /// <summary>
    /// A t�bb�g� el�gaz�st megval�s�t� oszt�ly
    /// </summary>
    public class MultipleBranch : AbstractBranch
    {

    #region Konstruktorok, (plusz a hozz�juk tartoz� f�ggv�nyek)

        /// <summary>
        /// A MultipleBranch konstruktora.
        /// </summary>
        public MultipleBranch(int branches) : base()
        {
            // Beletesz�nk annyi SimpleBranch elemet, amennyit megadtak a konstruktorban
            for (int i = 0; i < branches; ++i)
            {
                this.Controls.Add(new SimpleBranch());
            }
            defaultProperties();
        }

        /// <summary>
        /// Ezt a konstruktor szerializ�l�skor h�v�dik meg.
        /// </summary>
        /// <param name="size"> Az elem kezd�m�rete. </param>
        /// <param name="location"> Az elem kezd�poz�ci�ja. </param>
        public MultipleBranch(Size size, Point location) : base( "", size, location) { defaultProperties(); }

        /// <summary>
        /// A konstruktorok k�z�s tulajdons�gait �ll�tja be ez az elj�r�s.
        /// </summary>
        private void defaultProperties()
        {
            // Nem szeretn�nk, hogy a TAB billenty� lenyom�s�ra bele lehessen l�pni ebbe az elembe
            this.TabStop = false;
        }

    #endregion

    #region Kijel�l�s �s visszavon�sa

        /// <summary>
        /// Kijel�li az elemet.
        /// </summary>
        public override void Select()
        {
            base.Select();

            // Itt ki szeretn�nk jel�lni az �sszes SimpleBranch-et,
            // hogy ez jelezze, hogy ki van jel�lve a MultipleBranch
            foreach (ProgramConstruction pc in this.Controls)
            {
                pc.Select();
            }
        }

        /// <summary>
        /// Kijel�li az elemet rekurz�van.
        /// </summary>
        public override void SelectRecursively()
        {
            base.Select();

            // Itt ki szeretn�nk jel�lni az �sszes SimpleBranch-et �s amik benne vannak,
            // hogy ez jelezze, hogy rekurz�van ki van jel�lve a MultipleBranch
            foreach (ProgramConstruction pc in this.Controls)
            {
                pc.SelectRecursively();
            }
        }

        /// <summary>
        /// Visszavonja a kijel�l�st.
        /// </summary>
        public override void Deselect()
        {
            base.Deselect();

            // Visszavonjuk az �sszes SimpleBranch-t�l a kijel�l�st
            foreach (ProgramConstruction pc in this.Controls)
            {
                pc.Deselect();
            }
        }

        /// <summary>
        /// Visszavonja a kijel�l�st rekurz�van.
        /// </summary>
        public override void DeselectRecursively()
        {
            base.Deselect();

            // Visszavonjuk az �sszes SimpleBranch-t�l a kijel�l�st rekurz�van
            foreach (ProgramConstruction pc in this.Controls)
            {
                pc.DeselectRecursively();
            }
        }

    #endregion

    #region A z�ld sz�n� t�glalapok megjelen�t�s�vel kapcsolatos met�dusok, property-k

        /// <summary>
        /// Megadja, hogy a kurzor, az elem tetej�n (FELS� r�sz�ben) van-e.
        /// Vagyis megadn�, de ez a f�ggv�ny mindig HAMIS �rt�kkel fog visszat�rni,
        /// mert a MultipleBranch-et val�j�ban nem is l�tjuk, mert csak az�rt kell,
        /// hogy a SimpleBranch-eket �sszefogja.
        /// </summary>
        public override bool CursorInTheUpperRegion { get { return false; } }

        /// <summary>
        /// Megadja, hogy a kurzor, az elem alj�n (ALS� r�sz�ben) van-e.
        /// Vagyis megadn�, de ez a f�ggv�ny mindig HAMIS �rt�kkel fog visszat�rni,
        /// mert a MultipleBranch-et val�j�ban nem is l�tjuk, mert csak az�rt kell,
        /// hogy a SimpleBranch-eket �sszefogja.
        /// </summary>
        public override bool CursorInTheLowerRegion { get { return false; } }

        public override void CheckAndDrawRectangles()
        {
            ((SimpleBranch)this.Controls[0]).CheckAndDrawRectangles();
        }


        public override void RepaintRectangles(Graphics g)
        {
            if (Leaving == true)
            {
                foreach (SimpleBranch sb in this.Controls)
                {
                    sb.Leaving = true;
                    sb.RepaintRectangles(sb.CreateGraphics());
                }
            }
        }

        public override void HideRectangles()
        {
            ((SimpleBranch)this.Controls[0]).HideRectangles();
        }

    #endregion

    #region M�retet megad� met�dusok

        /// <summary>
        /// Megadja a programkonstrukci� aj�nlott m�ret�t.
        /// </summary>
        public override Size RecommendedSize
        {
            get
            {
                int recommendedWidth = 0, recommendedHeight = 0;
                Size recommendedSize;

                for (int i = 0; i < this.Controls.Count; ++i)
                {
                    recommendedSize = ((SimpleBranch)this.Controls[i]).RecommendedSize;

                    recommendedWidth += recommendedSize.Width - 1;

                    if (recommendedHeight < recommendedSize.Height)
                    {
                        recommendedHeight = recommendedSize.Height;
                    }
                }

                return new Size(recommendedWidth + 1, recommendedHeight);
            }
        }

        /// <summary>
        /// Megadja a MultipleBranch, SimpleBranch-einek maxim�lis magass�g�t.
        /// Ez az�rt kell mert egy MultipleBranch-ben minden felt�telnek azonos magass�g�nak kell lennie.
        /// </summary>
        public override int ConditionHeight
        {
            get
            {
                int maxConditionHeight = 0, conditionHeight;

                foreach (SimpleBranch sb in this.Controls)
                {
                    if (sb.Lines.Length > 0)
                    {
                        conditionHeight = sb.Lines.Length * sb.Font.Height + 8;
                    }
                    else
                    {
                        conditionHeight = sb.Font.Height + 8;
                    }

                    if (maxConditionHeight < conditionHeight)
                    {
                        maxConditionHeight = conditionHeight;
                    }
                }
                return maxConditionHeight;
            }
        }

    #endregion

    #region M�ret ellen�rz�s

        /// <summary>
        /// Ebben az oszt�lyban nem kell a m�retv�ltoz�sra figyelni.
        /// Megteszik a gyerek objektumok (SimpleBranch-ek).
        /// </summary>
        protected override void CheckSize(object sender, EventArgs e) { }

    #endregion

    #region Egy�b met�dusok

        /// <summary>
        /// Megadja, hogy h�ny sora van az aktu�lis elemnek. 
        /// </summary>
        /// <returns> A Text stringben a sorok sz�ma. </returns>
        public override int NumberOfLines
        {
            get
            {
                // Itt a SimpleBranch-ek maxim�lis felt�telmagass�g�ra vagyunk kiv�ncsiak
                int max = ((SimpleBranch)this.Controls[0]).NumberOfLines;

                for (int i = 1; i < this.Controls.Count; ++i)
                {
                    if (max < ((SimpleBranch)this.Controls[i]).NumberOfLines)
                    {
                        max = ((SimpleBranch)this.Controls[i]).NumberOfLines;
                    }
                }
                return max;
            }
        }

    #endregion

    }
}
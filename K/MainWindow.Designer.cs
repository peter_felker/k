namespace K
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.openMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.saveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAllMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toImageMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.latexMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.closeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeAllMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.exitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator15 = new System.Windows.Forms.ToolStripSeparator();
            this.textMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyTextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insertTextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutTextMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.insertElementMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.blockMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loopMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.branchMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.multipleBranchMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator16 = new System.Windows.Forms.ToolStripSeparator();
            this.selectElementMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.selectElementRecursivelyMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.moveModeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optimalSizeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resizeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteSelectedElementsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nezetMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showItemsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showEditMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showCategoryMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.basicMathSignsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.greekLettersMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.operatorsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arrowsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.negatedRelationSignsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.struktogramOptionsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemsStrip = new System.Windows.Forms.ToolStrip();
            this.newButton = new System.Windows.Forms.ToolStripButton();
            this.loadButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.saveButton = new System.Windows.Forms.ToolStripButton();
            this.saveAllButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.undoButton = new System.Windows.Forms.ToolStripButton();
            this.redoButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.closeAllButton = new System.Windows.Forms.ToolStripButton();
            this.closeButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.toImageButton = new System.Windows.Forms.ToolStripButton();
            this.latexButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.categoryLabel = new System.Windows.Forms.ToolStripLabel();
            this.categoryComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.categoryStrip = new System.Windows.Forms.ToolStrip();
            this.editStrip = new System.Windows.Forms.ToolStrip();
            this.blokkInsertButton = new System.Windows.Forms.ToolStripButton();
            this.cycleInsertButton = new System.Windows.Forms.ToolStripButton();
            this.branchInsertButton = new System.Windows.Forms.ToolStripButton();
            this.multipleBranchInsertButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator14 = new System.Windows.Forms.ToolStripSeparator();
            this.deleteButton = new System.Windows.Forms.ToolStripButton();
            this.resizeButton = new System.Windows.Forms.ToolStripButton();
            this.optimalSizeButton = new System.Windows.Forms.ToolStripButton();
            this.moveModeButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.stmTabControl = new K.StruktogramTabControl();
            this.menuStrip.SuspendLayout();
            this.itemsStrip.SuspendLayout();
            this.editStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenuItem,
            this.editMenuItem,
            this.nezetMenuItem,
            this.settingsMenuItem,
            this.helpMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.menuStrip.Size = new System.Drawing.Size(720, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "menuStrip1";
            // 
            // fileMenuItem
            // 
            this.fileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newMenuItem,
            this.toolStripSeparator1,
            this.openMenuItem,
            this.toolStripSeparator2,
            this.saveMenuItem,
            this.saveAsMenuItem,
            this.saveAllMenuItem,
            this.exportMenuItem,
            this.toolStripSeparator10,
            this.closeMenuItem,
            this.closeAllMenuItem,
            this.toolStripSeparator3,
            this.exitMenuItem});
            this.fileMenuItem.Name = "fileMenuItem";
            this.fileMenuItem.Size = new System.Drawing.Size(36, 20);
            this.fileMenuItem.Text = "Fájl";
            // 
            // newMenuItem
            // 
            this.newMenuItem.Name = "newMenuItem";
            this.newMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.newMenuItem.Size = new System.Drawing.Size(231, 22);
            this.newMenuItem.Text = "Új";
            this.newMenuItem.ToolTipText = "Új struktogram létrehozása";
            this.newMenuItem.Click += new System.EventHandler(this.newStruktogram);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(228, 6);
            // 
            // openMenuItem
            // 
            this.openMenuItem.Name = "openMenuItem";
            this.openMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openMenuItem.Size = new System.Drawing.Size(231, 22);
            this.openMenuItem.Text = "Betöltés";
            this.openMenuItem.ToolTipText = "Struktogram betöltése";
            this.openMenuItem.Click += new System.EventHandler(this.open);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(228, 6);
            // 
            // saveMenuItem
            // 
            this.saveMenuItem.Name = "saveMenuItem";
            this.saveMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveMenuItem.Size = new System.Drawing.Size(231, 22);
            this.saveMenuItem.Text = "Mentés";
            this.saveMenuItem.ToolTipText = "Struktogram elmentése";
            this.saveMenuItem.Click += new System.EventHandler(this.save);
            // 
            // saveAsMenuItem
            // 
            this.saveAsMenuItem.Name = "saveAsMenuItem";
            this.saveAsMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
                        | System.Windows.Forms.Keys.S)));
            this.saveAsMenuItem.Size = new System.Drawing.Size(231, 22);
            this.saveAsMenuItem.Text = "Mentés másként";
            this.saveAsMenuItem.ToolTipText = "Struktogram elmentése";
            this.saveAsMenuItem.Click += new System.EventHandler(this.saveAs);
            // 
            // saveAllMenuItem
            // 
            this.saveAllMenuItem.Name = "saveAllMenuItem";
            this.saveAllMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.S)));
            this.saveAllMenuItem.Size = new System.Drawing.Size(231, 22);
            this.saveAllMenuItem.Text = "Összes mentése";
            this.saveAllMenuItem.ToolTipText = "Összes struktogram elmentése";
            this.saveAllMenuItem.Click += new System.EventHandler(this.saveAll);
            // 
            // exportMenuItem
            // 
            this.exportMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toImageMenuItem,
            this.latexMenuItem});
            this.exportMenuItem.Name = "exportMenuItem";
            this.exportMenuItem.Size = new System.Drawing.Size(231, 22);
            this.exportMenuItem.Text = "Export...";
            // 
            // toImageMenuItem
            // 
            this.toImageMenuItem.Name = "toImageMenuItem";
            this.toImageMenuItem.Size = new System.Drawing.Size(164, 22);
            this.toImageMenuItem.Text = "... Képformátum";
            this.toImageMenuItem.ToolTipText = "Exportálás képformátumra";
            this.toImageMenuItem.Click += new System.EventHandler(this.saveToImage);
            // 
            // latexMenuItem
            // 
            this.latexMenuItem.Name = "latexMenuItem";
            this.latexMenuItem.Size = new System.Drawing.Size(164, 22);
            this.latexMenuItem.Text = "... LaTeX";
            this.latexMenuItem.ToolTipText = "Exportálás LaTeX formátumra";
            this.latexMenuItem.Click += new System.EventHandler(this.saveToLatex);
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(228, 6);
            // 
            // closeMenuItem
            // 
            this.closeMenuItem.Name = "closeMenuItem";
            this.closeMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
            this.closeMenuItem.Size = new System.Drawing.Size(231, 22);
            this.closeMenuItem.Text = "Bezárás";
            this.closeMenuItem.Click += new System.EventHandler(this.close);
            // 
            // closeAllMenuItem
            // 
            this.closeAllMenuItem.Name = "closeAllMenuItem";
            this.closeAllMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
                        | System.Windows.Forms.Keys.W)));
            this.closeAllMenuItem.Size = new System.Drawing.Size(231, 22);
            this.closeAllMenuItem.Text = "Összes bezárása";
            this.closeAllMenuItem.Click += new System.EventHandler(this.closeAll);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(228, 6);
            // 
            // exitMenuItem
            // 
            this.exitMenuItem.Name = "exitMenuItem";
            this.exitMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.exitMenuItem.Size = new System.Drawing.Size(231, 22);
            this.exitMenuItem.Text = "Kilépés";
            this.exitMenuItem.ToolTipText = "Kilépés a programból";
            this.exitMenuItem.Click += new System.EventHandler(this.exitMenuItem_Click);
            // 
            // editMenuItem
            // 
            this.editMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoMenuItem,
            this.redoMenuItem,
            this.toolStripSeparator15,
            this.textMenuItem,
            this.insertElementMenuItem,
            this.toolStripSeparator16,
            this.selectElementMenuItem,
            this.selectElementRecursivelyMenuItem,
            this.toolStripSeparator4,
            this.moveModeMenuItem,
            this.optimalSizeMenuItem,
            this.resizeMenuItem,
            this.deleteSelectedElementsMenuItem});
            this.editMenuItem.Name = "editMenuItem";
            this.editMenuItem.Size = new System.Drawing.Size(76, 20);
            this.editMenuItem.Text = "Szerkesztés";
            // 
            // undoMenuItem
            // 
            this.undoMenuItem.Name = "undoMenuItem";
            this.undoMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.undoMenuItem.Size = new System.Drawing.Size(396, 22);
            this.undoMenuItem.Text = "Visszavonás";
            this.undoMenuItem.Click += new System.EventHandler(this.undo);
            // 
            // redoMenuItem
            // 
            this.redoMenuItem.Name = "redoMenuItem";
            this.redoMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.redoMenuItem.Size = new System.Drawing.Size(396, 22);
            this.redoMenuItem.Text = "Újra";
            this.redoMenuItem.Click += new System.EventHandler(this.redo);
            // 
            // toolStripSeparator15
            // 
            this.toolStripSeparator15.Name = "toolStripSeparator15";
            this.toolStripSeparator15.Size = new System.Drawing.Size(393, 6);
            // 
            // textMenuItem
            // 
            this.textMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyTextMenuItem,
            this.insertTextMenuItem,
            this.cutTextMenuItem});
            this.textMenuItem.Name = "textMenuItem";
            this.textMenuItem.Size = new System.Drawing.Size(396, 22);
            this.textMenuItem.Text = "Szöveg ...";
            // 
            // copyTextMenuItem
            // 
            this.copyTextMenuItem.Name = "copyTextMenuItem";
            this.copyTextMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyTextMenuItem.Size = new System.Drawing.Size(187, 22);
            this.copyTextMenuItem.Text = "... Másolás";
            this.copyTextMenuItem.Click += new System.EventHandler(this.copyText);
            // 
            // insertTextMenuItem
            // 
            this.insertTextMenuItem.Name = "insertTextMenuItem";
            this.insertTextMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.insertTextMenuItem.Size = new System.Drawing.Size(187, 22);
            this.insertTextMenuItem.Text = "... Beillesztés";
            this.insertTextMenuItem.Click += new System.EventHandler(this.insertText);
            // 
            // cutTextMenuItem
            // 
            this.cutTextMenuItem.Name = "cutTextMenuItem";
            this.cutTextMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutTextMenuItem.Size = new System.Drawing.Size(187, 22);
            this.cutTextMenuItem.Text = "... Kivágás";
            this.cutTextMenuItem.Click += new System.EventHandler(this.cutText);
            // 
            // insertElementMenuItem
            // 
            this.insertElementMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.blockMenuItem,
            this.loopMenuItem,
            this.branchMenuItem,
            this.multipleBranchMenuItem});
            this.insertElementMenuItem.Name = "insertElementMenuItem";
            this.insertElementMenuItem.Size = new System.Drawing.Size(396, 22);
            this.insertElementMenuItem.Text = "Elem beszúrása ...";
            // 
            // blockMenuItem
            // 
            this.blockMenuItem.Name = "blockMenuItem";
            this.blockMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.B)));
            this.blockMenuItem.Size = new System.Drawing.Size(221, 22);
            this.blockMenuItem.Text = "... Blokk";
            this.blockMenuItem.Click += new System.EventHandler(this.blockInsert);
            // 
            // loopMenuItem
            // 
            this.loopMenuItem.Name = "loopMenuItem";
            this.loopMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.C)));
            this.loopMenuItem.Size = new System.Drawing.Size(221, 22);
            this.loopMenuItem.Text = "... Ciklus";
            this.loopMenuItem.Click += new System.EventHandler(this.loopInsert);
            // 
            // branchMenuItem
            // 
            this.branchMenuItem.Name = "branchMenuItem";
            this.branchMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.E)));
            this.branchMenuItem.Size = new System.Drawing.Size(221, 22);
            this.branchMenuItem.Text = "... Elágazás";
            this.branchMenuItem.Click += new System.EventHandler(this.branchInsert);
            // 
            // multipleBranchMenuItem
            // 
            this.multipleBranchMenuItem.Name = "multipleBranchMenuItem";
            this.multipleBranchMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.T)));
            this.multipleBranchMenuItem.Size = new System.Drawing.Size(221, 22);
            this.multipleBranchMenuItem.Text = "... Többágú elágazás";
            this.multipleBranchMenuItem.Click += new System.EventHandler(this.multipleBranchInsert);
            // 
            // toolStripSeparator16
            // 
            this.toolStripSeparator16.Name = "toolStripSeparator16";
            this.toolStripSeparator16.Size = new System.Drawing.Size(393, 6);
            // 
            // selectElementMenuItem
            // 
            this.selectElementMenuItem.Name = "selectElementMenuItem";
            this.selectElementMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt)
                        | System.Windows.Forms.Keys.Space)));
            this.selectElementMenuItem.Size = new System.Drawing.Size(396, 22);
            this.selectElementMenuItem.Text = "Aktuális elem kijelölése (visszavonása)";
            this.selectElementMenuItem.Click += new System.EventHandler(this.selectElement);
            // 
            // selectElementRecursivelyMenuItem
            // 
            this.selectElementRecursivelyMenuItem.Name = "selectElementRecursivelyMenuItem";
            this.selectElementRecursivelyMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.Space)));
            this.selectElementRecursivelyMenuItem.Size = new System.Drawing.Size(396, 22);
            this.selectElementRecursivelyMenuItem.Text = "Aktiális elem rekurzív kijelölése (visszavonása)";
            this.selectElementRecursivelyMenuItem.Click += new System.EventHandler(this.selectElementRecursively);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(393, 6);
            // 
            // moveModeMenuItem
            // 
            this.moveModeMenuItem.Name = "moveModeMenuItem";
            this.moveModeMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.M)));
            this.moveModeMenuItem.Size = new System.Drawing.Size(396, 22);
            this.moveModeMenuItem.Text = "Áthelyező mód";
            this.moveModeMenuItem.Click += new System.EventHandler(this.moveMode);
            // 
            // optimalSizeMenuItem
            // 
            this.optimalSizeMenuItem.Name = "optimalSizeMenuItem";
            this.optimalSizeMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.T)));
            this.optimalSizeMenuItem.Size = new System.Drawing.Size(396, 22);
            this.optimalSizeMenuItem.Text = "Optimális méret beállítása";
            this.optimalSizeMenuItem.Click += new System.EventHandler(this.optimalSize);
            // 
            // resizeMenuItem
            // 
            this.resizeMenuItem.Name = "resizeMenuItem";
            this.resizeMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.resizeMenuItem.Size = new System.Drawing.Size(396, 22);
            this.resizeMenuItem.Text = "Kijelölt elemek átméretezése";
            this.resizeMenuItem.Click += new System.EventHandler(this.resizeElements);
            // 
            // deleteSelectedElementsMenuItem
            // 
            this.deleteSelectedElementsMenuItem.Name = "deleteSelectedElementsMenuItem";
            this.deleteSelectedElementsMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.Delete)));
            this.deleteSelectedElementsMenuItem.Size = new System.Drawing.Size(396, 22);
            this.deleteSelectedElementsMenuItem.Text = "Kijelölt elemek törlése";
            this.deleteSelectedElementsMenuItem.Click += new System.EventHandler(this.deleteElements);
            // 
            // nezetMenuItem
            // 
            this.nezetMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showItemsMenuItem,
            this.showEditMenuItem,
            this.showCategoryMenuItem});
            this.nezetMenuItem.Name = "nezetMenuItem";
            this.nezetMenuItem.Size = new System.Drawing.Size(47, 20);
            this.nezetMenuItem.Text = "Nézet";
            // 
            // showItemsMenuItem
            // 
            this.showItemsMenuItem.Checked = true;
            this.showItemsMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showItemsMenuItem.Name = "showItemsMenuItem";
            this.showItemsMenuItem.Size = new System.Drawing.Size(142, 22);
            this.showItemsMenuItem.Text = "Eszközök";
            this.showItemsMenuItem.Click += new System.EventHandler(this.showItemsMenuItem_Click);
            // 
            // showEditMenuItem
            // 
            this.showEditMenuItem.Checked = true;
            this.showEditMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showEditMenuItem.Name = "showEditMenuItem";
            this.showEditMenuItem.Size = new System.Drawing.Size(142, 22);
            this.showEditMenuItem.Text = "Szerkesztés";
            this.showEditMenuItem.Click += new System.EventHandler(this.showEditMenuItem_Click);
            // 
            // showCategoryMenuItem
            // 
            this.showCategoryMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.basicMathSignsMenuItem,
            this.greekLettersMenuItem,
            this.operatorsMenuItem,
            this.arrowsMenuItem,
            this.negatedRelationSignsMenuItem});
            this.showCategoryMenuItem.Name = "showCategoryMenuItem";
            this.showCategoryMenuItem.Size = new System.Drawing.Size(142, 22);
            this.showCategoryMenuItem.Text = "Kategória";
            this.showCategoryMenuItem.Click += new System.EventHandler(this.categoryMenuItem_Click);
            // 
            // basicMathSignsMenuItem
            // 
            this.basicMathSignsMenuItem.Name = "basicMathSignsMenuItem";
            this.basicMathSignsMenuItem.Size = new System.Drawing.Size(213, 22);
            this.basicMathSignsMenuItem.Text = "Alapvető matematikai jelek";
            this.basicMathSignsMenuItem.Click += new System.EventHandler(this.basicMathSignsMenuItem_Click);
            // 
            // greekLettersMenuItem
            // 
            this.greekLettersMenuItem.Name = "greekLettersMenuItem";
            this.greekLettersMenuItem.Size = new System.Drawing.Size(213, 22);
            this.greekLettersMenuItem.Text = "Görög betűk";
            this.greekLettersMenuItem.Click += new System.EventHandler(this.greekLettersMenuItem_Click);
            // 
            // operatorsMenuItem
            // 
            this.operatorsMenuItem.Name = "operatorsMenuItem";
            this.operatorsMenuItem.Size = new System.Drawing.Size(213, 22);
            this.operatorsMenuItem.Text = "Operátorok";
            this.operatorsMenuItem.Click += new System.EventHandler(this.operatorsMenuItem_Click);
            // 
            // arrowsMenuItem
            // 
            this.arrowsMenuItem.Name = "arrowsMenuItem";
            this.arrowsMenuItem.Size = new System.Drawing.Size(213, 22);
            this.arrowsMenuItem.Text = "Nyilak";
            this.arrowsMenuItem.Click += new System.EventHandler(this.arrowsMenuItem_Click);
            // 
            // negatedRelationSignsMenuItem
            // 
            this.negatedRelationSignsMenuItem.Name = "negatedRelationSignsMenuItem";
            this.negatedRelationSignsMenuItem.Size = new System.Drawing.Size(213, 22);
            this.negatedRelationSignsMenuItem.Text = "Negált relációs jelek";
            this.negatedRelationSignsMenuItem.Click += new System.EventHandler(this.negatedRelationSignsMenuItem_Click);
            // 
            // settingsMenuItem
            // 
            this.settingsMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.struktogramOptionsMenuItem,
            this.optionsMenuItem});
            this.settingsMenuItem.Name = "settingsMenuItem";
            this.settingsMenuItem.Size = new System.Drawing.Size(69, 20);
            this.settingsMenuItem.Text = "Beállítások";
            // 
            // struktogramOptionsMenuItem
            // 
            this.struktogramOptionsMenuItem.Name = "struktogramOptionsMenuItem";
            this.struktogramOptionsMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.struktogramOptionsMenuItem.Size = new System.Drawing.Size(249, 22);
            this.struktogramOptionsMenuItem.Text = "Struktogram tulajdonságai";
            this.struktogramOptionsMenuItem.Click += new System.EventHandler(this.struktogramOptions);
            // 
            // optionsMenuItem
            // 
            this.optionsMenuItem.Name = "optionsMenuItem";
            this.optionsMenuItem.Size = new System.Drawing.Size(249, 22);
            this.optionsMenuItem.Text = "Opciók";
            this.optionsMenuItem.Click += new System.EventHandler(this.optionsMenuItem_Click);
            // 
            // helpMenuItem
            // 
            this.helpMenuItem.Name = "helpMenuItem";
            this.helpMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.helpMenuItem.Size = new System.Drawing.Size(66, 20);
            this.helpMenuItem.Text = "Súgó (F1)";
            this.helpMenuItem.Click += new System.EventHandler(this.helpMenuItem_Click);
            // 
            // itemsStrip
            // 
            this.itemsStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newButton,
            this.loadButton,
            this.toolStripSeparator9,
            this.saveButton,
            this.saveAllButton,
            this.toolStripSeparator5,
            this.undoButton,
            this.redoButton,
            this.toolStripSeparator11,
            this.closeAllButton,
            this.closeButton,
            this.toolStripSeparator6,
            this.toImageButton,
            this.latexButton,
            this.toolStripSeparator7,
            this.categoryLabel,
            this.categoryComboBox,
            this.toolStripSeparator8});
            this.itemsStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.itemsStrip.Location = new System.Drawing.Point(0, 24);
            this.itemsStrip.Name = "itemsStrip";
            this.itemsStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.itemsStrip.Size = new System.Drawing.Size(720, 31);
            this.itemsStrip.TabIndex = 1;
            this.itemsStrip.Text = "Eszközök";
            // 
            // newButton
            // 
            this.newButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newButton.Image = global::K.Properties.Resources._new;
            this.newButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.newButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newButton.Name = "newButton";
            this.newButton.Size = new System.Drawing.Size(23, 28);
            this.newButton.Text = "Új struktogram";
            this.newButton.Click += new System.EventHandler(this.newStruktogram);
            // 
            // loadButton
            // 
            this.loadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.loadButton.Image = global::K.Properties.Resources.load;
            this.loadButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.loadButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.loadButton.Name = "loadButton";
            this.loadButton.Size = new System.Drawing.Size(29, 28);
            this.loadButton.Text = "Struktogram betöltése";
            this.loadButton.Click += new System.EventHandler(this.open);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 31);
            // 
            // saveButton
            // 
            this.saveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveButton.Image = global::K.Properties.Resources.save;
            this.saveButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(23, 28);
            this.saveButton.Text = "Struktogram elmentése";
            this.saveButton.Click += new System.EventHandler(this.save);
            // 
            // saveAllButton
            // 
            this.saveAllButton.AutoSize = false;
            this.saveAllButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveAllButton.Image = ((System.Drawing.Image)(resources.GetObject("saveAllButton.Image")));
            this.saveAllButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveAllButton.Name = "saveAllButton";
            this.saveAllButton.Size = new System.Drawing.Size(23, 28);
            this.saveAllButton.Text = "Összes mentése";
            this.saveAllButton.Click += new System.EventHandler(this.saveAll);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 31);
            // 
            // undoButton
            // 
            this.undoButton.BackColor = System.Drawing.SystemColors.Control;
            this.undoButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.undoButton.Image = global::K.Properties.Resources.undo;
            this.undoButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.undoButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.undoButton.Name = "undoButton";
            this.undoButton.Size = new System.Drawing.Size(23, 28);
            this.undoButton.Text = "Visszavonás";
            this.undoButton.Click += new System.EventHandler(this.undo);
            // 
            // redoButton
            // 
            this.redoButton.BackColor = System.Drawing.SystemColors.Control;
            this.redoButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.redoButton.Image = global::K.Properties.Resources.redo;
            this.redoButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.redoButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.redoButton.Name = "redoButton";
            this.redoButton.Size = new System.Drawing.Size(23, 28);
            this.redoButton.Text = "Újra";
            this.redoButton.Click += new System.EventHandler(this.redo);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(6, 31);
            // 
            // closeAllButton
            // 
            this.closeAllButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.closeAllButton.AutoSize = false;
            this.closeAllButton.BackColor = System.Drawing.Color.Red;
            this.closeAllButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.closeAllButton.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.closeAllButton.ForeColor = System.Drawing.Color.White;
            this.closeAllButton.Image = ((System.Drawing.Image)(resources.GetObject("closeAllButton.Image")));
            this.closeAllButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.closeAllButton.Name = "closeAllButton";
            this.closeAllButton.Size = new System.Drawing.Size(22, 22);
            this.closeAllButton.Text = "X";
            this.closeAllButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.closeAllButton.ToolTipText = "Összes struktogram bezárása";
            this.closeAllButton.Click += new System.EventHandler(this.closeAll);
            // 
            // closeButton
            // 
            this.closeButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.closeButton.AutoSize = false;
            this.closeButton.BackColor = System.Drawing.Color.White;
            this.closeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.closeButton.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.closeButton.ForeColor = System.Drawing.Color.Red;
            this.closeButton.Image = ((System.Drawing.Image)(resources.GetObject("closeButton.Image")));
            this.closeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(22, 22);
            this.closeButton.Text = "X";
            this.closeButton.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.closeButton.ToolTipText = "Aktuális struktogram bezárása";
            this.closeButton.Click += new System.EventHandler(this.close);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 31);
            // 
            // toImageButton
            // 
            this.toImageButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toImageButton.Image = global::K.Properties.Resources.to_png;
            this.toImageButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toImageButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toImageButton.Name = "toImageButton";
            this.toImageButton.Size = new System.Drawing.Size(23, 28);
            this.toImageButton.Text = "Exportálás képformátumra";
            this.toImageButton.ToolTipText = "Exportálás képformátumra";
            this.toImageButton.Click += new System.EventHandler(this.saveToImage);
            // 
            // latexButton
            // 
            this.latexButton.BackColor = System.Drawing.SystemColors.Control;
            this.latexButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.latexButton.Image = global::K.Properties.Resources.latex;
            this.latexButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.latexButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.latexButton.Name = "latexButton";
            this.latexButton.Size = new System.Drawing.Size(45, 28);
            this.latexButton.Text = "Exportálás LaTeX nyelvre";
            this.latexButton.Click += new System.EventHandler(this.saveToLatex);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 31);
            // 
            // categoryLabel
            // 
            this.categoryLabel.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.categoryLabel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.categoryLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.categoryLabel.Name = "categoryLabel";
            this.categoryLabel.Size = new System.Drawing.Size(53, 28);
            this.categoryLabel.Text = "Kategória";
            // 
            // categoryComboBox
            // 
            this.categoryComboBox.Items.AddRange(new object[] {
            "(üres)",
            "Alapvető matematikai jelek",
            "Görög betűk",
            "Operátorok",
            "Nyilak",
            "Negált relációs jelek"});
            this.categoryComboBox.Name = "categoryComboBox";
            this.categoryComboBox.Size = new System.Drawing.Size(155, 31);
            this.categoryComboBox.Text = "(üres)";
            this.categoryComboBox.SelectedIndexChanged += new System.EventHandler(this.categoryComboBox_SelectedIndexChanged);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 31);
            // 
            // categoryStrip
            // 
            this.categoryStrip.Font = new System.Drawing.Font("Arial Unicode MS", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.categoryStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.categoryStrip.Location = new System.Drawing.Point(0, 55);
            this.categoryStrip.Name = "categoryStrip";
            this.categoryStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.categoryStrip.Size = new System.Drawing.Size(553, 25);
            this.categoryStrip.TabIndex = 3;
            this.categoryStrip.Text = "toolStrip1";
            this.categoryStrip.Visible = false;
            // 
            // editStrip
            // 
            this.editStrip.AutoSize = false;
            this.editStrip.Dock = System.Windows.Forms.DockStyle.Left;
            this.editStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.blokkInsertButton,
            this.cycleInsertButton,
            this.branchInsertButton,
            this.multipleBranchInsertButton,
            this.toolStripSeparator14,
            this.deleteButton,
            this.resizeButton,
            this.optimalSizeButton,
            this.moveModeButton,
            this.toolStripSeparator12,
            this.toolStripSeparator13});
            this.editStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.editStrip.Location = new System.Drawing.Point(0, 55);
            this.editStrip.Name = "editStrip";
            this.editStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.editStrip.Size = new System.Drawing.Size(42, 361);
            this.editStrip.Stretch = true;
            this.editStrip.TabIndex = 6;
            this.editStrip.Text = "Szerkesztés";
            // 
            // blokkInsertButton
            // 
            this.blokkInsertButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.blokkInsertButton.ForeColor = System.Drawing.SystemColors.ControlText;
            this.blokkInsertButton.Image = global::K.Properties.Resources.block;
            this.blokkInsertButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.blokkInsertButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.blokkInsertButton.Name = "blokkInsertButton";
            this.blokkInsertButton.Size = new System.Drawing.Size(40, 13);
            this.blokkInsertButton.Text = "Blokk beszúrása";
            this.blokkInsertButton.Click += new System.EventHandler(this.blockInsert);
            // 
            // cycleInsertButton
            // 
            this.cycleInsertButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cycleInsertButton.Image = global::K.Properties.Resources.cycle;
            this.cycleInsertButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.cycleInsertButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cycleInsertButton.Name = "cycleInsertButton";
            this.cycleInsertButton.Size = new System.Drawing.Size(40, 13);
            this.cycleInsertButton.Text = "Ciklus beszúrása";
            this.cycleInsertButton.Click += new System.EventHandler(this.loopInsert);
            // 
            // branchInsertButton
            // 
            this.branchInsertButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.branchInsertButton.Image = global::K.Properties.Resources._if;
            this.branchInsertButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.branchInsertButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.branchInsertButton.Name = "branchInsertButton";
            this.branchInsertButton.Size = new System.Drawing.Size(40, 13);
            this.branchInsertButton.Text = "Elágazás beszúrása";
            this.branchInsertButton.Click += new System.EventHandler(this.branchInsert);
            // 
            // multipleBranchInsertButton
            // 
            this.multipleBranchInsertButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.multipleBranchInsertButton.Image = global::K.Properties.Resources._case;
            this.multipleBranchInsertButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.multipleBranchInsertButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.multipleBranchInsertButton.Name = "multipleBranchInsertButton";
            this.multipleBranchInsertButton.Size = new System.Drawing.Size(40, 13);
            this.multipleBranchInsertButton.Text = "Többágú elágazás beszúrása";
            this.multipleBranchInsertButton.Click += new System.EventHandler(this.multipleBranchInsert);
            // 
            // toolStripSeparator14
            // 
            this.toolStripSeparator14.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator14.Name = "toolStripSeparator14";
            this.toolStripSeparator14.Size = new System.Drawing.Size(40, 6);
            // 
            // deleteButton
            // 
            this.deleteButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.deleteButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deleteButton.Image = global::K.Properties.Resources.x;
            this.deleteButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.deleteButton.Margin = new System.Windows.Forms.Padding(0, 1, 0, 20);
            this.deleteButton.Name = "deleteButton";
            this.deleteButton.Size = new System.Drawing.Size(40, 20);
            this.deleteButton.Text = "Kijelölt elemek törlése";
            this.deleteButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            this.deleteButton.Click += new System.EventHandler(this.deleteElements);
            // 
            // resizeButton
            // 
            this.resizeButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.resizeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.resizeButton.Image = global::K.Properties.Resources.resize;
            this.resizeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.resizeButton.Name = "resizeButton";
            this.resizeButton.Size = new System.Drawing.Size(40, 20);
            this.resizeButton.Text = "Átméretezés";
            this.resizeButton.ToolTipText = "Kijelölt elem(ek) átméretezése";
            this.resizeButton.Click += new System.EventHandler(this.resizeElements);
            // 
            // optimalSizeButton
            // 
            this.optimalSizeButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.optimalSizeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.optimalSizeButton.Image = global::K.Properties.Resources.optimal_size;
            this.optimalSizeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.optimalSizeButton.Name = "optimalSizeButton";
            this.optimalSizeButton.Size = new System.Drawing.Size(40, 20);
            this.optimalSizeButton.Text = "Optimális méret";
            this.optimalSizeButton.Click += new System.EventHandler(this.optimalSize);
            // 
            // moveModeButton
            // 
            this.moveModeButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.moveModeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.moveModeButton.Image = global::K.Properties.Resources.finger_hand_icon_cursor;
            this.moveModeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.moveModeButton.Name = "moveModeButton";
            this.moveModeButton.Size = new System.Drawing.Size(40, 20);
            this.moveModeButton.Text = "Áthelyező mód";
            this.moveModeButton.ToolTipText = "Áthelyező mód";
            this.moveModeButton.Click += new System.EventHandler(this.moveMode);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(40, 6);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(40, 6);
            // 
            // stmTabControl
            // 
            this.stmTabControl.AllowDrop = true;
            this.stmTabControl.Location = new System.Drawing.Point(45, 58);
            this.stmTabControl.Name = "stmTabControl";
            this.stmTabControl.SelectedIndex = 0;
            this.stmTabControl.Size = new System.Drawing.Size(675, 358);
            this.stmTabControl.TabIndex = 7;
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ClientSize = new System.Drawing.Size(720, 416);
            this.Controls.Add(this.stmTabControl);
            this.Controls.Add(this.editStrip);
            this.Controls.Add(this.categoryStrip);
            this.Controls.Add(this.itemsStrip);
            this.Controls.Add(this.menuStrip);
            this.Location = new System.Drawing.Point(1000, 400);
            this.MainMenuStrip = this.menuStrip;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "K";
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.MainWindow_MouseClick);
            this.SizeChanged += new System.EventHandler(this.MainWindow_SizeChanged);
            this.Shown += new System.EventHandler(this.MainWindow_Shown);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.itemsStrip.ResumeLayout(false);
            this.itemsStrip.PerformLayout();
            this.editStrip.ResumeLayout(false);
            this.editStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStripMenuItem fileMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem toImageMenuItem;
        private System.Windows.Forms.ToolStripMenuItem latexMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem exitMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem nezetMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showItemsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showCategoryMenuItem;
        private System.Windows.Forms.ToolStripMenuItem greekLettersMenuItem;
        private System.Windows.Forms.ToolStripMenuItem operatorsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem arrowsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem negatedRelationSignsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem basicMathSignsMenuItem;
        private System.Windows.Forms.ToolStripButton saveButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton undoButton;
        private System.Windows.Forms.ToolStripButton redoButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripComboBox categoryComboBox;
        private System.Windows.Forms.ToolStripLabel categoryLabel;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripButton latexButton;
        private System.Windows.Forms.ToolStripButton toImageButton;
        private System.Windows.Forms.ToolStripButton newButton;
        private System.Windows.Forms.ToolStripButton loadButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        public System.Windows.Forms.MenuStrip menuStrip;
        public System.Windows.Forms.ToolStrip itemsStrip;
        private System.Windows.Forms.ToolStrip categoryStrip;
        private System.Windows.Forms.ToolStrip editStrip;
        private System.Windows.Forms.ToolStripButton blokkInsertButton;
        private System.Windows.Forms.ToolStripButton cycleInsertButton;
        private System.Windows.Forms.ToolStripButton branchInsertButton;
        private System.Windows.Forms.ToolStripButton multipleBranchInsertButton;
        private System.Windows.Forms.ToolStripButton deleteButton;
        private System.Windows.Forms.ToolStripButton resizeButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator10;
        private System.Windows.Forms.ToolStripMenuItem closeMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeAllMenuItem;
        private System.Windows.Forms.ToolStripButton moveModeButton;
        private System.Windows.Forms.ToolStripButton closeButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator11;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator14;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator12;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator13;
        private System.Windows.Forms.ToolStripMenuItem saveAllMenuItem;
        private System.Windows.Forms.ToolStripButton saveAllButton;
        private System.Windows.Forms.ToolStripButton closeAllButton;
        private System.Windows.Forms.ToolStripMenuItem textMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyTextMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insertTextMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutTextMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator15;
        private System.Windows.Forms.ToolStripMenuItem moveModeMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resizeMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteSelectedElementsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem insertElementMenuItem;
        private System.Windows.Forms.ToolStripMenuItem blockMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loopMenuItem;
        private System.Windows.Forms.ToolStripMenuItem multipleBranchMenuItem;
        private System.Windows.Forms.ToolStripMenuItem branchMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showEditMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem struktogramOptionsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpMenuItem;
        private StruktogramTabControl stmTabControl;
        private System.Windows.Forms.ToolStripButton optimalSizeButton;
        private System.Windows.Forms.ToolStripMenuItem optimalSizeMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator16;
        private System.Windows.Forms.ToolStripMenuItem selectElementMenuItem;
        private System.Windows.Forms.ToolStripMenuItem selectElementRecursivelyMenuItem;
    }
}
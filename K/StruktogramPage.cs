using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace K
{
    /// <summary>
    /// Egy olyan TabPage, melyen egy struktogram van.
    /// </summary>
    public class StruktogramPage : TabPage
    {

    #region V�ltoz�k, property-k, konstruktor

        private Struktogram stm;
        private String fileName, tmpFilesDirectory;
        private bool modified;
        private LatexOptions oldLatexOptions;
        private String oldLatexFileName;

        /// <summary>
        /// Megadja, vagy be�ll�tja az el�z� Latex opci�kat.
        /// </summary>
        public LatexOptions OldLatexOptions
        {
            get { return oldLatexOptions; }
            set { oldLatexOptions = value; }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja azt az el�r�si utat,
        /// ahova el�z�leg mentett�k a Latex f�jlt.
        /// </summary>
        public String OldLatexFileName
        {
            get { return oldLatexFileName; }
            set { oldLatexFileName = value; }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja a TabPage-en l�v� struktogrammot.
        /// </summary>
        public Struktogram Stm
        {
            get { return stm; }
            set
            {
                if (stm != null) this.Controls.Remove(stm);
                stm = value;
                this.Controls.Add(stm);
                stm.Show();
            }
        }

        /// <summary>
        /// Megadja vagy be�ll�tja az el�r�si utat, ahova a struktogrammot menteni szeretn�nk.
        /// </summary>
        public String FileName
        {
            get { return fileName; }
            set
            {
                fileName = value;
                if (fileName != String.Empty)
                {
                    this.Text = fileName.Substring(fileName.LastIndexOf("\\") + 1);
                }
                else
                {
                    this.Text = "(n�vtelen)";
                }
            }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja a k�nyvt�rat, ahova a visszal�p�shez
        /// sz�ks�ges f�jlok vannak.
        /// </summary>
        public String TmpFilesDirectory
        {
            get { return tmpFilesDirectory; }
            set { tmpFilesDirectory = value; }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja, hogy t�rt�nt-e v�ltoz�s a StruktogramPage-en
        /// az el�z� ment�s, vagy bet�lt�s �ta.
        /// </summary>
        public bool Modified
        {
            get { return modified; }
            set
            {
                if (modified != value)
                {
                    modified = value;
                    if (modified)
                    {
                        this.Text += " *";
                    }
                    else
                    {
                        this.Text = FileName.Substring(FileName.LastIndexOf("\\") + 1);
                    }
                }
            }
        }

        /// <summary>
        /// A StruktogramPage konstruktora.
        /// </summary>
        public StruktogramPage()
        {
            int filesDirectoryCounter;

            this.MouseClick += new MouseEventHandler(StruktogramPage_MouseClick);
            this.MouseWheel += new MouseEventHandler(Scroll);

            this.Modified = false;
            this.AutoScroll = true;
            this.BackColor = Color.White;
            this.fileName = String.Empty;

            // Keres�nk egy olyan k�nyvt�rat, mely m�g nem l�tezik.
            // A k�nyv�trakat sz�mmal jel�lj�k, �s 1-t�l kezd�dik a sz�moz�suk.
            for (filesDirectoryCounter = 1; Directory.Exists(MainWindow.ProgramDirectory + "tmp\\" + filesDirectoryCounter.ToString()); ++filesDirectoryCounter);

            // Az els�nek megtal�lt, nem l�tez� k�nyvt�r lesz az �j mappa, melybe beletessz�k az ideiglenes f�jlokat.
            this.TmpFilesDirectory = MainWindow.ProgramDirectory + "tmp\\" + filesDirectoryCounter.ToString();
            Directory.CreateDirectory(this.TmpFilesDirectory);
            OldLatexOptions = null;
            OldLatexFileName = null;
        }

    #endregion

    #region F�jl m�veletek

        /// <summary>
        /// Bez�rja az aktu�lis struktogrammot.
        /// </summary>
        /// <returns> Megadja, hogy kell-e m�g tov�bbi struktogrammokat bez�rni.
        ///           (Pl. a felhaszn�l� r�kattintott a "Mindegyik bez�r�sa" gombra.) </returns>
        public bool CloseStruktogram()
        {
            ((TabControl)Parent).SelectedIndex = ((TabControl)Parent).TabPages.IndexOf(this);

            // Ha m�dosult valamit a struktogram, akkor el�sz�r megk�rdezz�k a felhaszn�l�t�l,
            // hogy nem szeretn�-e elmenteni a struktogrammot
            if (Modified)
            {
                ConfirmationWindow window = new ConfirmationWindow();

                // El�hozunk egy dial�gusablakot
                window.ShowDialog();

                // Igen eset�n elmentj�k a struktogrammot
                if (window.DialogResult == DialogResult.Yes)
                {
                    SaveStruktogram();
                }

                // Ha Igen vagy Nem a DialogResult, akkor be kell z�rni a Struktogrammot 
                if (window.DialogResult == DialogResult.Yes || window.DialogResult == DialogResult.No)
                {
                    try // T�r�lj�k a struktogramhoz tartoz� ideiglenes k�nyvt�rat
                    {
                        Directory.Delete(this.TmpFilesDirectory, true);
                    }
                    catch (Exception ex) { } // Ha kiv�tel l�p fel akkor az minket nem �rdekel
                    this.Controls.Clear();
                }

                // Ha m�gs�re kattintottunk, akkor elk�pzelhet�, hogy le kell �llni a bez�r�sokkal.
                // Ez akkor ford�l el�, ha a felhaszn�l� r�kattintott a "Mindegyik bez�r�sa" gombra
                return window.DialogResult == DialogResult.Cancel ? true : false;

            }
            else // Ha nem m�dosult semmit a struktogram, az el�z� ment�s �ta, akkor csak sim�n elt�ntetj�k a struktogrammot
            {
                try
                {
                    Directory.Delete(this.TmpFilesDirectory, true);
                }
                catch (Exception ex) { }
                this.Controls.Clear();
                return false;
            }
        }

        /// <summary>
        /// A StruktogramPage-en l�v� Struktogram elment�se m�sk�nt.
        /// </summary>
        public void SaveStruktogramAs()
        {
            SaveFileDialog dialog = new SaveFileDialog();

            dialog.Filter = "K kiterjeszt�s� f�jlok (*.k)|*.k|" +
                            "�sszes f�jl|*.*";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                String path = (dialog.FileName + (!(dialog.FileName.EndsWith(".k") || dialog.FileName.EndsWith(".K")) ? ".k" : ""));

                Stm.SaveState(path);
                FileName = path;
                Modified = false;
            }
        }

        /// <summary>
        /// A StruktogramPage-en l�v� Struktogram elment�se.
        /// </summary>
        public void SaveStruktogram()
        {
            // Ha a f�jlt m�r egyszer elmentett�k, akkor ugyanoda ment�nk
            if (FileName != String.Empty)
            {
                Stm.SaveState(FileName);
                Modified = false;
            }
            else // Ha m�g nem volt elmentve a f�jl, akkor megh�vjuk a SaveStruktogramAs() f�ggv�nyt
            {
                ((TabControl)Parent).SelectedIndex = ((TabControl)Parent).TabPages.IndexOf(this);
                SaveStruktogramAs();
            }
        }

    #endregion

    #region Egy�b esem�nykezel�k : MouseClick, Scroll

        /// <summary>
        /// Ha az aktu�lis TabPage-re kattintottunk, akkor vissza�ll�tjuk az �llapotot Normal-ra.
        /// </summary>
        private void StruktogramPage_MouseClick(object sender, MouseEventArgs e)
        {
            MainWindow.State = MainWindowState.Normal;
            MainWindow.Stm.DeselectAll();
        }

        /// <summary>
        /// Megval�s�tja a g�rget�st.
        /// Az�rt static, mert m�s oszt�ly is ezt h�vja meg.
        /// </summary>
        public static void Scroll(object sender, MouseEventArgs e)
        {
            int step = 50;

            if (e.Delta < 0)
            {
                // Lefele g�rget�s
                ((StruktogramPage)sender).AutoScrollPosition = new Point(0 - ((StruktogramPage)sender).AutoScrollPosition.X, step - ((StruktogramPage)sender).AutoScrollPosition.Y);
            }
            else
            {
                // Felfele g�rget�s
                ((StruktogramPage)sender).AutoScrollPosition = new Point(0 - ((StruktogramPage)sender).AutoScrollPosition.X, -step - ((StruktogramPage)sender).AutoScrollPosition.Y);
            }
        }

    #endregion

    }
}

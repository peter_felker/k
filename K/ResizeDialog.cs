using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace K
{
    /// <summary>
    /// Ebben a dial�gusablakban �ll�thatja be a felhaszn�l�
    /// a kiv�lasztott programkonstrukci�k m�ret�t.
    /// </summary>
    public partial class ResizeDialog : Form
    {

    #region V�ltoz�k, property-k, konstruktor

        private object parent;

        /// <summary>
        /// Megadja, vagy be�ll�tja a sz�l� objektumot.
        /// </summary>
        public object Parent
        {
            get { return parent; }
            set { parent = value; }
        }

        /// <summary>
        /// Megadja a be�ll�tott m�retet.
        /// </summary>
        public Size AdjustedSize
        {
            get { return new Size(Convert.ToInt32(widthNumericUpDown.Value), Convert.ToInt32(heightNumericUpDown.Value)); }
        }

        public ResizeDialog()
        {
            InitializeComponent();

            Size maxSize = getMaxSizeOfSelectedItems();
            widthNumericUpDown.Minimum = getMaxRecommendedWidthOfSelectedItems();
            heightNumericUpDown.Minimum = getMaxRecommendedHeightOfSelectedItems();

            widthNumericUpDown.Value = maxSize.Width;
            heightNumericUpDown.Value = maxSize.Height;
        }

    #endregion

    #region Maxim�lis �s aj�nlott m�ret kisz�m�t�sa

        /// <summary>
        /// Megadja a kijel�lt elemek m�ret�nek a maximum�t.
        /// </summary>
        /// <returns> A kijel�lt elemek k�z�l, a maxim�lis m�ret�. </returns>
        private Size getMaxSizeOfSelectedItems()
        {
            Size maxSize;
            List<Element> list = MainWindow.Stm.SelectedElements;

            maxSize = list[0].Size;

            foreach(Element e in list)
            {
                if ( (e.Size.Width*e.Size.Height) > (maxSize.Width*maxSize.Height) )
                {
                    maxSize = e.Size;
                }
            }

            return maxSize;
        }

        /// <summary>
        /// Megadja a kijel�lt elemek aj�nlott sz�less�g�nek a maximum�t.
        /// </summary>
        /// <returns> A kijel�lt elemek k�z�l, a maxim�lis aj�nlott sz�less�g�. </returns>
        private int getMaxRecommendedWidthOfSelectedItems()
        {
            int maxRecommendedWidth;
            List<Element> list = MainWindow.Stm.SelectedElements;

            maxRecommendedWidth = list[0].RecommendedSize.Width;

            foreach (Element e in list)
            {
                if (e.RecommendedSize.Width > maxRecommendedWidth )
                {
                    maxRecommendedWidth = e.RecommendedSize.Width;
                }
            }
            return maxRecommendedWidth;
        }

        /// <summary>
        /// Megadja a kijel�lt elemek aj�nlott magass�g�nak a maximum�t.
        /// </summary>
        /// <returns> A kijel�lt elemek k�z�l, a maxim�lis aj�nlott magass�g�. </returns>
        private int getMaxRecommendedHeightOfSelectedItems()
        {
            int maxRecommendedHeight;
            List<Element> list = MainWindow.Stm.SelectedElements;

            maxRecommendedHeight = list[0].RecommendedSize.Height;

            foreach (Element e in list)
            {
                if (e.RecommendedSize.Height > maxRecommendedHeight)
                {
                    maxRecommendedHeight = e.RecommendedSize.Height;
                }
            }
            return maxRecommendedHeight;
        }

    #endregion

    }
}
using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace K
{
    /// <summary>
    /// Az �sszes olyan programkonstrukci� �soszt�lya, melynek t�bb eleme is lehet.
    /// Ilyen a ciklus, el�gaz�s, �s t�bb�g� el�gaz�s.
    /// </summary>
    public abstract class ProgramConstructionWithCondition : ProgramConstruction
    {

    #region Konstruktorok, (plusz a hozz�juk tartoz� f�ggv�nyek)

        /// <summary>
        /// A ProgramConstructionWithCondition konstruktora.
        /// </summary>
        public ProgramConstructionWithCondition() : base()
        {
            defaultProperties();
        }

        /// <summary>
        /// Ezt a konstruktor szerializ�l�skor h�v�dik meg.
        /// </summary>
        /// <param name="text"> Az elem kezd� Text-je. </param>
        /// <param name="size"> Az elem kezd�m�rete. </param>
        /// <param name="location"> Az elem kezd�poz�ci�ja. </param>
        public ProgramConstructionWithCondition(String text, Size size, Point location) : base(text, size, location)
        {
            defaultProperties();
        }

        /// <summary>
        /// A konstruktorok k�z�s tulajdons�gaikat �ll�tja be az a met�dus.
        /// </summary>
        private void defaultProperties()
        {
            this.TextChanged += new EventHandler(ProgramConstructionWithCondition_TextChanged);
        }

    #endregion

    #region M�retet megad� met�dusok

        /// <summary>
        /// Megadja az elemhez tartoz� elemek �sszm�ret�t.
        /// </summary>
        public virtual Size ElementsSize
        {
            get
            {
                int elementsHeight = 0, elementsWidth = 0;

                for (int i = 0; i < this.Controls.Count; ++i)
                {
                    // Mivel �ssze�rnek a programkonstrukci�k, ez�rt a hat�rol� fekete vonalakat
                    // k�tszer sz�moljuk bele, ez�rt vonunk ki egyet, minden alkalommal.
                    elementsHeight += ((ProgramConstruction)this.Controls[i]).Size.Height - 1;

                    // A sz�less�gb�l pedig a legnagyobbat vessz�k
                    if (elementsWidth < ((ProgramConstruction)this.Controls[i]).Size.Width)
                    {
                        elementsWidth = ((ProgramConstruction)this.Controls[i]).Size.Width;
                    }
                }

                // Az utols� elem nem �rintkezik lefele semmivel, ez�rt ebb�l nem szabad kivonnunk 1-et.
                return new Size(elementsWidth, elementsHeight + 1);
            }
        }

        /// <summary>
        /// Ez a f�ggv�ny kisz�m�tja, hogy mekkora a felt�tel magass�ga.
        /// </summary>
        /// <returns> A felt�tel magass�ga. </returns>
        public virtual int ConditionHeight
        {
            get { return ( this.Lines.Length > 0 ? this.Lines.Length * this.FontHeight + 8 : this.FontHeight + 8); }
        }

    #endregion

    #region Absztrakt met�dusok

        /// <summary>
        /// Meghat�rozza, hogy mekkora az elem aj�nlott m�rete.
        /// (Minden oszt�ly saj�t maga fogja defini�lni)
        /// </summary>
        public override abstract Size RecommendedSize { get; }

    #endregion

    #region Egy�b esem�nykezel�k : TextChanged

        /// <summary>
        /// Ha megv�ltozik a Text v�ltoz� tartalma, �s nem f�rne bele a sz�veg
        /// az aktu�lis helyre, akkor megn�veli a m�retet.
        /// </summary>
        protected void ProgramConstructionWithCondition_TextChanged(object sender, EventArgs e)
        {
            Size old = this.Size;
            // A felt�tel magass�ga, valamint az elemek magass�ga lesz
            // a programkonstrukci� magass�ga
            int condAndElementsHeight = ConditionHeight + ElementsSize.Height,
                prefAndMargWidth = PreferredSize.Width + LeftRightMargin;

            this.Size = new Size(prefAndMargWidth > this.Size.Width ? prefAndMargWidth : this.Size.Width,
                                  this.Size.Height != condAndElementsHeight ? condAndElementsHeight : this.Size.Height);

            // M�retv�ltoz�s eset�n �jrarendezz�k a struktogrammot
            if (old != this.Size)
            {
                MainWindow.Stm.Alignment(this, true);
            }

            MainWindow.Stm.SaveState();
        }

    #endregion

    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Drawing;

namespace K
{
    /// <summary>
    /// A Title �s a ProgramConstruction �soszt�lya.
    /// </summary>
    public abstract class Element : TextBox
    {

    #region V�ltoz�k, property-k, konstruktorok, (plusz a hozz�juk tartoz� f�ggv�nyek)

        protected bool selected;
        private int leftRightMargin;

        /// <summary>
        /// Megadja, vagy be�ll�tja az elem bal- �s jobb marg�j�t.
        /// </summary>
        public int LeftRightMargin
        {
            get { return leftRightMargin; }
            set { leftRightMargin = value; }
        }

        /// <summary>
        /// Megadja, hogy az elem ki van-e jel�lve.
        /// </summary>
        public virtual bool Selected
        {
            get { return selected; }
        }

        /// <summary>
        /// A villog� kurzor elt�ntet�se.
        /// </summary>
        [DllImport("user32")]
        protected static extern bool HideCaret(IntPtr hWnd);
        /// <summary>
        /// A villog� kurzor megjelen�t�se.
        /// </summary>
        [DllImport("user32")]
        protected static extern bool ShowCaret(IntPtr hWnd);
        
        /// <summary>
        /// Az element oszt�ly konstruktora.
        /// </summary>
        public Element() : base()
        {
            defaultProperties();
        }
        
        /// <summary>
        /// Ezt a konstruktor szerializ�l�skor h�v�dik meg.
        /// </summary>
        /// <param name="text"> Az elem kezd� Text-je. </param>
        /// <param name="size"> Az elem kezd�m�rete. </param>
        /// <param name="location"> Az elem kezd�poz�ci�ja. </param>
        public Element(String text, Size size, Point location) : base()
        {
            this.Text = text;
            this.Size = size;
            this.Location = location;
            defaultProperties();
        }

        /// <summary>
        /// A konstruktorok k�z�s tulajdons�gait ez a f�ggv�ny �ll�tja be.
        /// </summary>
        private void defaultProperties()
        {
            this.MouseWheel += new MouseEventHandler(Element_MouseWheel);
            this.Disposed += new EventHandler(Element_Disposed);
            this.GotFocus += new EventHandler(Element_GotFocus);
            this.LostFocus += new EventHandler(Element_LostFocus);
            this.TextChanged += new EventHandler(Element_TextChanged);
            MainWindow.StateChanged += new EventHandler(MainWindowStateChanged);
            this.KeyUp += new KeyEventHandler(Element_KeyUp);
            this.KeyDown += new KeyEventHandler(Element_KeyDown);
            this.FontChanged += new EventHandler(Element_FontChanged);
            
            selected = false;
            WordWrap = false;
            TextAlign = HorizontalAlignment.Center;
        }

    #endregion

    #region MainWindowStateChanged

        /// <summary>
        /// Ha a f�ablak �llapota megv�ltozott, akkor megv�ltoztatjuk a kurzor kin�zet�t.
        /// </summary>
        protected void MainWindowStateChanged(object sender, EventArgs e)
        {
            switch (MainWindow.State)
            {
                case MainWindowState.Normal:
                    Cursor = Cursors.IBeam;
                    if (!Selected)
                    {
                        ShowCaret(this.Handle);
                    }
                    break;

                case MainWindowState.Moving:
                    Cursor = Cursors.Hand;
                    MainWindow.Stm.Parent.Focus();
                    break;

                case MainWindowState.MoveMode:
                case MainWindowState.BlockInsert:
                case MainWindowState.LoopInsert:
                case MainWindowState.BranchInsert:
                case MainWindowState.MultipleBranchInsert:
                    Cursor = Cursors.Hand;
                    ShowCaret(this.Handle);
                    MainWindow.Stm.DeselectAll();
                    break;

                case MainWindowState.ShiftDown:
                case MainWindowState.ControlDown:
                    Cursor = Cursors.Arrow;
                    break;

                case MainWindowState.PrepareToResizeHorizontal:
                    Cursor = Cursors.SizeWE;
                    break;

                case MainWindowState.ResizeHorizontal:
                    Cursor = Cursors.SizeWE;
                    HideCaret(this.Handle);
                    break;

                case MainWindowState.PrepareToResizeVertical:
                    Cursor = Cursors.SizeNS;
                    break;

                case MainWindowState.ResizeVertical:
                    Cursor = Cursors.SizeNS;
                    HideCaret(this.Handle);
                    break;
            }
        }

    #endregion

    #region �tv�lt�s kijel�l� m�dra : KeyDown, KeyUp

        /// <summary>
        /// Ha lenyomtuk a Control vagy Shift billenty�t, akkor �tv�lt kijel�l� �llapotba.
        /// A Control-t lenyomva egy elemet tudunk kijel�lni, m�g Shift-et lenyomva az elemet
        /// plusz, ha az elemhez tartoznak tov�bbi elemek (pl. ciklus), akkor azokat is kijel�li.
        /// </summary>
        public static void Element_KeyDown(object sender, KeyEventArgs e)
        {
            // A Control billenty� van lenyomva
            if (e.Control && MainWindow.State == MainWindowState.Normal)
            {
                MainWindow.State = MainWindowState.ControlDown;
            }

            // A Shift billenty� van lenyomva
            if (e.Shift && MainWindow.State == MainWindowState.Normal)
            {
                MainWindow.State = MainWindowState.ShiftDown;
            }

            // Megn�zz�k, hogy az Esc billenty�t nyomt�k-e le, de ezt m�r a MainWindow oszt�ly
            // fogja lekezelni.
            MainWindow.EscKeyDown(sender, e);
        }

        /// <summary>
        /// Ha elengedj�k a Control-t vagy a Shift gombot, akkor vissza�ll�tja a "Normal" �llapotot.
        /// </summary>
        public static void Element_KeyUp(object sender, KeyEventArgs e)
        {
            if (MainWindow.State == MainWindowState.ControlDown || MainWindow.State == MainWindowState.ShiftDown)
            {
                MainWindow.State = MainWindowState.Normal;
            }
        }

    #endregion

    #region Absztrakt met�dusok

        /// <summary>
        /// Meghat�rozza, hogy mekkora az elem aj�nlott m�rete.
        /// (Minden oszt�ly saj�t maga fogja defini�lni)
        /// </summary>
        public abstract Size RecommendedSize { get; }

    #endregion

    #region Egy�b esem�nykezel�k

        /// <summary>
        /// Ha v�ltozik az aktu�lis Text tartalma, �s vannak kijel�lt elemek,
        /// akkor visszavonja a kijel�l�seket, �s az �llapotot is vissza�ll�tja Normal-ra.
        /// </summary>
        private void Element_TextChanged(object sender, EventArgs e)
        {
            MainWindow.State = MainWindowState.Normal;
            MainWindow.Stm.DeselectAll();
            MainWindow.Stm.Refresh();
        }

        /// <summary>
        /// Ha a font megv�ltozik, akkor a m�retet az aj�nlott m�retre �ll�tja.
        /// </summary>
        private void Element_FontChanged(object sender, EventArgs e)
        {
            // Az oldalmarg� a fontm�rett�l f�gg
            LeftRightMargin = Convert.ToInt32(this.Font.Size) * 2;

            // Deszerializ�l�s k�zben megv�ltozik a font, de ekkor nem szabad
            // megv�ltoztatni a m�retet, mert a m�retek, poz�ci�k deszerializ�l�skor meghat�roz�dnak
            if (MainWindow.State != MainWindowState.Deserializing)
            {
                this.Size = this.RecommendedSize;
            }
        }

        /// <summary>
        /// Ha ki van jel�lve az elem �s fokuszt kap, akkor elt�nteti a villog� kurzort.
        /// </summary>
        private void Element_GotFocus(object sender, EventArgs e)
        {
            if (Selected)
            {
                HideCaret(this.Handle);
            }
        }

        /// <summary>
        /// Be�ll�tja az aktu�lis struktogram LatesFocusedElement-�t erre az elemre.
        /// </summary>
        private void Element_LostFocus(object sender, EventArgs e)
        {
            MainWindow.Stm.LatestFocusedElement = this;
        }

        /// <summary>
        /// Megval�s�tja a fel �s le scrolloz�st g�rg�vel.
        /// </summary>
        private void Element_MouseWheel(object sender, MouseEventArgs e)
        {
            StruktogramPage.Scroll(((StruktogramPage)MainWindow.Stm.Parent), e);
        }

        /// <summary>
        /// Elt�vol�t�skor el�hozza a villog� kurzort.
        /// </summary>
        private void Element_Disposed(object sender, EventArgs e)
        {
            ShowCaret(this.Handle);
        }

    #endregion

    }
}

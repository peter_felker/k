using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;

namespace K
{
    /// <summary>
    /// Az �sszes programkonsrukci� �soszt�lya
    /// </summary>
    public abstract class ProgramConstruction : Element
    {

    #region V�ltoz�k, property-k, konstruktor, (plusz a hozz�juk tartoz� f�ggv�nyek)

        protected bool upRect, downRect;
        protected Color rectColor = Color.Green;

        /// <summary>
        /// Jelzi, hogy meg kell-e �llni az �tm�retez�s k�zben, nehogy t�l kicsire
        /// m�retezz�k az elemet.
        /// </summary>
        protected static bool stopReducing = false;

        private bool leaving;
        private double heightRate;
        private int oldX, oldY;
        private Size oldSize;

        /// <summary>
        /// Megadja vagy be�ll�tja az elem magass�gar�ny�t.
        /// </summary>
        public double HeightRate
        {
            get { return heightRate; }
            set { heightRate = value; }
        }

        /// <summary>
        /// Megadja vagy be�ll�tja, hogy �ppen elhagyni k�sz�l�nk-e az eg�rkurzorral
        /// az elemet.
        /// </summary>
        public virtual bool Leaving
        {
            get { return leaving; }
            set { leaving = value; }
        }

        /// <summary>
        /// Megadja, hogy az elemben ki van-e rajzolva FEL�LRE, a z�ld sz�n�, t�glalap alak� "ny�l".
        /// </summary>
        public bool UpRect
        {
            get { return upRect; }
        }

        /// <summary>
        /// Megadja, hogy az elemben ki van-e rajzolva ALULRA, a z�ld sz�n�, t�glalap alak� "ny�l".
        /// </summary>
        public bool DownRect
        {
            get { return downRect; }
        }

        /// <summary>
        /// Megadja, hogy az elemnek l�tezik-e m�r sz�l�je.
        /// </summary>
        protected bool parentExist
        {
            get { return !Equals(this.Parent, null); }
        }

        /// <summary>
        /// Megadja, hogy ki kell-e rajzolni a z�ld t�glalapokat.
        /// </summary>
        /// <returns></returns>
        protected bool showRectangleCondition
        {
            get
            {
                return ((MainWindow.State >= MainWindowState.BlockInsert && MainWindow.State <= MainWindowState.MultipleBranchInsert)
                || (MainWindow.State == MainWindowState.Moving && !Selected));
            }
        }

        /// <summary>
        /// A ProgramConstruction konstruktora.
        /// </summary>
        public ProgramConstruction() : base()
        {
            oldSize = new Size(this.Size.Width, this.Size.Height);
            defaultProperties();
        }

        /// <summary>
        /// Ezt a konstruktor szerializ�l�skor h�v�dik meg.
        /// </summary>
        /// <param name="text"> Az elem kezd� Text-je. </param>
        /// <param name="size"> Az elem kezd�m�rete. </param>
        /// <param name="location"> Az elem kezd�poz�ci�ja. </param>
        public ProgramConstruction(String text, Size size, Point location) : base(text, size, location)
        {
            oldSize = size;
            defaultProperties();
        }

        /// <summary>
        /// A ProgramConstruction konstruktorainak k�z�s tulajdons�gait �ll�tja be ez az elj�r�s.
        /// </summary>
        private void defaultProperties()
        {
            this.MouseClick += new MouseEventHandler(ProgramConstruction_MouseClick);
            this.MouseMove += new MouseEventHandler(ProgramConstruction_MouseMove);
            this.MouseDown += new MouseEventHandler(ProgramConstruction_MouseDown);
            this.MouseUp += new MouseEventHandler(ProgramConstruction_MouseUp);
            this.MouseLeave += new EventHandler(ProgramConstruction_MouseLeave);
            this.SizeChanged += new EventHandler(CheckSize);

            upRect = downRect = leaving = false;
            Multiline = true;
            BorderStyle = BorderStyle.FixedSingle;
        }

    #endregion

    #region M�retez�sn�l haszn�lt esem�nykezel�k, property-k

        /// <summary>
        /// Megadja, hogy az elem JOBB sz�l�n �ll-e az eg�rkurzor.
        /// </summary>
        protected bool cursorInTheRightSide
        {
            get
            {
                Point absPos = this.PointToClient(Cursor.Position);
                return ((absPos.X >= this.Size.Width - 7) && (absPos.X <= this.Size.Width));
            }
        }

        /// <summary>
        /// Megadja, hogy az elem ALJ�N �ll-e az eg�rkurzor.
        /// </summary>
        protected bool cursorInTheBottom
        {
            get
            {
                Point absPos = this.PointToClient(Cursor.Position);
                return ((absPos.Y >= this.Size.Height - 6) && (absPos.Y <= this.Size.Height));
            }
        }

        /// <summary>
        /// Ha elengedj�k az eg�rkurzort, akkor az "�tm�rezet�" �llapotb�l vissz�ll�tja a "Normal" �llapotot.
        /// </summary>
        private void ProgramConstruction_MouseUp(object sender, MouseEventArgs e)
        {
            if (MainWindow.State == MainWindowState.ResizeHorizontal || MainWindow.State == MainWindowState.ResizeVertical)
            {
                MainWindow.State = MainWindowState.Normal;
                stopReducing = false;
                ShowCaret(this.Handle);
                if (oldSize.Width != this.Size.Width || oldSize.Height != this.Size.Height)
                {
                    MainWindow.Stm.SaveState();
                }
            }
        }

        /// <summary>
        /// Ha a struktogram alj�n, vagy jobb sz�l�n �llunk, �s nyomva tartjuk az eg�rgombot,
        /// akkor �tv�lt "�tm�retez�s" �llapotra, �s ekkor m�r a felhaszn�l� �tm�retezheti az elemet.
        /// </summary>
        private void ProgramConstruction_MouseDown(object sender, MouseEventArgs e)
        {
            if (MainWindow.State == MainWindowState.PrepareToResizeHorizontal || MainWindow.State == MainWindowState.PrepareToResizeVertical)
            {
                if (MainWindow.State == MainWindowState.PrepareToResizeHorizontal)
                {
                    MainWindow.State = MainWindowState.ResizeHorizontal;
                }
                else
                {
                    MainWindow.State = MainWindowState.ResizeVertical;
                }
                HideCaret(this.Handle);
                MainWindow.Stm.DeselectAll();
                oldSize = this.Size;
            }

            // Ha a Control, vagy a Shift nyomva van, akkor ne legyen kijel�lve sz�veg
            if (MainWindow.State == MainWindowState.ControlDown || MainWindow.State == MainWindowState.ShiftDown)
            {
                this.SelectionLength = 0;
            }
        }

        /// <summary>
        /// �tm�retez�skor fut le ez az esem�nykezel�. Vigy�z arra, hogy a programkonstrukci�k ne
        /// legyenek kisebbek, mint kellene.
        /// </summary>
        protected virtual void CheckSize(object sender, EventArgs e)
        {
            if (MainWindow.State == MainWindowState.ResizeHorizontal && !stopReducing && parentExist && this.Size.Width <= this.RecommendedSize.Width)
            {
                stopReducing = true;
                this.Size = new Size(this.RecommendedSize.Width, this.Size.Height);
                MainWindow.Stm.Alignment(this,true);
            }

            if (MainWindow.State == MainWindowState.ResizeVertical && !stopReducing && parentExist && this.Size.Height <= this.RecommendedSize.Height)
            {
                stopReducing = true;
                this.Size = new Size(this.Size.Width, this.RecommendedSize.Height);
                MainWindow.Stm.Alignment(this, true);
            }
        }

        /// <summary>
        /// Ha �ppen "Felk�sz�l�s�tm�retez�sre" �llapotban voltunk, �s elhagytuk
        /// az elemet, akkor vissza�ll�tja az �ll�potot "Normal"-ra.
        /// Valamint ha elhagyn�nk az elem fel�let�t akkor is fusson le az �jrarajzol�s,
        /// de ekkor m�r ne rajzoljon ki semmit a fel�letre. Ez�rt kell a Leaving
        /// logikai v�ltoz�.
        /// </summary>
        private void ProgramConstruction_MouseLeave(object sender, EventArgs e)
        {
            if (MainWindow.State == MainWindowState.PrepareToResizeHorizontal || MainWindow.State == MainWindowState.PrepareToResizeVertical)
            {
                MainWindow.State = MainWindowState.Normal;
            }

            HideRectangles();
        }

    #endregion

    #region A z�ld sz�n� t�glalapok megjelen�t�s�vel kapcsolatos met�dusok, property-k

        /// <summary>
        /// L�trehozza a besz�r�st jelz�, z�ld t�glalapokat, ha sz�ks�ges.
        /// </summary>
        protected virtual void createRectangles()
        {
            try
            {
                if (showRectangleCondition)
                {
                    int indexOfPrev = this.Parent.Controls.IndexOf(this) - 1,
                        indexOfNext = this.Parent.Controls.IndexOf(this) + 1;

                    // Az el�z� elemnek a z�ld t�glalapj�t is elt�ntetj�k
                    if ((this.Parent is Struktogram && indexOfPrev >= 1) || (!(this.Parent is Struktogram) && indexOfPrev >= 0))
                    {
                        ((ProgramConstruction)this.Parent.Controls[indexOfPrev]).CheckAndDrawRectangles();
                    }

                    // Valamint a k�vetkez� elemnek is elt�ntetj�k a z�ld t�glalapj�t
                    if (indexOfNext < this.Parent.Controls.Count)
                    {
                        ((ProgramConstruction)this.Parent.Controls[indexOfNext]).CheckAndDrawRectangles();
                    }

                    this.CheckAndDrawRectangles();
                }
            }
            catch (Exception ex) { }
        }

        /// <summary>
        /// Ellen�rzi, hogy ProgramConstruction-ben sz�ks�ges-e �jrarajzolni a z�ld t�glalapokat.
        /// Ha az ellen�rz�s sor�n igazat kapott, akkor ki is rajzolja a z�ld t�glalapo(ka)t.
        /// </summary>
        public virtual void CheckAndDrawRectangles()
        {
            // Ez a felt�tel az�rt van itt, hogy ne fusson le feleslegesesen, �jra �s �jra a kirajzol�s
            if (((this.UpRect && !this.CursorInTheUpperRegion) || (this.DownRect && !this.CursorInTheLowerRegion) ||
                (!this.UpRect && this.CursorInTheUpperRegion) || (!this.DownRect && this.CursorInTheLowerRegion) ||
                Leaving) && !this.Selected)
            {
                this.RepaintRectangles(this.CreateGraphics());
            }
        }

        /// <summary>
        /// Az elemb�l val� t�voz�skor elt�nteti a z�ld t�glalapokat.
        /// </summary>
        public virtual void HideRectangles()
        {
            if (showRectangleCondition)
            {
                int indexOfPrev = this.Parent.Controls.IndexOf(this) - 1,
                    indexOfNext = this.Parent.Controls.IndexOf(this) + 1;

                // Az el�z� elemnek a z�ld t�glalapj�t is elt�ntetj�k
                if ((this.Parent is Struktogram && indexOfPrev >= 1) || (!(this.Parent is Struktogram) && indexOfPrev >= 0))
                {
                    ((ProgramConstruction)this.Parent.Controls[indexOfPrev]).Leaving = true;
                    ((ProgramConstruction)this.Parent.Controls[indexOfPrev]).RepaintRectangles(((ProgramConstruction)this.Parent.Controls[indexOfPrev]).CreateGraphics());
                }

                // Valamint a k�vetkez� elemnek is elt�ntetj�k a z�ld t�glalapj�t
                if (indexOfNext < this.Parent.Controls.Count)
                {
                    ((ProgramConstruction)this.Parent.Controls[indexOfNext]).Leaving = true;
                    ((ProgramConstruction)this.Parent.Controls[indexOfNext]).RepaintRectangles(((ProgramConstruction)this.Parent.Controls[indexOfNext]).CreateGraphics());
                }

                // V�g�l az elemb�l, melyb�l t�vozunk, is elt�ntetj�k a z�ld t�glalapokat
                Leaving = true;
                RepaintRectangles(this.CreateGraphics());
            }
        }

    #endregion

    #region Kijel�l�s �s visszavon�sa

        /// <summary>
        /// Kijel�li az elemet.
        /// </summary>
        public virtual void Select()
        {
            this.BackColor = Color.SteelBlue;
            this.ForeColor = Color.White;
            selected = true;
            HideCaret(this.Handle);
        }

        /// <summary>
        /// Rekurz�van kiej�li az elemet.
        /// Teh�t kijel�li az elemet, �s ha tartoznak hozz� tov�bbi elemek (Pl. ciklus), akkor azokat is.
        /// </summary>
        public virtual void SelectRecursively()
        {
            Select();

            if (this.Controls.Count > 0)
            {
                foreach (ProgramConstruction pc in this.Controls)
                {
                    pc.SelectRecursively();
                }
            }  
        }

        /// <summary>
        /// A kijel�l�st visszavonja.
        /// </summary>
        public virtual void Deselect()
        {
            this.BackColor = Color.White;
            this.ForeColor = MainWindow.Stm.FontColor;
            selected = false;
            ShowCaret(this.Handle);
        }

        /// <summary>
        /// A kiv�laszt�st rekurz�van visszavonja.
        /// Teh�t ha tartoznak az elemhez tov�bbi elemek, akkor azoknak is a kijel�l�s�t rekurz�van visszavonja.
        /// </summary>
        public virtual void DeselectRecursively()
        {
            Deselect();

            if (this.Controls.Count > 0)
            {
                foreach (ProgramConstruction pc in this.Controls)
                {
                    pc.DeselectRecursively();
                }
            }
        }

    #endregion

    #region T�bb dolgot lekezel� esem�nykezel�k : MouseMove, MouseClick

        /// <summary>
        /// Az elemen bel�l mozogva, ha az alj�hoz, vagy a jobb sz�l�hez �r az eg�rkurzor
        /// akkor �tv�lt "Felk�sz�l�s�tm�retez�s" m�dra, vagy ha m�r ebben az �llapotban vagyunk
        /// akkor �tm�retezi a programkonstrukci�t.
        /// </summary>
        protected virtual void ProgramConstruction_MouseMove(object sender, MouseEventArgs e)
        {
            // �tm�retez� ikonok megjelen�t�se, ha sz�ks�ges
            if (!((MainWindow.State >= MainWindowState.BlockInsert && MainWindow.State <= MainWindowState.MultipleBranchInsert) || MainWindow.State == MainWindowState.MoveMode ||
                  MainWindow.State == MainWindowState.Moving || MainWindow.State == MainWindowState.ResizeHorizontal || MainWindow.State == MainWindowState.ResizeVertical))
            {
                // Ha a jobb oldalon vagyunk
                if (cursorInTheRightSide)
                {
                    if (!(MainWindow.State == MainWindowState.PrepareToResizeHorizontal || MainWindow.State == MainWindowState.ResizeHorizontal))
                    {
                        MainWindow.State = MainWindowState.PrepareToResizeHorizontal;
                    }
                }

                // Ha az elem alj�n vagyunk
                if (cursorInTheBottom)
                {
                    if (!(MainWindow.State == MainWindowState.PrepareToResizeVertical || MainWindow.State == MainWindowState.ResizeVertical))
                    {
                        MainWindow.State = MainWindowState.PrepareToResizeVertical;
                    }
                }
            }

            // B�r a kurzor elt�nik m�retez�s k�zben, de ha az eg�rkurzor r�megy a sz�vegre,
            // akkor m�g �gy is kijel�l�dik, �s ezt szeretn�nk megadakad�lyozni
            if ((MainWindow.State == MainWindowState.ResizeHorizontal || MainWindow.State == MainWindowState.ResizeVertical) && this.SelectionLength > 0)
            {
                this.SelectionLength = 0;
            }

            // Ha nem vagyunk se az alj�n, se a jobb sz�len, akkor vissz�llunk "Normal" m�dra
            if (!cursorInTheRightSide && !cursorInTheBottom && (MainWindow.State == MainWindowState.PrepareToResizeHorizontal || MainWindow.State == MainWindowState.PrepareToResizeVertical))
            {
                MainWindow.State = MainWindowState.Normal;
            }

            // �tm�retez�s visszintesen
            if (MainWindow.State == MainWindowState.ResizeHorizontal)
            {
                if (oldX < e.X)
                {
                    this.Size = new Size(this.Size.Width + (e.X - oldX), this.Size.Height);
                    MainWindow.Stm.Alignment(this, true);

                    stopReducing = false;
                }
                CheckSize(sender, e);
                if (oldX > e.X && !stopReducing)
                {
                    this.Size = new Size(this.Size.Width - (oldX - e.X), this.Size.Height);
                    MainWindow.Stm.Alignment(this, true);
                }
            }

            // �tm�retez�s f�gg�legesen
            if (MainWindow.State == MainWindowState.ResizeVertical)
            {
                if (oldY < e.Y)
                {
                    this.Size = new Size(this.Size.Width, this.Size.Height + (e.Y - oldY));
                    MainWindow.Stm.Alignment(this, true);

                    stopReducing = false;
                }
                CheckSize(sender, e);
                if (oldY > e.Y && !stopReducing)
                {
                    this.Size = new Size(this.Size.Width, this.Size.Height - (oldY - e.Y));
                    MainWindow.Stm.Alignment(this, true);
                }
            }

            // Az �tm�retez�shez kellenek, hogy tudjuk, hogy mennyit mozdult el az eg�r
            oldX = e.X;
            oldY = e.Y;

            createRectangles();
        }

        /// <summary>
        /// Eg�rklikkel�s eset�n ez az esem�nykezel� fut le.
        /// </summary>
        protected virtual void ProgramConstruction_MouseClick(object sender, MouseEventArgs e)
        {
            // Ha a Control billenty� nyomva van, akkor kijel�lj�k az elemet, vagy
            // �ppen visszavonjuk a kijel�l�st, att�l f�gg�en, hogy ki volt-e el�tte jel�lve.
            if (MainWindow.State == MainWindowState.ControlDown)
            {
                if (!Selected)
                {
                    Select();
                }
                else
                {
                    Deselect();
                }
            }

            // Ha a Shift van lenyomva, akkor rekurz�van kijel�lj�k, vagy rekurz�van
            // visszavonjuk a kijel�l�st.
            if (MainWindow.State == MainWindowState.ShiftDown)
            {
                if (!Selected)
                {
                    SelectRecursively();
                }
                else
                {
                    DeselectRecursively();
                }
            }

            // �thelyez� �llapot eset�n kijel�lj�k az elemet �thelyez�sre
            if (MainWindow.State == MainWindowState.MoveMode)
            {
                SelectRecursively();
                MainWindow.State = MainWindowState.Moving;
            }

            // Ha megjelentek a z�ld t�glalapok, akkor besz�r�sra, vagy �thelyez�sre ker�l sor
            if (showRectangleCondition)
            {
                // Ha a FELS� t�glalap l�tszik
                if (UpRect)
                {
                    switch (MainWindow.State)
                    {
                        case MainWindowState.BlockInsert: MainWindow.Stm.AddBlock(Parent, Parent.Controls.IndexOf(this)); break;
                        case MainWindowState.LoopInsert: MainWindow.Stm.AddLoop(Parent, Parent.Controls.IndexOf(this)); break;
                        case MainWindowState.BranchInsert: MainWindow.Stm.AddBranch(Parent, Parent.Controls.IndexOf(this)); break;
                        case MainWindowState.MultipleBranchInsert: MainWindow.Stm.AddMultipleBranch(Parent, Parent.Controls.IndexOf(this)); break;
                        case MainWindowState.Moving: MainWindow.Stm.MoveElement(Parent, MainWindow.Stm.SelectedElements , Parent.Controls.IndexOf(this)); break;
                    }
                }
                // Ha az ALS� t�glalap l�tszik
                if (DownRect)
                {
                    switch (MainWindow.State)
                    {
                        case MainWindowState.BlockInsert: MainWindow.Stm.AddBlock(Parent, Parent.Controls.IndexOf(this) + 1); break;
                        case MainWindowState.LoopInsert: MainWindow.Stm.AddLoop(Parent, Parent.Controls.IndexOf(this) + 1); break;
                        case MainWindowState.BranchInsert: MainWindow.Stm.AddBranch(Parent, Parent.Controls.IndexOf(this) + 1); break;
                        case MainWindowState.MultipleBranchInsert: MainWindow.Stm.AddMultipleBranch(Parent, Parent.Controls.IndexOf(this) + 1); break;
                        case MainWindowState.Moving: MainWindow.Stm.MoveElement(Parent, MainWindow.Stm.SelectedElements, Parent.Controls.IndexOf(this) + 1); break;
                    }
                }
            }

            // Ha egy elemre klikkel�nk �s vannak kijel�lve elemek, akkor
            // visszavonjuk a kijel�l�seket.
            if (MainWindow.State == MainWindowState.Normal)
            {
                ShowCaret(this.Handle);
                MainWindow.Stm.DeselectAll();
            }
        }

    #endregion

    #region Absztrakt met�dusok

        /// <summary>
        /// Ez rajzolja ki az elem tetej�re, illetve alj�ra a z�ld t�glalapokat.
        /// Defini�l�sa a sz�rmaztatott oszt�lyokra van b�zva.
        /// </summary>
        public abstract void RepaintRectangles(Graphics g);

        /// <summary>
        /// Megadja, hogy a kurzor, az elem tetej�n (FELS� r�sz�ben) van-e.
        /// A sz�rmaztatott oszt�lyoknak kell implement�lni.
        /// </summary>
        public abstract bool CursorInTheUpperRegion { get;}

        /// <summary>
        /// Megadja, hogy a kurzor, az elem alj�n (Als� r�sz�ben) van-e.
        /// A sz�rmaztatott oszt�lyoknak kell implement�lni.
        /// </summary>
        public abstract bool CursorInTheLowerRegion { get;}

    #endregion

    #region Egy�b met�dusok

        /// <summary>
        /// Megadja, hogy h�ny sora van az aktu�lis programkonstrukci�nak. 
        /// </summary>
        /// <returns> A Text stringben a sorok sz�ma. </returns>
        public virtual int NumberOfLines
        {
            get { return ( (this.Lines.Length > 0) ? this.Lines.Length : 1 ); }
        }

        /// <summary>
        /// Meghat�rozza, hogy mekkora az elem aj�nlott m�rete.
        /// (Minden oszt�ly saj�t maga fogja defini�lni)
        /// </summary>
        public override Size RecommendedSize { get { return new Size(); } }

    #endregion

    }
}
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace K
{
    /// <summary>
    /// Az opci�k ablak, melyben a felhaszn�l� az alkalmaz�s be�ll�t�sait tudja be�ll�tani.
    /// </summary>
    public partial class OptionsDialog : Form
    {
        public OptionsDialog()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja a visszal�p�sek sz�m�nak �rt�k�t,
        /// az OptionsDialog-ban. 
        /// </summary>
        public int NumberOfBackSteps
        {
            get { return ((int)numberOfBackStepsNumericUpDown.Value); }
            set { numberOfBackStepsNumericUpDown.Value = value; }
        }

    }
}
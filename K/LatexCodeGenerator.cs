using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace K
{

#region LatexMode, LatexFont, LatexCharacterSize

    /// <summary>
    /// Latex-ben elérhető módok halmaza.
    /// </summary>
    public enum LatexMode { Math, Text }

    /// <summary>
    /// Latex-ben elérhető fontok halmaza.
    /// </summary>
    public enum LatexFont { Roman, SansSerif, Typewriter, Default }

    /// <summary>
    /// Latex-ben elérhető betűméretek halmaza.
    /// </summary>
    public enum LatexCharacterSize { Tiny, ScriptSize, FootnoteSize, Small, Normal,
                                     Large, Larger, Largest, Huge, Hugger }

#endregion

#region LatexOptions osztály

    /// <summary>
    /// Ebben az osztályban tároljuk a Latex kód generálásához szükséges opciókat.
    /// </summary>
    public class LatexOptions
    {
        LatexMode mode;
        LatexFont font;
        LatexCharacterSize characterSize;
        bool thickLines, showTitle, onlyStruktogramCode;
        int struktogramWidth, heightUnit;

        /// <summary>
        /// Visszaadja vagy beállítja a Latex módot.
        /// </summary>
        public LatexMode Mode
        {
            get { return mode; }
            set { mode = value; }
        }

        /// <summary>
        /// Visszaadja vagy beállítja a Latex fontot.
        /// </summary>
        public LatexFont Font
        {
            get { return font; }
            set { font = value; }
        }

        /// <summary>
        /// Visszaadja vagy beállítja a Latex karakterméretet.
        /// </summary>
        public LatexCharacterSize CharacterSize
        {
            get { return characterSize; }
            set { characterSize = value; }
        }

        /// <summary>
        /// Visszaadja vagy beállítja, hogy vastagított vonalak legyenek-e a struktogramban.
        /// </summary>
        public bool ThickLines
        {
            get { return thickLines; }
            set { thickLines = value; }
        }

        /// <summary>
        /// Visszaadja vagy beállítja, hogy a struktogramnak legyen-e címe.
        /// </summary>
        public bool ShowTitle
        {
            get { return showTitle; }
            set { showTitle = value; }
        }

        /// <summary>
        /// Megadja, vagy beállítja, hogy csak struktogram kódot készítsen-e a generátor, vagy
        /// belehelyezzen a kódba minden olyan utasítást, amivel meg lehet nézni az eredményt.
        /// </summary>
        public bool OnlyStruktogramCode
        {
            get { return onlyStruktogramCode; }
            set { onlyStruktogramCode = value; }
        }

        /// <summary>
        /// Visszaadja vagy beállítja a struktogram szélességét.
        /// </summary>
        public int StruktogramWidth
        {
            get { return struktogramWidth; }
            set { struktogramWidth = value; }
        }

        /// <summary>
        /// Visszaadja vagy beállítja a struktogram magasságegység értékét.
        /// </summary>
        public int HeightUnit
        {
            get { return heightUnit; }
            set { heightUnit = value; }
        }
    }

#endregion

    /// <summary>
    /// Ez az osztály generálja a LaTeX kódot.
    /// </summary>
    public class LatexCodeGenerator
    {

    #region Változók, property-k, konstruktorok

        /// <summary>
        /// Ebben a változóban tároljuk, hogy milyen mélyen vagyunk a rekurzióban.
        /// </summary>
        private int recursionDepth;

        private Struktogram stm;
        private LatexOptions latexOptions;

        /// <summary>
        /// Megadja, vagy beállítja a LaTeX kód generálásához szükséges struktogrammot.
        /// </summary>
        public Struktogram Struktogram
        {
            get { return stm; }
            set { this.stm = value; }
        }

        /// <summary>
        /// Megadja, vagy beállítja a LaTeX kód generálásakor használandó opciókat.
        /// </summary>
        public LatexOptions LatexOptions
        {
            get { return latexOptions; }
            set { this.latexOptions = value; }
        }

        /// <summary>
        /// A LatexCodeGenerator konstruktora.
        /// </summary>
        public LatexCodeGenerator()
        {
            this.stm = null;
            this.LatexOptions = null;
        }

        /// <summary>
        /// A LatexCodeGenerator konstruktora.
        /// </summary>
        /// <param name="stm">A struktogram, melyből Latex kódot szeretnénk generálni.</param>
        public LatexCodeGenerator(Struktogram stm)
        {
            this.stm = stm;
            this.LatexOptions = null;
        }

        /// <summary>
        /// A LatexCodeGenerator konstruktora.
        /// </summary>
        /// <param name="options">A Latex kód generálásához szükséges opciók.</param>
        public LatexCodeGenerator(LatexOptions options)
        {
            this.stm = null;
            this.LatexOptions = options;
        }

        /// <summary>
        /// A LatexCodeGenerator konstruktora.
        /// </summary>
        /// <param name="stm">A struktogram, melyből Latex kódot szeretnénk generálni.</param>
        /// <param name="options">A Latex kód generálásához szükséges opciók.</param>
        public LatexCodeGenerator(Struktogram stm, LatexOptions options)
        {
            this.stm = stm;
            this.LatexOptions = options;
        }

    #endregion

    #region LaTeX kód generálás

        /// <summary>
        /// Latex kód generálása a megadott struktogramból és opciókból.
        /// </summary>
        /// <param name="stm">A struktogram, melyből a Latex kódot szeretnénk generálni.</param>
        /// <param name="options">Az opciók, melyeket a kódgenerátor figyelembe vesz a kód generálása közben.</param>
        /// <returns>Az elkészített Latex kód.</returns>
        public string CreateLatexCode(Struktogram stm, LatexOptions options)
        {
            this.stm = stm;
            this.LatexOptions = options;
            return CreateLatexCode();
        }

        /// <summary>
        /// Latex kód generálása a megadott opciókból, és a már előre megadott struktogramból.
        /// </summary>
        /// <param name="options">Az opciók, melyeket a kódgenerátor figyelembe vesz a kód generálása közben.</param>
        /// <returns>Az elkészített Latex kód.</returns>
        public string CreateLatexCode(LatexOptions options)
        {
            this.LatexOptions = options;
            return CreateLatexCode();
        }

        /// <summary>
        /// Latex kód generálása a megadott struktogramból, és a már előre megadott opciókból.
        /// </summary>
        /// <param name="stm">A struktogram, melyből a Latex kódot szeretnénk generálni.</param>
        /// <returns>Az elkészített Latex kód.</returns>
        public string CreateLatexCode(Struktogram stm)
        {
            this.stm = stm;
            return CreateLatexCode();
        }

        /// <summary>
        /// Latex kód generálása a már előre megadott struktogramból és opciókból.
        /// </summary>
        /// <returns>Az elkészített Latex kód.</returns>
        public string CreateLatexCode()
        {
            // Struktogram nélkül nehéz létrehozni kódot
            if (stm == null)
            {
                throw new NullReferenceException("Can't create LaTeX code without struktogram!");
            }
            else
            {
                String code = String.Empty;

                if (!LatexOptions.OnlyStruktogramCode)
                {
                    code += "\\documentclass[11pt]{article}\n" +
                              "\\usepackage{stuki}\n" +
                              "\\usepackage{amsmath}\n" +
                              "\\usepackage{amssymb}\n" +
                              "\\begin{document}\n";
                    recursionDepth = 2;
                }
                else
                {
                    recursionDepth = 1;
                }
                
                // Betűméret beállítása
                switch (LatexOptions.CharacterSize)
                {
                    case LatexCharacterSize.Tiny :
                        code += "\\tiny\n";
                    break;
                    case LatexCharacterSize.ScriptSize:
                        code += "\\scriptsize\n";
                    break;
                    case LatexCharacterSize.FootnoteSize:
                        code += "\\footnotesize\n";
                    break;
                    case LatexCharacterSize.Small:
                        code += "\\small\n";
                    break;
                    case LatexCharacterSize.Normal:
                        code += "\\normalsize\n";
                    break;
                    case LatexCharacterSize.Large:
                        code += "\\large\n";
                    break;
                    case LatexCharacterSize.Larger:
                        code += "\\Large\n";
                    break;
                    case LatexCharacterSize.Largest:
                        code += "\\LARGE\n";
                    break;
                    case LatexCharacterSize.Huge:
                        code += "\\huge\n";
                    break;
                    case LatexCharacterSize.Hugger:
                        code += "\\Huge\n";
                    break;
                }

                // Vastagított vonalak
                if (LatexOptions.ThickLines)
                {
                    code += "\\thicklines\n";
                }

                // Ha a kiválasztott LatexMode Text, akkor
                // beállítjuk a megadott fontot
                if(LatexOptions.Mode == LatexMode.Text)
                {
                    switch (LatexOptions.Font)
                    {
                        case LatexFont.Roman :
                            code += "\\rmfamily\n";
                        break;
                        case LatexFont.SansSerif :
                            code += "\\sffamily\n";
                        break;
                        case LatexFont.Typewriter:
                            code += "\\ttfamily\n";
                        break;
                    }
                }

                // Magasságegység beállítása
                code += "\\setlength{\\stmheight}{" + LatexOptions.HeightUnit.ToString() + "pt}\n";

                code +="\t\\begin{stuki" + (LatexOptions.ShowTitle ? "*" : "") + "}" + "[" + Convert.ToString(LatexOptions.StruktogramWidth) + "pt]";

                // Cím mutatása, ha szükséges
                if (LatexOptions.ShowTitle)
                {
                    code += "{" + AnalizeText(stm.Controls[0].Text) + "}\n";
                }
                else
                {
                    code += "\n";
                }

                // Egy fabejárással bejárjuk a struktogrammot és létrehozzuk a kódot
                treeWalkLatexCodeGeneration(ref code, stm.Controls);

                if (!LatexOptions.OnlyStruktogramCode)
                {
                    code += "\t\\end{stuki" + (LatexOptions.ShowTitle ? "*" : "") + "}\n" +
                            "\\end{document}\n";
                }
                else
                {
                    code += "\\end{stuki" + (LatexOptions.ShowTitle ? "*" : "") + "}\n";
                }

                return code;
            }
        }

        /// <summary>
        /// Ez a függvény fabejárással végigmegy az összes elemen és létrehozza belőlük a LaTeX kódot.
        /// </summary>
        /// <param name="code"> Ide kerűl majd a végleges LaTeX kód. </param>
        /// <param name="elements"> Az éppen bejárandó programkonstrukciók. </param>
        private void treeWalkLatexCodeGeneration(ref string code, Control.ControlCollection elements)
        {
            ProgramConstruction pc;

            // Ha a szülő Struktogram, akkor a címet nem kell belevennünk a vizsgálásba
            for (int i = (elements.Owner is Struktogram ? 1 : 0); i < elements.Count; ++i)
            {
                pc = ((ProgramConstruction)elements[i]);

                // Először kitesszük a tabokat
                if (!(pc.Parent is Branch))
                {
                    addTabs(ref code);
                }

                // Az elem adatainak kitevése, majd ha az elemben van szöveg, akkor annak analizálása
                switch (pc.GetType().ToString())
                {
                    case "K.Block":
                        code += "\\stm" + displayStar() + "[" + Math.Round(ElementHeight(pc), 2).ToString() + "]{" + AnalizeText(pc.Text) + "}\n";
                        break;

                    case "K.Loop":
                        code += "\\begin{WHILE}{" + Convert.ToString(Math.Round(ElementHeight(pc) - ((ProgramConstruction)pc).NumberOfLines, 2)) + "}" + "{ \\stm" + displayStar() + "[" + ((Loop)pc).NumberOfLines.ToString() + "]{" + AnalizeText(pc.Text) + "} }\n";
                        break;

                    case "K.Branch":
                        int separatorRate = Convert.ToInt32(Math.Round((Convert.ToDouble(((Branch)pc).Controls[0].Size.Width) / Convert.ToDouble(((Branch)pc).Size.Width)) * 100));

                        code += "\\begin{IF}[" + separatorRate.ToString() + "]{" + Convert.ToString(Math.Round(ElementHeight(pc) - ((Branch)pc).NumberOfLines, 2)) + "}" + "{ \\stm" + displayStar() + "[" + ((Branch)pc).NumberOfLines.ToString() + "]{" + AnalizeText(pc.Text) + "} }\n";
                        break;

                    case "K.MultipleBranch":
                        code += "\\begin{CASE}[" + ((MultipleBranch)pc).NumberOfLines.ToString() + "]{" + Convert.ToString(Math.Round(ElementHeight(pc) - ((MultipleBranch)pc).NumberOfLines, 2)) + "}" + "{" + Convert.ToString(pc.Size.Width + pc.Controls.Count - 1) + "}\n";
                        break;

                    case "K.SimpleBranch":
                        if (!(pc.Parent is Branch))
                        {
                            code += "\\WHEN[" + pc.Size.Width + "]" + "{ \\stm" + displayStar() + "[" + ((SimpleBranch)pc).NumberOfLines.ToString() + "]{" + AnalizeText(pc.Text) + "} }\n";
                        }
                        break;
                }

                // Ha az elemnek vannak elemei, akkor bejárjuk azokat is
                if (pc.Controls.Count > 0)
                {
                    if (!(pc.Parent is Branch))
                    {
                        ++recursionDepth;
                    }
                    treeWalkLatexCodeGeneration(ref code, pc.Controls);
                    if (!(pc.Parent is Branch))
                    {
                        --recursionDepth;
                    }
                }

                // Az elemekhez tartozó lezárások (\end) kitevése
                switch (pc.GetType().ToString())
                {
                    case "K.Loop":
                        addTabs(ref code);
                        code += "\\end{WHILE}\n";
                        break;

                    case "K.Branch":
                        addTabs(ref code);
                        code += "\\end{IF}\n";
                        break;

                    case "K.MultipleBranch":
                        addTabs(ref code);
                        code += "\\end{CASE}\n";
                        break;
                }

                // Branch szülő esetén a másik elágazásba lépésnél az ELSE kitevése
                if (pc.Parent is Branch)
                {
                    // Tabok kitevése
                    --recursionDepth;
                    addTabs(ref code);
                    ++recursionDepth;

                    code += "\\ELSE\n";
                }

            }
        }

        /// <summary>
        /// Meghatározza egy elem (össz)magasságát.
        /// </summary>
        /// <param name="pc"> A programkonstrukció, melynek az (össz)magasságát szeretnénk megtudni.</param>
        /// <returns>A megadott elem (össz)magassága.</returns>
        private double ElementHeight(ProgramConstruction pc)
        {
            double elementHeight = 0;

            switch (pc.GetType().ToString())
            {
                case "K.Block":
                    elementHeight = Convert.ToDouble(pc.Size.Height / (pc.Font.GetHeight() + 8.0));
                    break;

                case "K.Loop":
                    elementHeight = Convert.ToDouble(pc.NumberOfLines);
                    for (int i = 0; i < pc.Controls.Count; ++i)
                    {
                        elementHeight += ElementHeight(((ProgramConstruction)pc.Controls[i]));
                    }
                    break;

                case "K.Branch":
                    double leftHeight = 0.0, rightHeight = 0.0;

                    elementHeight = Convert.ToDouble(pc.NumberOfLines);

                    leftHeight = ElementHeight(((ProgramConstruction)pc.Controls[0]));
                    rightHeight = ElementHeight(((ProgramConstruction)pc.Controls[1]));

                    if (leftHeight > rightHeight)
                    {
                        elementHeight += leftHeight;
                    }
                    else
                    {
                        elementHeight += rightHeight;
                    }
                    break;

                case "K.MultipleBranch":
                    double tmpElementHeight, tmpNumberOfLines = Convert.ToDouble(pc.NumberOfLines);

                    elementHeight = 0.0;

                    for (int i = 0; i < pc.Controls.Count; ++i)
                    {
                        tmpElementHeight = ElementHeight(((ProgramConstruction)pc.Controls[i]));

                        if (elementHeight < tmpNumberOfLines + tmpElementHeight)
                        {
                            elementHeight = tmpNumberOfLines + tmpElementHeight;
                        }
                    }
                    break;

                case "K.SimpleBranch":
                    for (int i = 0; i < pc.Controls.Count; ++i)
                    {
                        elementHeight += ElementHeight(((ProgramConstruction)pc.Controls[i]));
                    }
                    break;
            }

            return elementHeight;
        }

        /// <summary>
        /// A felhasználó dolgát megkönnyítve ez a függvény kiteszi a sorok elejére a megfelelő
        /// mennyiségű tabulátort, a szép tagolás érdekében.
        /// </summary>
        /// <param name="code"></param>
        private void addTabs(ref string code)
        {
            for (int i = 0; i < recursionDepth; ++i)
            {
                code += "\t";
            }
        }

        /// <summary>
        /// A Latex módtól függően visszatér "*"-al vagy üres stringgel.
        /// Ez a kódban lévő \stm karaktersorozat után kell tenni.
        /// Az üres string azt jelenti, hogy Math módban vagyunk, míg a csillag
        /// a Text módot jelenti.
        /// </summary>
        /// <returns></returns>
        private string displayStar()
        {
            return ((Equals(LatexOptions, null) || LatexOptions.Mode == LatexMode.Math) ? "" : "*");
        }

    #endregion

    #region AnalizeText

        /// <summary>
        /// Átírja a szövegben előforduló speciális karaktereket.
        /// </summary>
        /// <param name="text">Az analizálandó szöveg.</param>
        /// <returns>Az analizált szöveg.</returns>
        private string AnalizeText(string text)
        {
            String analizedText = "";

            for (int i = 0; i < text.Length; ++i)
            {
                switch (text[i])
                {
                    // ÉKEZETES KARAKTEREK ///////////////////////////////////

                    case 'á':
                        analizedText += "\\'a";
                        break;
                    case 'ä':
                        analizedText += "\\\"a";
                        break;
                    case 'Á':
                        analizedText += "\\'A";
                        break;
                    case 'Ä':
                        analizedText += "\\\"A";
                        break;
                    case 'é':
                        analizedText += "\\'e";
                        break;
                    case 'ë':
                        analizedText += "\\\"e";
                        break;
                    case 'É':
                        analizedText += "\\'E";
                        break;
                    case 'Ë':
                        analizedText += "\\\"E";
                        break;
                    case 'í':
                        analizedText += "\\'i";
                        break;
                    case 'ï':
                        analizedText += "\\\"i";
                        break;
                    case 'Í':
                        analizedText += "\\'I";
                        break;
                    case 'Ï':
                        analizedText += "\\\"I";
                        break;
                    case 'ó':
                        analizedText += "\\'o";
                        break;
                    case 'ö':
                        analizedText += "\\\"o";
                        break;
                    case 'ő':
                        analizedText += "\\H o";
                        break;
                    case 'Ó':
                        analizedText += "\\'O";
                        break;
                    case 'Ö':
                        analizedText += "\\\"O";
                        break;
                    case 'Ő':
                        analizedText += "\\H O";
                        break;
                    case 'ú':
                        analizedText += "\\'u";
                        break;
                    case 'ü':
                        analizedText += "\\\"u";
                        break;
                    case 'ű':
                        analizedText += "\\H u";
                        break;
                    case 'Ú':
                        analizedText += "\\'U";
                        break;
                    case 'Ü':
                        analizedText += "\\\"U";
                        break;
                    case 'Ű':
                        analizedText += "\\H U";
                        break;

                    // KÜLÖNLEGES KARAKTEREK ///////////////////////////////////

                    case '\n':
                        analizedText += "\\\\";
                        break;
                    case '\\':
                        analizedText += "$\\backslash$";
                        break;
                    case '{':
                        analizedText += "\\{";
                        break;
                    case '}':
                        analizedText += "\\}";
                        break;
                    case '$':
                        analizedText += "\\$";
                        break;
                    case '&':
                        analizedText += "\\&";
                        break;
                    case '%':
                        analizedText += "\\%";
                        break;
                    case '#':
                        analizedText += "\\#";
                        break;
                    case '_':
                        analizedText += "\\_";
                        break;
                    case '~':
                        analizedText += "\\sim ";
                        break;

                    // ALAPVETŐ MATEMATIKAI JELEK //////////////////////////////////////

                    case '±':
                        analizedText += "\\pm ";
                        break;
                    case '∓':
                        analizedText += "\\mp ";
                        break;
                    case '×':
                        analizedText += "\\times ";
                        break;
                    case '∙':
                        analizedText += "\\cdot ";
                        break;
                    case '⌈':
                        analizedText += "\\left\\lceil ";
                        break;
                    case '⌉':
                        analizedText += "\\right\\rceil ";
                        break;
                    case '⌊':
                        analizedText += "\\left\\lfloor ";
                        break;
                    case '⌋':
                        analizedText += "\\right\\rfloor ";
                        break;
                    case '∞':
                        analizedText += "\\infty ";
                        break;
                    case '≠':
                        analizedText += "\\neq ";
                        break;
                    case '≅':
                        analizedText += "\\cong ";
                        break;
                    case '≈':
                        analizedText += "\\approx ";
                        break;
                    case '≡':
                        analizedText += "\\equiv ";
                        break;
                    case '∝':
                        analizedText += "\\propto ";
                        break;
                    case '≪':
                        analizedText += "\\ll ";
                        break;
                    case '≫':
                        analizedText += "\\gg ";
                        break;
                    case '≤':
                        analizedText += "\\le ";
                        break;
                    case '≥':
                        analizedText += "\\ge ";
                        break;
                    case '∀':
                        analizedText += "\\forall ";
                        break;
                    case '∃':
                        analizedText += "\\exists ";
                        break;
                    case '∄':
                        analizedText += "\\nexists ";
                        break;
                    case '¬':
                        analizedText += "\\neg ";
                        break;
                    case '∂':
                        analizedText += "\\partial ";
                        break;
                    case '∪':
                        analizedText += "\\cup ";
                        break;
                    case '∩':
                        analizedText += "\\cap ";
                        break;
                    case '∈':
                        analizedText += "\\in ";
                        break;
                    case '∉':
                        analizedText += "\\notin ";
                        break;
                    case '∅':
                        analizedText += "\\emptyset ";
                        break;
                    case '℉':
                        analizedText += "\\,^{\\circ}F ";
                        break;
                    case '℃':
                        analizedText += "\\,^{\\circ}C ";
                        break;
                    case '∆':
                        analizedText += "\\triangle ";
                        break;
                    case '∇':
                        analizedText += "\\triangledown ";
                        break;

                    // GÖRÖG BETŰK /////////////////////////////////////////////////

                    case 'α':
                        analizedText += "\\alpha ";
                        break;
                    case 'β':
                        analizedText += "\\beta ";
                        break;
                    case 'γ':
                        analizedText += "\\gamma ";
                        break;
                    case 'δ':
                        analizedText += "\\delta ";
                        break;
                    case 'ε':
                        analizedText += "\\varepsilon ";
                        break;
                    case 'ζ':
                        analizedText += "\\zeta ";
                        break;
                    case 'η':
                        analizedText += "\\eta ";
                        break;
                    case 'θ':
                        analizedText += "\\theta ";
                        break;
                    case 'ϑ':
                        analizedText += "\\vartheta ";
                        break;
                    case 'ι':
                        analizedText += "\\iota ";
                        break;
                    case 'κ':
                        analizedText += "\\kappa ";
                        break;
                    case 'λ':
                        analizedText += "\\lambda ";
                        break;
                    case 'μ':
                        analizedText += "\\mu ";
                        break;
                    case 'ν':
                        analizedText += "\\nu ";
                        break;
                    case 'ξ':
                        analizedText += "\\xi ";
                        break;
                    case 'π':
                        analizedText += "\\pi ";
                        break;
                    case 'ρ':
                        analizedText += "\\rho ";
                        break;
                    case 'ϱ':
                        analizedText += "\\varrho ";
                        break;
                    case 'σ':
                        analizedText += "\\sigma ";
                        break;
                    case 'ς':
                        analizedText += "\\varsigma ";
                        break;
                    case 'τ':
                        analizedText += "\\tau ";
                        break;
                    case 'υ':
                        analizedText += "\\upsilon ";
                        break;
                    case 'ϕ':
                        analizedText += "\\phi ";
                        break;
                    case 'φ':
                        analizedText += "\\varphi ";
                        break;
                    case 'χ':
                        analizedText += "\\chi ";
                        break;
                    case 'ψ':
                        analizedText += "\\psi ";
                        break;
                    case 'ω':
                        analizedText += "\\omega ";
                        break;
                    case 'Γ':
                        analizedText += "\\Gamma ";
                        break;
                    case 'Δ':
                        analizedText += "\\Delta ";
                        break;
                    case 'Θ':
                        analizedText += "\\Theta ";
                        break;
                    case 'Λ':
                        analizedText += "\\Lambda ";
                        break;
                    case 'Ξ':
                        analizedText += "\\Xi ";
                        break;
                    case 'Π':
                        analizedText += "\\Pi ";
                        break;
                    case 'Σ':
                        analizedText += "\\Sigma ";
                        break;
                    case 'Υ':
                        analizedText += "\\Upsilon ";
                        break;
                    case 'Φ':
                        analizedText += "\\Phi ";
                        break;
                    case 'Ψ':
                        analizedText += "\\Psi ";
                        break;
                    case 'Ω':
                        analizedText += "\\Omega ";
                        break;

                    // Operátorok ////////////////////////////////////////////////

                    case '∧':
                        analizedText += "\\wedge ";
                        break;
                    case '∨':
                        analizedText += "\\vee ";
                        break;
                    case '⊢':
                        analizedText += "\\vdash ";
                        break;
                    case '⊂':
                        analizedText += "\\subset ";
                        break;
                    case '⊃':
                        analizedText += "\\supset ";
                        break;
                    case '⊆':
                        analizedText += "\\subseteq ";
                        break;
                    case '⊇':
                        analizedText += "\\supseteq ";
                        break;
                    case '∋':
                        analizedText += "\\ni ";
                        break;
                    case '≦':
                        analizedText += "\\leqq ";
                        break;
                    case '≧':
                        analizedText += "\\geqq ";
                        break;
                    case '≺':
                        analizedText += "\\prec ";
                        break;
                    case '≻':
                        analizedText += "\\succ ";
                        break;
                    case '≼':
                        analizedText += "\\preceq ";
                        break;
                    case '≽':
                        analizedText += "\\succeq ";
                        break;
                    case '∘':
                        analizedText += "\\circ ";
                        break;
                    case '∑':
                        analizedText += "\\sum ";
                        break;
                    case '∫':
                        analizedText += "\\int ";
                        break;
                    case '∬':
                        analizedText += "\\iint ";
                        break;
                    case '∭':
                        analizedText += "\\iiint ";
                        break;
                    case '∏':
                        analizedText += "\\prod ";
                        break;
                    case '∐':
                        analizedText += "\\coprod ";
                        break;
                    case '∥':
                        analizedText += "\\parallel ";
                        break;
                    case '⊥':
                        analizedText += "\\bot ";
                        break;
                    case '⋈':
                        analizedText += "\\Join ";
                        break;
                    case '⊕':
                        analizedText += "\\oplus ";
                        break;
                    case '⊖':
                        analizedText += "\\ominus ";
                        break;
                    case '⊗':
                        analizedText += "\\otimes ";
                        break;
                    case '⊘':
                        analizedText += "\\oslash ";
                        break;
                    case '⊙':
                        analizedText += "\\odot ";
                        break;
                    case '⊛':
                        analizedText += "\\circledast ";
                        break;
                    case '⊚':
                        analizedText += "\\circledcirc ";
                        break;

                    // NYILAK ////////////////////////////////////////////

                    case '←':
                        analizedText += "\\leftarrow ";
                        break;
                    case '→':
                        analizedText += "\\rightarrow ";
                        break;
                    case '↑':
                        analizedText += "\\uparrow ";
                        break;
                    case '↓':
                        analizedText += "\\downarrow ";
                        break;
                    case '↔':
                        analizedText += "\\leftrightarrow ";
                        break;
                    case '↗':
                        analizedText += "\\nearrow ";
                        break;
                    case '↖':
                        analizedText += "\\nwarrow ";
                        break;
                    case '↘':
                        analizedText += "\\searrow ";
                        break;
                    case '↙':
                        analizedText += "\\swarrow ";
                        break;
                    case '↚':
                        analizedText += "\\nleftarrow ";
                        break;
                    case '↛':
                        analizedText += "\\nrightarrow ";
                        break;
                    case '↮':
                        analizedText += "\\nleftrightarrow ";
                        break;
                    case '⇐':
                        analizedText += "\\Leftarrow ";
                        break;
                    case '⇒':
                        analizedText += "\\Rightarrow ";
                        break;
                    case '⇑':
                        analizedText += "\\Uparrow ";
                        break;
                    case '⇓':
                        analizedText += "\\Downarrow ";
                        break;
                    case '⇔':
                        analizedText += "\\Leftrightarrow ";
                        break;
                    case '⇕':
                        analizedText += "\\Updownarrow ";
                        break;
                    case '⇍':
                        analizedText += "\\nLeftarrow ";
                        break;
                    case '⇏':
                        analizedText += "\\nRightarrow ";
                        break;
                    case '⇎':
                        analizedText += "\\nLeftrightarrow ";
                        break;
                    case '↼':
                        analizedText += "\\leftharpoonup ";
                        break;
                    case '↽':
                        analizedText += "\\leftharpoondown ";
                        break;
                    case '⇀':
                        analizedText += "\\rightharpoonup ";
                        break;
                    case '⇁':
                        analizedText += "\\rightharpoondown ";
                        break;
                    case '↿':
                        analizedText += "\\upharpoonleft ";
                        break;
                    case '↾':
                        analizedText += "\\upharpoonright ";
                        break;
                    case '⇃':
                        analizedText += "\\downharpoonleft ";
                        break;
                    case '⇂':
                        analizedText += "\\downharpoonright ";
                        break;
                    case '⇋':
                        analizedText += "\\leftrightharpoons ";
                        break;
                    case '⇌':
                        analizedText += "\\rightleftharpoons ";
                        break;
                    case '↦':
                        analizedText += "\\mapsto ";
                        break;
                    case '↩':
                        analizedText += "\\hookleftarrow ";
                        break;
                    case '↪':
                        analizedText += "\\hookrightarrow ";
                        break;

                    // NEGÁLT RELÁCIÓS JELEK ////////////////////////////////////////////////

                    case '≮':
                        analizedText += "\\nless ";
                        break;
                    case '≯':
                        analizedText += "\\ngtr ";
                        break;
                    case '≰':
                        analizedText += "\\nleq ";
                        break;
                    case '≱':
                        analizedText += "\\ngeq ";
                        break;
                    case '≨':
                        analizedText += "\\lneqq ";
                        break;
                    case '≩':
                        analizedText += "\\gneqq ";
                        break;
                    case '≢':
                        analizedText += "\\not \\equiv ";
                        break;
                    case '≉':
                        analizedText += "\\not \\approx ";
                        break;
                    case '≁':
                        analizedText += "\\not \\sim ";
                        break;
                    case '≇':
                        analizedText += "\\ncong ";
                        break;
                    case '⊀':
                        analizedText += "\\nprec ";
                        break;
                    case '⊁':
                        analizedText += "\\nsucc ";
                        break;
                    case '⋠':
                        analizedText += "\\npreceq ";
                        break;
                    case '⋡':
                        analizedText += "\\nsucceq ";
                        break;
                    case '⊄':
                        analizedText += "\\not \\subset ";
                        break;
                    case '⊅':
                        analizedText += "\\not \\supset ";
                        break;
                    case '⊈':
                        analizedText += "\\nsubseteq ";
                        break;
                    case '⊉':
                        analizedText += "\\nsupseteq ";
                        break;
                    case '⊊':
                        analizedText += "\\subsetneq ";
                        break;
                    case '⊋':
                        analizedText += "\\supsetneq ";
                        break;
                    case '∤':
                        analizedText += "\\nmid ";
                        break;
                    case '∦':
                        analizedText += "\\nparallel ";
                        break;
                    case '⊬':
                        analizedText += "\\nvdash ";
                        break;
                    case '⊭':
                        analizedText += "\\nvDash ";
                        break;
                    case '⊮':
                        analizedText += "\\nVdash";
                        break;
                    case '⊯':
                        analizedText += "\\nVDash ";
                        break;

                    // SIMA KARAKTER ////////////////////////////////////////////////

                    default:
                        analizedText += text[i];
                        break;
                }
            }

            return analizedText;
        }

    #endregion

    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace K
{
    /// <summary>
    /// Ebben a dial�gusablakban �ll�thatja be a felhaszn�l�, hogy
    /// h�ny el�gaz�sb�l �lljon a T�bb�g� el�gaz�s.
    /// </summary>
    public partial class NumberOfBranchesDialog : Form
    {
        /// <summary>
        /// Megadja, vagy be�ll�tja a dial�gusablakban be�ll�tott �gak sz�m�t.
        /// </summary>
        public int NumberOfBranches
        {
            get { return ((int)branchNumericUpDown.Value); }
            set { branchNumericUpDown.Value = value; }
        }

        public NumberOfBranchesDialog()
        {
            InitializeComponent();
        }
    }
}
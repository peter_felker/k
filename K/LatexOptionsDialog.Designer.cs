namespace K
{
    partial class LatexOptionsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean UpRect any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mathModRadioButton = new System.Windows.Forms.RadioButton();
            this.textModRadioButton = new System.Windows.Forms.RadioButton();
            this.fontGroup = new System.Windows.Forms.GroupBox();
            this.defaultRadioButton = new System.Windows.Forms.RadioButton();
            this.typewriterStyleRadioButton = new System.Windows.Forms.RadioButton();
            this.sansSerifStyleRadioButton = new System.Windows.Forms.RadioButton();
            this.romanStyleRadioButton = new System.Windows.Forms.RadioButton();
            this.pathTextBox = new System.Windows.Forms.TextBox();
            this.pathLabel = new System.Windows.Forms.Label();
            this.browseButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.saveButton = new System.Windows.Forms.Button();
            this.sizeGroup = new System.Windows.Forms.GroupBox();
            this.heightUnitNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.heightUnitLabel = new System.Windows.Forms.Label();
            this.struktogramWidthNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.struktogramWidthLabel = new System.Windows.Forms.Label();
            this.modGroupBox = new System.Windows.Forms.GroupBox();
            this.otherGroup = new System.Windows.Forms.GroupBox();
            this.onlyStruktogramCodeCheckBox = new System.Windows.Forms.CheckBox();
            this.showTitleCheckBox = new System.Windows.Forms.CheckBox();
            this.characterSizeLabel = new System.Windows.Forms.Label();
            this.thickLinesCheckBox = new System.Windows.Forms.CheckBox();
            this.characterSizeComboBox = new System.Windows.Forms.ComboBox();
            this.fontGroup.SuspendLayout();
            this.sizeGroup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.heightUnitNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.struktogramWidthNumericUpDown)).BeginInit();
            this.modGroupBox.SuspendLayout();
            this.otherGroup.SuspendLayout();
            this.SuspendLayout();
            // 
            // mathModRadioButton
            // 
            this.mathModRadioButton.AutoSize = true;
            this.mathModRadioButton.Location = new System.Drawing.Point(6, 19);
            this.mathModRadioButton.Name = "mathModRadioButton";
            this.mathModRadioButton.Size = new System.Drawing.Size(105, 17);
            this.mathModRadioButton.TabIndex = 0;
            this.mathModRadioButton.Text = "Matematikai m�d";
            this.mathModRadioButton.UseVisualStyleBackColor = true;
            // 
            // textModRadioButton
            // 
            this.textModRadioButton.AutoSize = true;
            this.textModRadioButton.Location = new System.Drawing.Point(6, 45);
            this.textModRadioButton.Name = "textModRadioButton";
            this.textModRadioButton.Size = new System.Drawing.Size(95, 17);
            this.textModRadioButton.TabIndex = 1;
            this.textModRadioButton.Text = "Sz�veges m�d";
            this.textModRadioButton.UseVisualStyleBackColor = true;
            this.textModRadioButton.CheckedChanged += new System.EventHandler(this.textModRadioButton_CheckedChanged);
            // 
            // fontGroup
            // 
            this.fontGroup.Controls.Add(this.defaultRadioButton);
            this.fontGroup.Controls.Add(this.typewriterStyleRadioButton);
            this.fontGroup.Controls.Add(this.sansSerifStyleRadioButton);
            this.fontGroup.Controls.Add(this.romanStyleRadioButton);
            this.fontGroup.Location = new System.Drawing.Point(106, 45);
            this.fontGroup.Name = "fontGroup";
            this.fontGroup.Size = new System.Drawing.Size(116, 113);
            this.fontGroup.TabIndex = 2;
            this.fontGroup.TabStop = false;
            this.fontGroup.Text = "Font";
            // 
            // defaultRadioButton
            // 
            this.defaultRadioButton.AutoSize = true;
            this.defaultRadioButton.Location = new System.Drawing.Point(6, 19);
            this.defaultRadioButton.Name = "defaultRadioButton";
            this.defaultRadioButton.Size = new System.Drawing.Size(97, 17);
            this.defaultRadioButton.TabIndex = 3;
            this.defaultRadioButton.TabStop = true;
            this.defaultRadioButton.Text = "Alap�rtelmezett";
            this.defaultRadioButton.UseVisualStyleBackColor = true;
            // 
            // typewriterStyleRadioButton
            // 
            this.typewriterStyleRadioButton.AutoSize = true;
            this.typewriterStyleRadioButton.Location = new System.Drawing.Point(6, 88);
            this.typewriterStyleRadioButton.Name = "typewriterStyleRadioButton";
            this.typewriterStyleRadioButton.Size = new System.Drawing.Size(103, 17);
            this.typewriterStyleRadioButton.TabIndex = 2;
            this.typewriterStyleRadioButton.TabStop = true;
            this.typewriterStyleRadioButton.Text = "Typewriter family";
            this.typewriterStyleRadioButton.UseVisualStyleBackColor = true;
            // 
            // sansSerifStyleRadioButton
            // 
            this.sansSerifStyleRadioButton.AutoSize = true;
            this.sansSerifStyleRadioButton.Location = new System.Drawing.Point(6, 65);
            this.sansSerifStyleRadioButton.Name = "sansSerifStyleRadioButton";
            this.sansSerifStyleRadioButton.Size = new System.Drawing.Size(100, 17);
            this.sansSerifStyleRadioButton.TabIndex = 1;
            this.sansSerifStyleRadioButton.TabStop = true;
            this.sansSerifStyleRadioButton.Text = "Sans serif family";
            this.sansSerifStyleRadioButton.UseVisualStyleBackColor = true;
            // 
            // romanStyleRadioButton
            // 
            this.romanStyleRadioButton.AutoSize = true;
            this.romanStyleRadioButton.Location = new System.Drawing.Point(6, 42);
            this.romanStyleRadioButton.Name = "romanStyleRadioButton";
            this.romanStyleRadioButton.Size = new System.Drawing.Size(88, 17);
            this.romanStyleRadioButton.TabIndex = 0;
            this.romanStyleRadioButton.TabStop = true;
            this.romanStyleRadioButton.Text = "Roman family";
            this.romanStyleRadioButton.UseVisualStyleBackColor = true;
            // 
            // pathTextBox
            // 
            this.pathTextBox.Location = new System.Drawing.Point(18, 195);
            this.pathTextBox.Name = "pathTextBox";
            this.pathTextBox.Size = new System.Drawing.Size(384, 20);
            this.pathTextBox.TabIndex = 4;
            // 
            // pathLabel
            // 
            this.pathLabel.AutoSize = true;
            this.pathLabel.Location = new System.Drawing.Point(15, 179);
            this.pathLabel.Name = "pathLabel";
            this.pathLabel.Size = new System.Drawing.Size(59, 13);
            this.pathLabel.TabIndex = 5;
            this.pathLabel.Text = "El�r�si �t  :";
            // 
            // browseButton
            // 
            this.browseButton.Location = new System.Drawing.Point(408, 193);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(74, 23);
            this.browseButton.TabIndex = 6;
            this.browseButton.Text = "Tall�z�s...";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(321, 221);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 7;
            this.cancelButton.Text = "M�gse";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(108, 221);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 8;
            this.saveButton.Text = "Ment�s";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // sizeGroup
            // 
            this.sizeGroup.Controls.Add(this.heightUnitNumericUpDown);
            this.sizeGroup.Controls.Add(this.heightUnitLabel);
            this.sizeGroup.Controls.Add(this.struktogramWidthNumericUpDown);
            this.sizeGroup.Controls.Add(this.struktogramWidthLabel);
            this.sizeGroup.Location = new System.Drawing.Point(248, 12);
            this.sizeGroup.Name = "sizeGroup";
            this.sizeGroup.Size = new System.Drawing.Size(246, 66);
            this.sizeGroup.TabIndex = 9;
            this.sizeGroup.TabStop = false;
            this.sizeGroup.Text = "M�ret";
            // 
            // heightUnitNumericUpDown
            // 
            this.heightUnitNumericUpDown.Location = new System.Drawing.Point(136, 37);
            this.heightUnitNumericUpDown.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.heightUnitNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.heightUnitNumericUpDown.Name = "heightUnitNumericUpDown";
            this.heightUnitNumericUpDown.Size = new System.Drawing.Size(95, 20);
            this.heightUnitNumericUpDown.TabIndex = 3;
            this.heightUnitNumericUpDown.Value = new decimal(new int[] {
            18,
            0,
            0,
            0});
            // 
            // heightUnitLabel
            // 
            this.heightUnitLabel.AutoSize = true;
            this.heightUnitLabel.Location = new System.Drawing.Point(34, 39);
            this.heightUnitLabel.Name = "heightUnitLabel";
            this.heightUnitLabel.Size = new System.Drawing.Size(96, 13);
            this.heightUnitLabel.TabIndex = 2;
            this.heightUnitLabel.Text = "Magass�gegys�g :";
            // 
            // struktogramWidthNumericUpDown
            // 
            this.struktogramWidthNumericUpDown.Location = new System.Drawing.Point(136, 14);
            this.struktogramWidthNumericUpDown.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.struktogramWidthNumericUpDown.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.struktogramWidthNumericUpDown.Name = "struktogramWidthNumericUpDown";
            this.struktogramWidthNumericUpDown.Size = new System.Drawing.Size(95, 20);
            this.struktogramWidthNumericUpDown.TabIndex = 1;
            this.struktogramWidthNumericUpDown.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // struktogramWidthLabel
            // 
            this.struktogramWidthLabel.AutoSize = true;
            this.struktogramWidthLabel.Location = new System.Drawing.Point(6, 16);
            this.struktogramWidthLabel.Name = "struktogramWidthLabel";
            this.struktogramWidthLabel.Size = new System.Drawing.Size(125, 13);
            this.struktogramWidthLabel.TabIndex = 0;
            this.struktogramWidthLabel.Text = "Struktogram sz�less�ge :";
            // 
            // modGroupBox
            // 
            this.modGroupBox.Controls.Add(this.textModRadioButton);
            this.modGroupBox.Controls.Add(this.mathModRadioButton);
            this.modGroupBox.Controls.Add(this.fontGroup);
            this.modGroupBox.Location = new System.Drawing.Point(12, 12);
            this.modGroupBox.Name = "modGroupBox";
            this.modGroupBox.Size = new System.Drawing.Size(230, 164);
            this.modGroupBox.TabIndex = 10;
            this.modGroupBox.TabStop = false;
            this.modGroupBox.Text = "M�d";
            // 
            // otherGroup
            // 
            this.otherGroup.Controls.Add(this.onlyStruktogramCodeCheckBox);
            this.otherGroup.Controls.Add(this.showTitleCheckBox);
            this.otherGroup.Controls.Add(this.characterSizeLabel);
            this.otherGroup.Controls.Add(this.thickLinesCheckBox);
            this.otherGroup.Controls.Add(this.characterSizeComboBox);
            this.otherGroup.Location = new System.Drawing.Point(248, 84);
            this.otherGroup.Name = "otherGroup";
            this.otherGroup.Size = new System.Drawing.Size(246, 92);
            this.otherGroup.TabIndex = 11;
            this.otherGroup.TabStop = false;
            this.otherGroup.Text = "Egy�b";
            // 
            // onlyStruktogramCodeCheckBox
            // 
            this.onlyStruktogramCodeCheckBox.AutoSize = true;
            this.onlyStruktogramCodeCheckBox.Location = new System.Drawing.Point(30, 69);
            this.onlyStruktogramCodeCheckBox.Name = "onlyStruktogramCodeCheckBox";
            this.onlyStruktogramCodeCheckBox.Size = new System.Drawing.Size(184, 17);
            this.onlyStruktogramCodeCheckBox.TabIndex = 2;
            this.onlyStruktogramCodeCheckBox.Text = "Csak struktogram k�d gener�l�sa";
            this.onlyStruktogramCodeCheckBox.UseVisualStyleBackColor = true;
            // 
            // showTitleCheckBox
            // 
            this.showTitleCheckBox.AutoSize = true;
            this.showTitleCheckBox.Location = new System.Drawing.Point(6, 45);
            this.showTitleCheckBox.Name = "showTitleCheckBox";
            this.showTitleCheckBox.Size = new System.Drawing.Size(114, 17);
            this.showTitleCheckBox.TabIndex = 1;
            this.showTitleCheckBox.Text = "C�m megjelen�t�se";
            this.showTitleCheckBox.UseVisualStyleBackColor = true;
            // 
            // characterSizeLabel
            // 
            this.characterSizeLabel.AutoSize = true;
            this.characterSizeLabel.Location = new System.Drawing.Point(27, 21);
            this.characterSizeLabel.Name = "characterSizeLabel";
            this.characterSizeLabel.Size = new System.Drawing.Size(61, 13);
            this.characterSizeLabel.TabIndex = 1;
            this.characterSizeLabel.Text = "Bet�m�ret :";
            // 
            // thickLinesCheckBox
            // 
            this.thickLinesCheckBox.AutoSize = true;
            this.thickLinesCheckBox.Location = new System.Drawing.Point(121, 45);
            this.thickLinesCheckBox.Name = "thickLinesCheckBox";
            this.thickLinesCheckBox.Size = new System.Drawing.Size(119, 17);
            this.thickLinesCheckBox.TabIndex = 0;
            this.thickLinesCheckBox.Text = "Vastag�tott vonalak";
            this.thickLinesCheckBox.UseVisualStyleBackColor = true;
            // 
            // characterSizeComboBox
            // 
            this.characterSizeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.characterSizeComboBox.FormattingEnabled = true;
            this.characterSizeComboBox.Items.AddRange(new object[] {
            "Tiny",
            "Script Size",
            "Footnote Size",
            "Small",
            "Normal",
            "Large",
            "Larger",
            "Largest",
            "Huge",
            "Hugger"});
            this.characterSizeComboBox.Location = new System.Drawing.Point(94, 18);
            this.characterSizeComboBox.Name = "characterSizeComboBox";
            this.characterSizeComboBox.Size = new System.Drawing.Size(121, 21);
            this.characterSizeComboBox.TabIndex = 0;
            // 
            // LatexOptionsDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(508, 252);
            this.Controls.Add(this.otherGroup);
            this.Controls.Add(this.modGroupBox);
            this.Controls.Add(this.sizeGroup);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.browseButton);
            this.Controls.Add(this.pathLabel);
            this.Controls.Add(this.pathTextBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "LatexOptionsDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "LaTeX be�ll�t�sok";
            this.fontGroup.ResumeLayout(false);
            this.fontGroup.PerformLayout();
            this.sizeGroup.ResumeLayout(false);
            this.sizeGroup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.heightUnitNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.struktogramWidthNumericUpDown)).EndInit();
            this.modGroupBox.ResumeLayout(false);
            this.modGroupBox.PerformLayout();
            this.otherGroup.ResumeLayout(false);
            this.otherGroup.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton mathModRadioButton;
        private System.Windows.Forms.RadioButton textModRadioButton;
        private System.Windows.Forms.GroupBox fontGroup;
        private System.Windows.Forms.RadioButton typewriterStyleRadioButton;
        private System.Windows.Forms.RadioButton sansSerifStyleRadioButton;
        private System.Windows.Forms.RadioButton romanStyleRadioButton;
        private System.Windows.Forms.TextBox pathTextBox;
        private System.Windows.Forms.Label pathLabel;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.GroupBox sizeGroup;
        private System.Windows.Forms.NumericUpDown struktogramWidthNumericUpDown;
        private System.Windows.Forms.Label struktogramWidthLabel;
        private System.Windows.Forms.NumericUpDown heightUnitNumericUpDown;
        private System.Windows.Forms.Label heightUnitLabel;
        private System.Windows.Forms.GroupBox modGroupBox;
        private System.Windows.Forms.GroupBox otherGroup;
        private System.Windows.Forms.Label characterSizeLabel;
        private System.Windows.Forms.ComboBox characterSizeComboBox;
        private System.Windows.Forms.CheckBox thickLinesCheckBox;
        private System.Windows.Forms.RadioButton defaultRadioButton;
        private System.Windows.Forms.CheckBox showTitleCheckBox;
        private System.Windows.Forms.CheckBox onlyStruktogramCodeCheckBox;

    }
}
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace K
{
    /// <summary>
    /// Mikor r�klikkel a felhaszn�l� a LaTeX gombra, akkor egy ilyen dial�gus ablak ugrik el�.
    /// </summary>
    public partial class LatexOptionsDialog : Form
    {

    #region V�ltoz�k, property-k, konstruktor

        object parent;

        /// <summary>
        /// Megadja vagy be�ll�tja a sz�l� objektumot.
        /// </summary>
        public object Parent
        {
            get { return parent; }
            set { this.parent = value; }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja, az ablakban be�ll�tott LaTeX opci�kat.
        /// </summary>
        public LatexOptions LatexOptions
        {
            get
            {
                LatexOptions ret = new LatexOptions();

                ret.Font = this.Font;
                ret.Mode = this.Mode;
                ret.StruktogramWidth = this.StruktogramWidth;
                ret.HeightUnit = this.HeightUnit;
                ret.CharacterSize = this.CharacterSize;
                ret.ThickLines = this.ThickLines;
                ret.ShowTitle = this.ShowTitle;
                ret.OnlyStruktogramCode = this.OnlyStruktogramCode;

                return ret;
            }
            set
            {
                Font = value.Font;
                Mode = value.Mode;
                StruktogramWidth = value.StruktogramWidth;
                HeightUnit = value.HeightUnit;
                CharacterSize = value.CharacterSize;
                ThickLines = value.ThickLines;
                ShowTitle = value.ShowTitle;
                OnlyStruktogramCode = value.OnlyStruktogramCode;
            }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja az el�r�si utat, ahova a LaTeX f�jlt szeretn�nk elmenteni.
        /// </summary>
        public string FileName
        {
            get { return pathTextBox.Text; }
            set { pathTextBox.Text = value; }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja a Latex m�dot.
        /// </summary>
        public LatexMode Mode
        {
            get
            {
                if (mathModRadioButton.Checked)
                {
                    return LatexMode.Math;
                }
                else
                {
                    return LatexMode.Text;
                }
            }
            set
            {
                switch (value)
                {
                    case LatexMode.Text: textModRadioButton.Checked = true; break;
                    case LatexMode.Math:
                    default: mathModRadioButton.Checked = true; break;
                }
            }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja a Latex fontot.
        /// </summary>
        public LatexFont Font
        {
            get
            {
                if (romanStyleRadioButton.Checked)
                {
                    return LatexFont.Roman;
                }
                else
                {
                    if (sansSerifStyleRadioButton.Checked)
                    {
                        return LatexFont.SansSerif;
                    }
                    else
                    {
                        if (typewriterStyleRadioButton.Checked)
                        {
                            return LatexFont.Typewriter;
                        }
                        else
                        {
                            return LatexFont.Default;
                        }
                    }
                }
            }

            set
            {
                switch (value)
                {
                    case LatexFont.Roman: romanStyleRadioButton.Checked = true; break;
                    case LatexFont.SansSerif: sansSerifStyleRadioButton.Checked = true; break;
                    case LatexFont.Typewriter: typewriterStyleRadioButton.Checked = true; break;
                    default: defaultRadioButton.Checked = true; break;
                }
            }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja a struktogram sz�less�g�t.
        /// </summary>
        public int StruktogramWidth
        {
            get { return Convert.ToInt32(struktogramWidthNumericUpDown.Value); }
            set { struktogramWidthNumericUpDown.Value = value; }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja a magass�gegys�get.
        /// </summary>
        public int HeightUnit
        {
            get { return Convert.ToInt32(heightUnitNumericUpDown.Value); }
            set { heightUnitNumericUpDown.Value = value; }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja a bet�m�retet.
        /// </summary>
        public LatexCharacterSize CharacterSize
        {
            get { return ((LatexCharacterSize)characterSizeComboBox.SelectedIndex); }
            set { characterSizeComboBox.SelectedIndex = ((Int32)value); }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja, hogy vastag�tott vonalak legyenek-e a struktogramban.
        /// </summary>
        public bool ThickLines
        {
            get { return thickLinesCheckBox.Checked; }
            set { thickLinesCheckBox.Checked = value; }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja, hogy legyen-e a struktogramnak c�me.
        /// </summary>
        public bool ShowTitle
        {
            get { return showTitleCheckBox.Checked; }
            set { showTitleCheckBox.Checked = value; }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja, hogy csak struktogram k�dot k�sz�tsen-e a gener�tor, vagy
        /// belehelyezzen a k�dba minden olyan utas�t�st, amivel meg lehet n�zni az eredm�nyt.
        /// </summary>
        public bool OnlyStruktogramCode
        {
            get { return onlyStruktogramCodeCheckBox.Checked; }
            set { onlyStruktogramCodeCheckBox.Checked = value; }
        }

        /// <summary>
        /// A LatexOptionsDialog konstruktora.
        /// </summary>
        /// <param name="parent"> A sz�l� objektum. </param>
        public LatexOptionsDialog(object parent)
        {
            InitializeComponent();

            this.parent = parent;

            mathModRadioButton.Checked = true;
            fontGroup.Enabled = false;
            struktogramWidthNumericUpDown.Value = MainWindow.Stm.WidthOfStm;
            heightUnitNumericUpDown.Value = 18;
            characterSizeComboBox.SelectedIndex = 4;
            showTitleCheckBox.Checked = true;

            pathTextBox.Text = Directory.GetCurrentDirectory() + "\\";
        }

    #endregion

    #region Egy�b esem�nykezel�k

        /// <summary>
        /// Ha a Text M�d r�di�gombra kattitunk, akkor el�rhet�v� v�lik a Font csoport,
        /// melyben a felhaszn�l� be�ll�thatja a haszn�lni k�v�nt fontot.
        /// </summary>
        private void textModRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            if (textModRadioButton.Checked)
            {
                fontGroup.Enabled = true;
            }
            else
            {
                fontGroup.Enabled = false;
            }
        }

        /// <summary>
        /// A Tall�z�s... gombra kattintva a felhaszn�l� egy dial�gusablakban kiv�laszthatja
        /// a helyet, ahova el szeretn� menteni a Latex k�dot.
        /// </summary>
        private void browseButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();

            dialog.Title = "Export�l�s LaTeX k�dba";
            dialog.Filter = "�sszes f�jl|*.*";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                pathTextBox.Text = dialog.FileName;
            }
        }

        /// <summary>
        /// A Ment�s gombra kattintva (ha az ellen�rz�sen �tment a megadott f�jl) bez�r�dik a dial�gus ablak.
        /// </summary>
        private void saveButton_Click(object sender, EventArgs e)
        {
            String libraryExistanceTest = pathTextBox.Text.Remove(pathTextBox.Text.LastIndexOf('\\'));
            
            if (!Directory.Exists(libraryExistanceTest))
            {
                MessageBox.Show(this,"A megadott el�r�si �t nem l�tezik", "Hiba!");
            }
            else
            {
                if (pathTextBox.Text.EndsWith("\\"))
                {
                    MessageBox.Show(this,"Nincs megadva f�jln�v!", "Hiba!");
                }
                else
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
        }

    #endregion

    }
}
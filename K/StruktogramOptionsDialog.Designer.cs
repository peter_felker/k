namespace K
{
    partial class StruktogramOptionsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.showTitleCheckBox = new System.Windows.Forms.CheckBox();
            this.setFontButton = new System.Windows.Forms.Button();
            this.fontGroupBox = new System.Windows.Forms.GroupBox();
            this.colorLabel = new System.Windows.Forms.Label();
            this.characterSizeLabel = new System.Windows.Forms.Label();
            this.characterStyleLabel = new System.Windows.Forms.Label();
            this.characterTypeLabel = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.fontGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // showTitleCheckBox
            // 
            this.showTitleCheckBox.AutoSize = true;
            this.showTitleCheckBox.Checked = true;
            this.showTitleCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.showTitleCheckBox.Location = new System.Drawing.Point(12, 12);
            this.showTitleCheckBox.Name = "showTitleCheckBox";
            this.showTitleCheckBox.Size = new System.Drawing.Size(91, 17);
            this.showTitleCheckBox.TabIndex = 0;
            this.showTitleCheckBox.Text = "C�m mutat�sa";
            this.showTitleCheckBox.UseVisualStyleBackColor = true;
            // 
            // setFontButton
            // 
            this.setFontButton.Location = new System.Drawing.Point(65, 76);
            this.setFontButton.Name = "setFontButton";
            this.setFontButton.Size = new System.Drawing.Size(102, 23);
            this.setFontButton.TabIndex = 1;
            this.setFontButton.Text = "Font megad�sa";
            this.setFontButton.UseVisualStyleBackColor = true;
            this.setFontButton.Click += new System.EventHandler(this.setFontButton_Click);
            // 
            // fontGroupBox
            // 
            this.fontGroupBox.Controls.Add(this.colorLabel);
            this.fontGroupBox.Controls.Add(this.characterSizeLabel);
            this.fontGroupBox.Controls.Add(this.characterStyleLabel);
            this.fontGroupBox.Controls.Add(this.characterTypeLabel);
            this.fontGroupBox.Controls.Add(this.setFontButton);
            this.fontGroupBox.Location = new System.Drawing.Point(12, 35);
            this.fontGroupBox.Name = "fontGroupBox";
            this.fontGroupBox.Size = new System.Drawing.Size(247, 104);
            this.fontGroupBox.TabIndex = 2;
            this.fontGroupBox.TabStop = false;
            this.fontGroupBox.Text = "Font";
            // 
            // colorLabel
            // 
            this.colorLabel.AutoSize = true;
            this.colorLabel.Location = new System.Drawing.Point(12, 55);
            this.colorLabel.Name = "colorLabel";
            this.colorLabel.Size = new System.Drawing.Size(35, 13);
            this.colorLabel.TabIndex = 5;
            this.colorLabel.Text = "label4";
            // 
            // characterSizeLabel
            // 
            this.characterSizeLabel.AutoSize = true;
            this.characterSizeLabel.Location = new System.Drawing.Point(12, 42);
            this.characterSizeLabel.Name = "characterSizeLabel";
            this.characterSizeLabel.Size = new System.Drawing.Size(35, 13);
            this.characterSizeLabel.TabIndex = 4;
            this.characterSizeLabel.Text = "label3";
            // 
            // characterStyleLabel
            // 
            this.characterStyleLabel.AutoSize = true;
            this.characterStyleLabel.Location = new System.Drawing.Point(12, 29);
            this.characterStyleLabel.Name = "characterStyleLabel";
            this.characterStyleLabel.Size = new System.Drawing.Size(35, 13);
            this.characterStyleLabel.TabIndex = 3;
            this.characterStyleLabel.Text = "label2";
            // 
            // characterTypeLabel
            // 
            this.characterTypeLabel.AutoSize = true;
            this.characterTypeLabel.Location = new System.Drawing.Point(12, 16);
            this.characterTypeLabel.Name = "characterTypeLabel";
            this.characterTypeLabel.Size = new System.Drawing.Size(35, 13);
            this.characterTypeLabel.TabIndex = 2;
            this.characterTypeLabel.Text = "label1";
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(12, 145);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 3;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(184, 147);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 4;
            this.cancelButton.Text = "M�gse";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // PropertyWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(269, 177);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.fontGroupBox);
            this.Controls.Add(this.showTitleCheckBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "PropertyWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "A struktogram tulajdons�gai";
            this.fontGroupBox.ResumeLayout(false);
            this.fontGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox showTitleCheckBox;
        private System.Windows.Forms.Button setFontButton;
        private System.Windows.Forms.GroupBox fontGroupBox;
        private System.Windows.Forms.Label characterTypeLabel;
        private System.Windows.Forms.Label characterSizeLabel;
        private System.Windows.Forms.Label characterStyleLabel;
        private System.Windows.Forms.Label colorLabel;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
    }
}
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace K
{
    /// <summary>
    /// Ez az oszt�ly megval�s�t egy dial�gusablakot, melyben a felhaszn�l�
    /// be tudja �ll�tani a struktogram tulajdons�gait.
    /// </summary>
    public partial class StruktogramOptionsDialog : Form
    {

    #region V�ltoz�k, property-k, konstruktor

        private Font font;
        private Color fontColor;

        /// <summary>
        /// �sszegy�jti �s visszaadja, vagy be�ll�tja az �sszes struktogram opci�t.
        /// </summary>
        public StruktogramOptions StruktogramOptions
        {
            get { return new StruktogramOptions(this.Font, this.ForeColor, this.ShowTitle); }
            set
            {
                this.Font = value.Font;
                this.FontColor = value.FontColor;
                this.ShowTitle = value.ShowTitle;
            }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja, hogy azt az opci�t, hogy struktogramnak legyen-e c�me.
        /// </summary>
        public bool ShowTitle
        {
            get { return this.showTitleCheckBox.Checked; }
            set { this.showTitleCheckBox.Checked = value; }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja "a struktogram elemeiben haszn�lt font" opci�t.
        /// </summary>
        public Font Font
        {
            get { return font; }
            set
            {
                font = value;

                characterTypeLabel.Text = "Bet�t�pus : " + font.FontFamily.Name;
                characterStyleLabel.Text = "Bet�st�lus : ";

                if (!font.Bold && !font.Italic && !font.Strikeout && !font.Underline)
                {
                    characterStyleLabel.Text += "Norm�l";
                }
                else
                {
                    if (font.Bold)
                    {
                        characterStyleLabel.Text += "F�lk�v�r, ";
                    }
                    if (font.Italic)
                    {
                        characterStyleLabel.Text += "D�lt, ";
                    }
                    if (font.Strikeout)
                    {
                        characterStyleLabel.Text += "�th�zott, ";
                    }
                    if (font.Underline)
                    {
                        characterStyleLabel.Text += "Al�h�zott, ";
                    }
                }

                characterSizeLabel.Text = "Bet�m�ret : " + font.Size.ToString();
            }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja "a struktogram elemeiben haszn�lt font sz�n" opci�t.
        /// </summary>
        public Color FontColor
        {
            get { return fontColor; }
            set
            { 
                fontColor = value;
                colorLabel.Text = "Bet�sz�n : " + fontColor.Name;
            }
        }

        /// <summary>
        /// A konstruktor
        /// </summary>
        public StruktogramOptionsDialog()
        {
            InitializeComponent();

            Font = new Font("Arial Unicode MS", (float)11.25, FontStyle.Regular, GraphicsUnit.Point, 238);
            FontColor = Color.Black;
        }

    #endregion

    #region Be�ll�t�sok

        /// <summary>
        /// El�hoz egy FontDialog ablakot, melyben a felhaszn�l� megv�ltoztathatja a fontot.
        /// </summary>
        private void setFontButton_Click(object sender, EventArgs e)
        {
            FontDialog dialog = new FontDialog();

            dialog.ShowEffects = true;
            dialog.ShowColor = true;
            dialog.Font = this.Font;
            dialog.AllowScriptChange = false;
            dialog.Color = this.FontColor;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                this.Font = dialog.Font;
                this.FontColor = dialog.Color;
            }
        }

    #endregion

    }
}
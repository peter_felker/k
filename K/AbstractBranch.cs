using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace K
{
    /// <summary>
    /// Az el�gaz�sok �soszt�lya.
    /// </summary>
    public abstract class AbstractBranch : ProgramConstructionWithCondition
    {

    #region V�ltoz�k, property-k, konstruktorok, (plusz a hozz�juk tartoz� f�ggv�nyek)

        private int prevSelectionLength;

        /// <summary>
        /// Megadja, hogy az el�gaz�snak h�ny �ga van.
        /// </summary>
        public int Branches
        {
            get { return this.Controls.Count; }
        }

        /// <summary>
        /// Az AbstractBranch oszt�ly konstruktora.
        /// </summary>
        public AbstractBranch() : base()
        {
            defaultProperties();
        }

        /// <summary>
        /// Ezt a konstruktor szerializ�l�skor h�v�dik meg.
        /// </summary>
        /// <param name="text"> Az elem kezd� Text-je. </param>
        /// <param name="size"> Az elem kezd�m�rete. </param>
        /// <param name="location"> Az elem kezd�poz�ci�ja. </param>
        public AbstractBranch(String text, Size size, Point location) : base(text, size, location)
        {
            defaultProperties();
        }

        /// <summary>
        /// A konstruktorok k�z�s tulajdons�gait �ll�tja be ez az elj�r�s.
        /// </summary>
        private void defaultProperties()
        {
            this.MouseDown += new MouseEventHandler(AbstarctBranch_MouseDown);
            this.TextChanged += new EventHandler(AbstractBranch_TextChanged);
            this.LostFocus += new EventHandler(AbstractBranch_LostFocus);
            this.MouseMove += new MouseEventHandler(AbstractBranch_MouseMove);

            prevSelectionLength = 0;
        }

    #endregion

    #region Sarkakban l�v� vonalak kirarajzol�sa

        /// <summary>
        /// Ennek a fel�l�r�s�val, mindig megfognak jelenni a sarkakban l�v� ferde vonalak .
        /// </summary>
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case 15:
                    //this.Invalidate();
                    // Elsz�r megh�vjuk a sz�l� f�ggv�y�t.  
                    base.WndProc(ref m);
                    // V�g�l belerazoljuk a vonalakat.  
                    this.RepaintLines(this.CreateGraphics());
                break;
                default:
                    base.WndProc(ref m);
                break;
            }
        }

        /// <summary>
        /// A sarkakban l�v� ferde vonalak kirajzol�s�ra szolg�l.
        /// (A gyerek oszt�lyok fogj�k majd implement�lni, felt�ve ha sz�ks�ges.)
        /// </summary>
        public virtual void RepaintLines(Graphics g) { }

    #endregion

    #region Sarkakban l�v� vonalak �jrarajzol�sa

        /// <summary>
        /// A sz�veg megv�ltoz�sakor, a ferde vonalb�l elt�nik valamennyi, �s ez�rt
        /// ez a f�ggv�ny �jrarajzolja ezeket.
        /// </summary>
        protected void AbstractBranch_TextChanged(object sender, EventArgs e)
        {
            RepaintLines(this.CreateGraphics());
        }

        /// <summary>
        /// A sz�veg kijel�l�sekor, a ferde vonalb�l elt�nik valamennyi pixel, �s ez�rt
        /// ez a f�ggv�ny �jrarajzolja ezeket.
        /// </summary>
        protected void AbstarctBranch_MouseDown(object sender, MouseEventArgs e)
        {
            RepaintLines(this.CreateGraphics());
        }

        /// <summary>
        /// A sz�veg kijel�l�sekor, a ferde vonalb�l elt�nik valamennyi pixel, �s ez�rt
        /// ez a f�ggv�ny �jrarajzolja ezeket.
        /// </summary>
        protected void AbstractBranch_LostFocus(object sender, EventArgs e)
        {
            if (this.SelectionLength > 0)
            {
                RepaintLines(this.CreateGraphics());
            }
        }

        /// <summary>
        /// A sz�veg kijel�l�sekor, a ferde vonalb�l elt�nik valamennyi pixel, �s ez�rt
        /// ez a f�ggv�ny �jrarajzolja ezeket.
        /// </summary>
        protected void AbstractBranch_MouseMove(object sender, MouseEventArgs e)
        {
            if (prevSelectionLength != SelectionLength)
            {
                RepaintLines(this.CreateGraphics());
                prevSelectionLength = SelectionLength;
            }

            if ((MainWindow.State == MainWindowState.ResizeHorizontal || MainWindow.State == MainWindowState.ResizeVertical))
            {
                RepaintLines(this.CreateGraphics());
            }
        }

    #endregion

    #region M�retet megad� met�dusok

        /// <summary>
        /// Megadja az elemhez tartoz� elemek �sszm�ret�t.
        /// </summary>
        public override Size ElementsSize
        {
            get
            {
                int elementsWidth = 0, elementsHeight = 0;

                for (int i = 0; i < this.Controls.Count; ++i)
                {
                    elementsWidth += this.Controls[i].Size.Width - 1;

                    if (elementsHeight < this.Controls[i].Size.Height)
                    {
                        elementsHeight = this.Controls[i].Size.Height;
                    }
                }
                return new Size(elementsWidth + 1, elementsHeight);
            }
        }

    #endregion

    }
}

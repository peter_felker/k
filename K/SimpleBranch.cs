using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace K
{
    /// <summary>
    /// Egy egyszer� �gat megval�s�t� oszt�ly.
    /// </summary>
    public class SimpleBranch : AbstractBranch
    {

    #region V�ltoz�k, property-k, konstruktorok, (plusz a hozz�juk tartoz� f�ggv�nyek)

        private double widthRate;

        /// <summary>
        /// Megadja, vagy be�ll�tja a SimpleBranch sz�less�g-ar�ny�t.
        /// Ellent�tben a HeightRate-el, a WidthRate-re csak itt van sz�ks�g
        /// mert a SimpleBranch-ekn�l elk�pzelhet�, hogy a sz�less�g�knek is
        /// ar�nyosan kell v�ltozniuk.
        /// </summary>
        public double WidthRate
        {
            get { return widthRate; }
            set { widthRate = value; }
        }

        /// <summary>
        /// A SimpleBranch konstruktora.
        /// </summary>
        public SimpleBranch() : base()
        {
            defaultProperties();
        }

        /// <summary>
        /// Ezt a konstruktor szerializ�l�skor h�v�dik meg.
        /// </summary>
        /// <param name="text"> Az elem kezd� Text-je. </param>
        /// <param name="size"> Az elem kezd�m�rete. </param>
        /// <param name="location"> Az elem kezd�poz�ci�ja. </param>
        public SimpleBranch(String text, Size size, Point location) : base(text, size, location)
        {
            defaultProperties();
        }

        /// <summary>
        /// A konstruktorok k�z�s tulajdons�gait �ll�tja be ez az elj�r�s.
        /// </summary>
        private void defaultProperties()
        {
            this.ParentChanged += new EventHandler(SimpleBranch_ParentChanged);
        }

        private void SimpleBranch_ParentChanged(object sender, EventArgs e)
        {
            // Branch sz�l� eset�n nem szeretn�nk belel�pni a SimpleBranch-be
            // ha megnyomtuk a TAB gombot.
            if (this.Parent is Branch)
            {
                this.TabStop = false;
            }
        }

    #endregion

    #region A sarkakban l�v� vonalak kirajzol�s�nak megval�s�t�sa

        /// <summary>
        /// Ez a f�ggv�ny rajzolja �jra a sarkakban l�v� vonalakat.
        /// </summary>
        public override void RepaintLines(Graphics g)
        {
            int tmpConditionHeight = ((AbstractBranch)Parent).ConditionHeight;

            g.DrawLine(new Pen(Color.Black), new Point(0, tmpConditionHeight - this.Font.Height - 1), new Point(Convert.ToInt32(this.Font.Size), tmpConditionHeight));
        }

   #endregion

    #region A z�ld sz�n� t�glalapok megjelen�t�s�vel kapcsolatos met�dusok, property-k

        /// <summary>
        /// Ez a f�ggv�ny megadja, hogy a kurzor az el�gaz�s fels� r�sz�ben van-e 
        /// </summary>
        public override bool CursorInTheUpperRegion
        {
            get
            {
                Point absPos = this.PointToClient(Cursor.Position);
                return ((absPos.Y >= -6) && (absPos.Y <= ((MultipleBranch)Parent).ConditionHeight / 2 - 1));
            }
        }

        /// <summary>
        /// Ez a f�ggv�ny megadja, hogy a kurzor az el�gaz�s als� r�sz�ben van-e
        /// </summary>
        public override bool CursorInTheLowerRegion
        {
            get
            {
                Point absPos = this.PointToClient(Cursor.Position);
                int tmpConditionHeight = ((MultipleBranch)Parent).ConditionHeight;
                return ((absPos.Y >= tmpConditionHeight / 2) && (absPos.Y <= tmpConditionHeight - 1));
            }
        }

        /// <summary>
        /// L�trehozza a besz�r�st jelz�, z�ld t�glalapokat, ha sz�ks�ges.
        /// </summary>
        protected override void createRectangles()
        {
            try
            {
                // Itt kicsit m�shogy szeretn�nk elj�rni mint az �soszt�lyban
                if (showRectangleCondition)
                {
                    int indexOfPrev = this.Parent.Parent.Controls.IndexOf(this.Parent) - 1,
                        indexOfNext = this.Parent.Parent.Controls.IndexOf(this.Parent) + 1;

                    // Az el�z� elemnek a z�ld t�glalapj�t is elt�ntetj�k
                    if ((this.Parent.Parent is Struktogram && indexOfPrev >= 1) || (!(this.Parent.Parent is Struktogram) && indexOfPrev >= 0))
                    {
                        ((ProgramConstruction)this.Parent.Parent.Controls[indexOfPrev]).CheckAndDrawRectangles();
                    }

                    // Valamint a k�vetkez� elemnek is elt�ntetj�k a z�ld t�glalapj�t
                    if (indexOfNext < this.Parent.Parent.Controls.Count)
                    {
                        ((ProgramConstruction)this.Parent.Parent.Controls[indexOfNext]).CheckAndDrawRectangles();
                    }

                    this.CheckAndDrawRectangles();

                }
            }
            catch (Exception ex) { }
        }

        /// <summary>
        /// Ellen�rzi, hogy SimpleBranch-ben sz�ks�ges-e �jrarajzolni a z�ld t�glalapokat.
        /// Ha az ellen�rz�s sor�n igazat kapott, akkor ki is rajzolja a z�ld t�glalapo(ka)t.
        /// </summary>
        public override void CheckAndDrawRectangles()
        {
            foreach (SimpleBranch pc in ((MultipleBranch)this.Parent).Controls)
            {
                if (((pc.UpRect && !pc.CursorInTheUpperRegion) || (pc.DownRect && !pc.CursorInTheLowerRegion) ||
                     (!pc.UpRect && pc.CursorInTheUpperRegion) || (!pc.DownRect && pc.CursorInTheLowerRegion) ||
                       pc.Leaving) && !pc.Selected)
                {
                    pc.RepaintRectangles(pc.CreateGraphics());
                }
            }
        }

        /// <summary>
        /// Kirajzolja a z�ld t�glalapokat, ha sz�ks�ges.
        /// </summary>
        public override void RepaintRectangles(Graphics g)
        {
            int tmpConditionHeight = ((MultipleBranch)Parent).ConditionHeight,
                indexOfThis = ((MultipleBranch)Parent).Controls.IndexOf(this),
                parentControlsCount = ((MultipleBranch)Parent).Controls.Count;

            // Ezzekkel a logikai v�ltoz�kkal jelezz�k, hogy al�l vagy fel�l van-e jelz�s
            upRect = downRect = false;

            //El�sz�r �jrarajzoljuk a vez�rl�t
            this.Refresh();

            // Fels�, z�ld sz�n� jelz�s, mely a besz�r�sn�l jelzi, hogy �ppen hova sz�r�dna be a kiv�lasztott elem
            if (MainWindow.State != MainWindowState.Normal && !Leaving && CursorInTheUpperRegion)
            {
                // Az els� elemn�l elkezd�dik a t�glalap
                if (indexOfThis == 0)
                {
                    g.DrawLine(new Pen(rectColor), new Point(2, 2), new Point(2, 9));
                    g.DrawLine(new Pen(rectColor), new Point(2, 2), new Point(this.Size.Width, 2));
                }

                // A k�ztes elemeken folytat�dik
                if (indexOfThis > 0 && indexOfThis < parentControlsCount - 1)
                {
                    g.DrawLine(new Pen(rectColor), new Point(0, 2), new Point(this.Size.Width, 2));
                }

                // Az utols� elemen pedig lez�rul a t�glalap
                if (indexOfThis == parentControlsCount - 1)
                {
                    g.DrawLine(new Pen(rectColor), new Point(0, 2), new Point(this.Size.Width - 3, 2));
                    g.DrawLine(new Pen(rectColor), new Point(this.Size.Width - 3, 2), new Point(this.Size.Width - 3, 9));
                }

                upRect = true;
             }
            
             // Als�, z�ld sz�n� jelz�s, mely a besz�r�sn�l jelzi, hogy �ppen hova sz�r�dna be a kiv�lasztott elem
             if (MainWindow.State != MainWindowState.Normal && !Leaving && CursorInTheLowerRegion)
             {
                // Az els� elemn�l elkezd�dik a t�glalap
                if (indexOfThis == 0)
                {
                    g.DrawLine(new Pen(rectColor), new Point(2, tmpConditionHeight - 9), new Point(2, tmpConditionHeight - 2));
                    g.DrawLine(new Pen(rectColor), new Point(2, tmpConditionHeight - 2), new Point(this.Size.Width, tmpConditionHeight - 2));
                }

                // A k�ztes elemeken folytat�dik
                if (indexOfThis > 0 && indexOfThis < parentControlsCount - 1)
                {
                    g.DrawLine(new Pen(rectColor), new Point(0, tmpConditionHeight - 2), new Point(this.Size.Width, tmpConditionHeight - 2));
                }

                // Az utols� elemen pedig lez�rul a t�glalap
                if (indexOfThis == parentControlsCount - 1)
                {
                    g.DrawLine(new Pen(rectColor), new Point(0, tmpConditionHeight - 2), new Point(this.Size.Width - 3, tmpConditionHeight - 2));
                    g.DrawLine(new Pen(rectColor), new Point(this.Size.Width - 3, tmpConditionHeight - 2), new Point(this.Size.Width - 3, tmpConditionHeight - 9));
                }

                downRect = true;
             }

             Leaving = false;
        }

        /// <summary>
        /// Az elemb�l val� t�voz�skor elt�nteti a z�ld t�glalapokat.
        /// </summary>
        public override void HideRectangles()
        {
            if (showRectangleCondition)
            {
                int indexOfPrev = this.Parent.Parent.Controls.IndexOf(this.Parent) - 1,
                    indexOfNext = this.Parent.Parent.Controls.IndexOf(this.Parent) + 1;

                // Az el�z� elemnek a z�ld t�glalapj�t is elt�ntetj�k
                // Itt m�gegy m�lys�get fentebb kell menn�nk a MultipleBranch miatt
                if ((this.Parent.Parent is Struktogram && indexOfPrev >= 1) || (!(this.Parent.Parent is Struktogram) && indexOfPrev >= 0))
                {
                    ((ProgramConstruction)this.Parent.Parent.Controls[indexOfPrev]).Leaving = true;
                    ((ProgramConstruction)this.Parent.Parent.Controls[indexOfPrev]).RepaintRectangles(((ProgramConstruction)this.Parent.Parent.Controls[indexOfPrev]).CreateGraphics());
                }

                // Valamint a k�vetkez� elemnek is elt�ntetj�k a z�ld t�glalapj�t
                // Itt m�gegy m�lys�get fentebb kell menn�nk a MultipleBranch miatt
                if ((indexOfNext < this.Parent.Parent.Controls.Count))
                {
                    ((ProgramConstruction)this.Parent.Parent.Controls[indexOfNext]).Leaving = true;
                    ((ProgramConstruction)this.Parent.Parent.Controls[indexOfNext]).RepaintRectangles(((ProgramConstruction)this.Parent.Parent.Controls[indexOfNext]).CreateGraphics());
                }

                // V�g�l az �sszes SimpleBranch-b�l elt�ntetj�k a z�ld t�glalapokat
                foreach (SimpleBranch sb in this.Parent.Controls)
                {
                    sb.Leaving = true;
                    sb.RepaintRectangles(sb.CreateGraphics());
                }
            }
        }

    #endregion

    #region M�retet megad� met�dusok

        /// <summary>
        /// Megadja az elemhez tartoz� elemek �sszm�ret�t.
        /// </summary>
        public override Size ElementsSize
        {
            get
            {
                int elementsWidth = 0, elementsHeight = 0;

                for (int i = 0; i < this.Controls.Count; ++i)
                {
                    // Sz�less�gn�l megfelel a sima m�ret
                    elementsHeight += this.Controls[i].Size.Height - 1;

                    if (elementsWidth < this.Controls[i].Size.Width)
                    {
                        elementsWidth = this.Controls[i].Size.Width;
                    }
                }
                return new Size(elementsWidth, elementsHeight + 1);
            }
        }

        /// <summary>
        /// Megadja a programkonstrukci� aj�nlott m�ret�t.
        /// </summary>
        public override Size RecommendedSize
        {
            get
            {
                int recommendedWidth = 0, recommendedHeight = 0, minWidth, prefAndMargWidth, calculatedWidth;
                Size recommendedSize;

                for (int i = 0; i < this.Controls.Count; ++i)
                {
                    recommendedSize = ((ProgramConstruction)this.Controls[i]).RecommendedSize;

                    // Az�rt kell a -1, mert a hat�rol� vonalakat nem szeretn�nk belesz�molni k�tszer
                    recommendedHeight += recommendedSize.Height - 1;

                    // A sz�legg�gn�l a legnagyobbra vagyunk kiv�ncsiak
                    if (recommendedWidth < recommendedSize.Width)
                    {
                        recommendedWidth = recommendedSize.Width;
                    }
                }

                // Viszont az utols� elem nem �rinkezik ut�nna semmivel, �gy ebb�l
                // nem kell kivonni 1-et, ez�rt itt hozz�adunk 1-et.
                recommendedHeight += 1;

                // A minim�lis sz�less�g
                minWidth = Convert.ToInt32(this.Font.Size) * 6;

                // A sz�m�tott sz�less�g
                prefAndMargWidth = this.PreferredSize.Width + LeftRightMargin;
                calculatedWidth = prefAndMargWidth < recommendedWidth ?
                                  recommendedWidth : prefAndMargWidth;

                return new Size(calculatedWidth < minWidth ? minWidth : calculatedWidth, this.ConditionHeight + recommendedHeight);
            }
        }

        /// <summary>
        /// Ez a f�ggv�ny kisz�m�tja, hogy mekkora a felt�tel magass�ga.
        /// </summary>
        /// <returns> A felt�tel magass�ga. </returns>
        public override int ConditionHeight
        {
            get
            {
                try
                {
                    // Mivel a k�tf�le sz�l� lehet (MultipleBranch �s Branch), �gy
                    // k�tf�lek�ppen is kell elj�rnunk
                    if (Parent is Branch)
                    {
                        // Branch sz�l� eset�n ne jelenjen meg felt�tel
                        return 0;
                    }
                    else
                    {
                        // MultipleBranch eset�n pedig a sz�l�t�l megtudjuk, hogy
                        // melyik elemnek a legnagyobb a magass�ga
                        return ((MultipleBranch)this.Parent).ConditionHeight;
                    }
                } catch (Exception ex) { return 0; }
            }
        }

    #endregion

    #region Egy�b esem�nykezel�k : A ProgramConstruction_MouseClick fel�ldefini�l�sa

        /// <summary>
        /// Eg�rklikkel�s eset�n ez az esem�nykezel� fut le.
        /// Itt kicsit m�shogy kell elj�rnunk mint az �soszt�lyban.
        /// </summary>
        protected override void ProgramConstruction_MouseClick(object sender, MouseEventArgs e)
        {
            // Annyi elt�r�s van az �soszt�lyt�l, hogy egy l�p�ssel feljebb kell menn�nk elem besz�r�sakor
            if (showRectangleCondition)
            {
                if (UpRect)
                {
                    switch (MainWindow.State)
                    {
                        case MainWindowState.BlockInsert: MainWindow.Stm.AddBlock(Parent.Parent, Parent.Parent.Controls.IndexOf(Parent)); break;
                        case MainWindowState.LoopInsert: MainWindow.Stm.AddLoop(Parent.Parent, Parent.Parent.Controls.IndexOf(Parent)); break;
                        case MainWindowState.BranchInsert: MainWindow.Stm.AddBranch(Parent.Parent, Parent.Parent.Controls.IndexOf(Parent)); break;
                        case MainWindowState.MultipleBranchInsert: MainWindow.Stm.AddMultipleBranch(Parent.Parent, Parent.Parent.Controls.IndexOf(Parent)); break;
                        case MainWindowState.Moving: MainWindow.Stm.MoveElement(Parent.Parent, MainWindow.Stm.SelectedElements, Parent.Parent.Controls.IndexOf(Parent)); break;
                    }
                }
                if (DownRect)
                {
                    switch (MainWindow.State)
                    {
                        case MainWindowState.BlockInsert: MainWindow.Stm.AddBlock(Parent.Parent, Parent.Parent.Controls.IndexOf(Parent) + 1); break;
                        case MainWindowState.LoopInsert: MainWindow.Stm.AddLoop(Parent.Parent, Parent.Parent.Controls.IndexOf(Parent) + 1); break;
                        case MainWindowState.BranchInsert: MainWindow.Stm.AddBranch(Parent.Parent, Parent.Parent.Controls.IndexOf(Parent) + 1); break;
                        case MainWindowState.MultipleBranchInsert: MainWindow.Stm.AddMultipleBranch(Parent.Parent, Parent.Parent.Controls.IndexOf(Parent) + 1); break;
                        case MainWindowState.Moving: MainWindow.Stm.MoveElement(Parent.Parent, MainWindow.Stm.SelectedElements, Parent.Parent.Controls.IndexOf(Parent) + 1); break;
                    }
                }
            }
            else
            {
                // Ha a Control nyomva van, akkor a t�bbi SimpleBranch-et is ki kell jel�ln�nk
                if (MainWindow.State == MainWindowState.ControlDown)
                {
                    if (!selected)
                    {
                        ((MultipleBranch)Parent).Select();
                    }
                    else
                    {
                        ((MultipleBranch)Parent).Deselect();
                    }
                }
                else
                {
                    // Ha a Shift nyomva van, akkor a t�bbi SimpleBranch-et is rekurz�van ki kell jel�ln�nk
                    if (MainWindow.State == MainWindowState.ShiftDown)
                    {
                        if (!selected)
                        {
                            ((MultipleBranch)Parent).SelectRecursively();
                        }
                        else
                        {
                            ((MultipleBranch)Parent).DeselectRecursively();
                        }
                    }
                    else
                    {
                        if (MainWindow.State == MainWindowState.MoveMode)
                        {
                            ((MultipleBranch)Parent).SelectRecursively();
                            MainWindow.State = MainWindowState.Moving;
                        }
                        else // V�g�l megh�vjuk az �s esem�nykezel�j�t
                        {
                            base.ProgramConstruction_MouseClick(sender, e);
                        }
                    }
                }
            }
        }

    #endregion

    }
}
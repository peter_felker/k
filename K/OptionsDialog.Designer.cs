namespace K
{
    partial class OptionsDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.backStepsLabel = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.numberOfBackStepsNumericUpDown = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.numberOfBackStepsNumericUpDown)).BeginInit();
            this.SuspendLayout();
            // 
            // backStepsLabel
            // 
            this.backStepsLabel.AutoSize = true;
            this.backStepsLabel.Location = new System.Drawing.Point(12, 9);
            this.backStepsLabel.Name = "backStepsLabel";
            this.backStepsLabel.Size = new System.Drawing.Size(234, 13);
            this.backStepsLabel.TabIndex = 0;
            this.backStepsLabel.Text = "Visszal�p�sek lehets�ges sz�ma (max.: 65535) :";
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(15, 34);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 1;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(255, 34);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "M�gse";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // numberOfBackStepsNumericUpDown
            // 
            this.numberOfBackStepsNumericUpDown.Location = new System.Drawing.Point(251, 7);
            this.numberOfBackStepsNumericUpDown.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.numberOfBackStepsNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numberOfBackStepsNumericUpDown.Name = "numberOfBackStepsNumericUpDown";
            this.numberOfBackStepsNumericUpDown.Size = new System.Drawing.Size(79, 20);
            this.numberOfBackStepsNumericUpDown.TabIndex = 3;
            this.numberOfBackStepsNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // OptionsDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 65);
            this.Controls.Add(this.numberOfBackStepsNumericUpDown);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.backStepsLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "OptionsDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Opci�k";
            ((System.ComponentModel.ISupportInitialize)(this.numberOfBackStepsNumericUpDown)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label backStepsLabel;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.NumericUpDown numberOfBackStepsNumericUpDown;
    }
}
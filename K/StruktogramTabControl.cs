using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Drawing;

namespace K
{
    /// <summary>
    /// Ezen az oszt�lyon lesznek a StruktogramPage-ek, amiken pedig a Struktogrammok lesznek.
    /// </summary>
    public class StruktogramTabControl : TabControl
    {

    #region Konstruktor

        /// <summary>
        /// Az StruktogramTabControl konstruktora.
        /// </summary>
        public StruktogramTabControl() : base()
        {
            this.MouseWheel += new MouseEventHandler(StruktogramTabControl_Scroll);
            this.SelectedIndexChanged += new EventHandler(StruktogramTabControl_SelectedIndexChanged);

            this.KeyUp += new KeyEventHandler(Element.Element_KeyUp);
            this.KeyDown += new KeyEventHandler(Element.Element_KeyDown);
            this.AllowDrop = true;
        }

    #endregion

    #region A StruktogramPage-ek f�l�nek sorrendj�nek megv�ltoztat�sa

        /// <summary>
        /// A DragAndDrop m�velet megval�s�t�sa.
        /// </summary>
        protected override void OnDragOver(System.Windows.Forms.DragEventArgs e)
        {
            base.OnDragOver(e);

            Point pt = new Point(e.X, e.Y);
            // Kliens koordin�t�kra van sz�ks�g�nk.
            pt = PointToClient(pt);

            // A StruktogramPage, amit �ppen megfoktunk
            StruktogramPage hoverPage = GetTabOfStruktogramPage(pt);

            // Biztons�g kedv��rt leellen�rizz�k, hogy nem null-e az objektum
            if (hoverPage != null)
            {
                // Megn�zz�k, hogy t�nyleg StruktogramPage-et fogtunk-e meg
                if (e.Data.GetDataPresent(typeof(StruktogramPage)))
                {
                    e.Effect = DragDropEffects.Move;
                    StruktogramPage dragPage = (StruktogramPage)e.Data.GetData(typeof(StruktogramPage));

                    int dragedPageIndex = this.TabPages.IndexOf(dragPage);
                    int dropLocationIndex = this.TabPages.IndexOf(hoverPage);

                    // Ha ugyanoda pakoljuk, akkor nem csin�lunk semmit
                    if (dragedPageIndex != dropLocationIndex)
                    {
                        ArrayList pages = new ArrayList();

                        // Beletessz�k az �sszes StruktogramPage-et egy t�mbbe.
                        for (int i = 0; i < TabPages.Count; i++)
                        {
                            // Kiv�ve, amit �t akarunk pakolni
                            if (i != dragedPageIndex)
                            {
                                pages.Add(((StruktogramPage)TabPages[i]));
                            }
                        }

                        // Most �ttessz�k a megfelel� helyre
                        pages.Insert(dropLocationIndex, dragPage);

                        // T�ntess�k el az �sszeset
                        TabPages.Clear();

                        // Majd hozzuk vissza
                        TabPages.AddRange((StruktogramPage[])pages.ToArray(typeof(StruktogramPage)));

                        // A v�g�n az �tpakolt elemre helyezz�k a fokuszt
                        SelectedTab = dragPage;
                    }
                }
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        /// <summary>
        /// Ha nyomva van az eg�r gombja az egyik struktogramf�l�n, akkor megkezdi
        /// a DragAndDrop m�veletet.
        /// </summary>
        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);

            Point pt = new Point(e.X, e.Y);
            StruktogramPage sp = GetTabOfStruktogramPage(pt);

            if (sp != null)
            {
                DoDragDrop(sp, DragDropEffects.All);
            }
        }

        /// <summary>
        /// Megkeresi azt a StruktogramPage f�let, mely mag�bafoglalja a megadott pontot.
        /// </summary>
        /// <param name="pt"> Az ellen�rizend� pont .</param>
        /// <returns> A StruktogramPage, melynek a f�le tartalmazza a megadott pontot.</returns>
        private StruktogramPage GetTabOfStruktogramPage(Point pt)
        {
            StruktogramPage sp = null;

            for (int i = 0; i < TabPages.Count; i++)
            {
                if (GetTabRect(i).Contains(pt))
                {
                    sp = ((StruktogramPage)TabPages[i]);
                    break;
                }
            }

            return sp;
        }

    #endregion

    #region Egy�b esem�nykezel�k : Scroll, SelectedIndexChanged

        /// <summary>
        /// A g�rget�st val�s�tja meg.
        /// </summary>
        private void StruktogramTabControl_Scroll(object sender, MouseEventArgs e)
        {
            StruktogramPage.Scroll(((StruktogramPage)this.SelectedTab), e);
        }

        /// <summary>
        /// Ha megv�ltozott a TabControl indexe, akkor �t�ll�tja az aktu�lis struktogrammot
        /// a be�ll�tott index�re.
        /// </summary>
        private void StruktogramTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            MainWindow.State = MainWindowState.Normal;

            if (this.TabPages.Count > 0)
            {
                MainWindow.Stm = ((StruktogramPage)this.Controls[this.SelectedIndex]).Stm;
            }
        }

    #endregion

    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace K
{
    /// <summary>
    /// A k�t�g� el�gaz�st megval�s�t� oszt�ly.
    /// </summary>
    public class Branch : AbstractBranch
    {

    #region Konstruktorok

        /// <summary>
        /// A Branch oszt�ly konstruktora.
        /// </summary>
        public Branch() : base()
        {
            // Beletesz�nk 2 SimpleBranch elemet
            this.Controls.Add(new SimpleBranch());
            this.Controls.Add(new SimpleBranch());
        }

        /// <summary>
        /// Ezt a konstruktor szerializ�l�skor h�v�dik meg.
        /// </summary>
        /// <param name="text"> Az elem kezd� Text-je. </param>
        /// <param name="size"> Az elem kezd�m�rete. </param>
        /// <param name="location"> Az elem kezd�poz�ci�ja. </param>
        public Branch(String text, Size size, Point location) : base(text, size, location) { }

    #endregion

    #region A sarkakban l�v� vonalak kirajzol�s�nak megval�s�t�sa

        /// <summary>
        /// Ez a f�ggv�ny rajzolja �jra a sarkakban l�v� vonalakat.
        /// </summary>
        public override void RepaintLines(Graphics g)
        {
            int tmpConditionHeight = this.ConditionHeight;

            g.DrawLine(new Pen(Color.Black), new Point(0, tmpConditionHeight - this.Font.Height - 1), new Point(Convert.ToInt32(this.Font.Size), tmpConditionHeight));
            g.DrawLine(new Pen(Color.Black), new Point(this.Size.Width - 1, tmpConditionHeight - this.Font.Height - 1), new Point(this.Size.Width - Convert.ToInt32(this.Font.Size) - 1, tmpConditionHeight));
        }

    #endregion

    #region A z�ld sz�n� t�glalapok megjelen�t�s�vel kapcsolatos met�dusok, property-k

        /// <summary>
        /// Kirajzolja a z�ld t�glalapokat, ha sz�ks�ges.
        /// </summary>
        public override void RepaintRectangles(Graphics g)
        {
            int tmpConditionHeight = this.ConditionHeight;

            // Ezzekkel a logikai v�ltoz�kkal jelezz�k, hogy al�l vagy fel�l van-e jelz�s
            upRect = downRect = false;

            //El�sz�r �jrarajzoljuk a vez�rl�t
            this.Refresh();

            // Fels�, z�ld sz�n� jelz�s, mely a besz�r�sn�l jelzi, hogy �ppen hova sz�r�dna be a kiv�lasztott elem
            if (MainWindow.State != MainWindowState.Normal && !Leaving && CursorInTheUpperRegion)
            {
                g.DrawLine(new Pen(rectColor), new Point(2, 2), new Point(2, 9));
                g.DrawLine(new Pen(rectColor), new Point(2, 2), new Point(this.Size.Width - 3, 2));
                g.DrawLine(new Pen(rectColor), new Point(this.Size.Width - 3, 2), new Point(this.Size.Width - 3, 9));

                upRect = true;
            }

            // Als�, z�ld sz�n� jelz�s, mely a besz�r�sn�l jelzi, hogy �ppen hova sz�r�dna be a kiv�lasztott elem
            if (MainWindow.State != MainWindowState.Normal && !Leaving && CursorInTheLowerRegion)
            {
                g.DrawLine(new Pen(rectColor), new Point(2, tmpConditionHeight - 9), new Point(2, tmpConditionHeight - 2));
                g.DrawLine(new Pen(rectColor), new Point(2, tmpConditionHeight - 2), new Point(this.Size.Width - 3, tmpConditionHeight - 2));
                g.DrawLine(new Pen(rectColor), new Point(this.Size.Width - 3, tmpConditionHeight - 2), new Point(this.Size.Width - 3, tmpConditionHeight - 9));

                downRect = true;
            }

            Leaving = false;
        }

        /// <summary>
        /// Megadja, hogy a kurzor, az elem tetej�n (FELS� r�sz�ben) van-e.
        /// </summary>
        public override bool CursorInTheUpperRegion
        {
            get
            {
                Point absPos = this.PointToClient(Cursor.Position);
                return ((absPos.X >= 1) && (absPos.X <= this.Size.Width - 1)) &&
                       ((absPos.Y >= -6) && (absPos.Y <= ConditionHeight / 2 - 1));
            }
        }

        /// <summary>
        /// Megadja, hogy a kurzor, az elem alj�n (ALS� r�sz�ben) van-e.
        /// </summary>
        public override bool CursorInTheLowerRegion
        {
            get
            {
                Point absPos = this.PointToClient(Cursor.Position);
                return ((absPos.X >= 1) && (absPos.X <= this.Size.Width - 1)) &&
                       ((absPos.Y >= ConditionHeight / 2) && (absPos.Y <= ConditionHeight - 1));
            }
        }

    #endregion

    #region M�retet megad� met�dusok

        /// <summary>
        /// Megadja a programkonstrukci� aj�nlott m�ret�t.
        /// </summary>
        public override Size RecommendedSize
        {
            get
            {
                int recommendedWidth = 0, recommendedHeight = 0, minWidth, prefAndMargWidth, calculatedWidth;
                Size recommendedSize;

                for (int i = 0; i < this.Controls.Count; ++i)
                {
                    recommendedSize = ((SimpleBranch)this.Controls[i]).RecommendedSize;
                    // Az�rt kell a -1, mert a hat�rol� vonalakat nem szeretn�nk belesz�molni k�tszer
                    recommendedWidth += recommendedSize.Width - 1;

                    // A magass�gokb�l a legnagyobbra vagyunk kiv�ncsiak
                    if (recommendedHeight < recommendedSize.Height)
                    {
                        recommendedHeight = recommendedSize.Height;
                    }
                }

                // Viszont az utols� elem nem �rinkezik ut�nna semmivel, �gy ebb�l
                // nem kell kivonni 1-et, ez�rt itt hozz�adunk 1-et.
                recommendedWidth += 1;

                // A minim�lis sz�less�g
                minWidth = Convert.ToInt32(this.Font.Size) * 10;

                // A sz�m�tott sz�less�g
                prefAndMargWidth = this.PreferredSize.Width + LeftRightMargin;
                calculatedWidth = prefAndMargWidth < recommendedWidth ?
                                  recommendedWidth : prefAndMargWidth;

                return new Size(calculatedWidth < minWidth ? minWidth : calculatedWidth, this.ConditionHeight + recommendedHeight);
            }
        }

    #endregion

    #region M�ret ellen�rz�s

        /// <summary>
        /// �tm�retez�skor fut le ez az esem�nykezel�. Vigy�z arra, hogy a programkonstrukci�k ne
        /// legyenek kisebbek, mint kellene.
        /// </summary>
        protected override void CheckSize(object sender, EventArgs e)
        {
            // El�gaz�s eset�n kicsit m�shogy kell elj�rnunk, mint a t�bbi programkonstrukci� eset�n.
            if (!stopReducing && parentExist && this.Size.Width < this.PreferredSize.Width + LeftRightMargin)
            {
                stopReducing = true;
                this.Size = this.RecommendedSize;
                MainWindow.Stm.Alignment(this, true);
            }
        }

    #endregion

    }
}
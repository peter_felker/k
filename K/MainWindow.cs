using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;

namespace K
{
    /// <summary>
    /// Állapotok, melyeket felvehet a főablak.
    /// </summary>
    public enum MainWindowState
    {
        /// <summary>
        /// Alaphelyzetben, ebben az állapotban van a program.
        /// </summary>
        Normal,

        /// <summary>
        /// Blokk beszúrása.
        /// </summary>
        BlockInsert,

        /// <summary>
        /// Ciklus beszúrása.
        /// </summary>
        LoopInsert,

        /// <summary>
        /// Elágazás beszúrása.
        /// </summary>
        BranchInsert,

        /// <summary>
        /// Többágú elágazás beszúrása.
        /// </summary>
        MultipleBranchInsert,

        /// <summary>
        /// Le van nyomva a Control gomb.
        /// </summary>
        ControlDown,

        /// <summary>
        /// Le van nyomva a SHIFT gomb.
        /// </summary>
        ShiftDown,
        
        /// <summary>
        /// Horizontális átméretezés.
        /// </summary>
        ResizeHorizontal,
        
        /// <summary>
        /// Vertikális átméretezés.
        /// </summary>
        ResizeVertical,
        
        /// <summary>
        /// Jelzi, ha a kurzor, a programkonstrukció jobb szélén van.
        /// </summary>
        PrepareToResizeHorizontal,

        /// <summary>
        /// Jelzi, ha a kurzor, a programkonstrukció alján van-e.
        /// </summary>
        PrepareToResizeVertical,
        
        /// <summary>
        /// Áthelyező mód.
        /// </summary>
        MoveMode,
        
        /// <summary>
        /// Áthelyező módban a felhasználó kiválasztotta az áthelyezendő elemet.
        /// </summary>
        Moving,
        
        /// <summary>
        /// Deszerializálás.
        /// </summary>
        Deserializing
    }

    /// <summary>
    /// A főablakot megvalósító osztály.
    /// </summary>
    public partial class MainWindow : Form
    {
    
    #region Változók, property-k, konstruktor

        public static event EventHandler StateChanged;
        private static MainWindowState state;
        private static Struktogram stm;
        private static int numberOfPossibleSavedStates;
        private static String programDirectory;
        private bool stopClosing;
        private int clickedPageIndex;
        private LatexOptions oldLatexOptions;

        /// <summary>
        /// Megadja, vagy beállítja, hogy maximálisan hány elmentett állapot létezhet.
        /// </summary>
        public static int NumberOfPossibleSavedStates
        {
            get { return numberOfPossibleSavedStates; }
            set { numberOfPossibleSavedStates = value; }
        }

        /// <summary>
        /// Megadja, vagy beállítja a könyvtárnevet, ahol a program van.
        /// </summary>
        public static String ProgramDirectory
        {
            get { return programDirectory; }
            set { programDirectory = value; }
        }

        /// <summary>
        /// Megadja, vagy beállítja az aktuális struktogrammot.
        /// </summary>
        public static Struktogram Stm
        {
            get { return stm; }
            set { stm = value; }
        }

        /// <summary>
        /// Visszatér az éppen aktuális StruktogramPage-el.
        /// </summary>
        public StruktogramPage StmTabPage
        {
            get { return ((StruktogramPage)stmTabControl.Controls[stmTabControl.SelectedIndex]); }
        }

        /// <summary>
        /// Megadja vagy beállítja a főablak állapotát. Beállítás esetén kiváltódik
        /// a StateChanged esemény.
        /// </summary>
        public static MainWindowState State
        {
            get { return state; }
            set
            {
                if (state != value)
                {
                    state = value;
                    StateChanged(value, new EventArgs());
                }
            }
        }

        /// <summary>
        /// A főablak konstruktora.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            // Meghatározzuk a könyvtárat, melyben a program helyezkedik el
            ProgramDirectory = Directory.GetCurrentDirectory() + "\\";
            numberOfPossibleSavedStates = 101;

            // Létrehozzuk a régebbi állapotok tárolására szolgáló könyvtárat
            try
            {
                Directory.Delete(ProgramDirectory + "tmp", true);
            }
            catch (Exception ex) { }
            Directory.CreateDirectory(ProgramDirectory + "tmp");

            State = MainWindowState.Normal;
            StateChanged += new EventHandler(MainWindowStateChanged);

            stopClosing = false;

            stmTabControl.Location = new Point(editStrip.Location.X + editStrip.Size.Width, menuStrip.Size.Height + itemsStrip.Size.Height);
            stmTabControl.Size = new Size(this.Size.Width - editStrip.Size.Width - 8, this.Size.Height - menuStrip.Size.Height - itemsStrip.Size.Height - 34);

            categoryComboBox.DropDownClosed += new EventHandler(categoryComboBox_DropDownClosed);
            setMenusEnabled(false);
        }

    #endregion

    #region Strip-ek eltüntetése, stmTabControl méretváltozása, alkalmazás infromációk elmentése, betöltése

        /// <summary>
        /// Bizonyos menüpontokat engedélyez, vagy letilt.
        /// </summary>
        /// <param name="value"> Engedélyezés, vagy letiltás </param>
        private void setMenusEnabled(bool value)
        {
            // Nyilván ezt csak akkor kell végrehajtani, ha StruktogramPage-ek száma 0 volt, vagy 0 lett.
            if (stmTabControl.TabPages.Count == 0)
            {
                saveMenuItem.Enabled = saveButton.Enabled = value;
                saveAsMenuItem.Enabled = value;
                saveAllMenuItem.Enabled = saveAllButton.Enabled = value;
                closeMenuItem.Enabled = closeButton.Enabled = value;
                closeAllMenuItem.Enabled = closeAllButton.Enabled = value;
                toImageMenuItem.Enabled = toImageButton.Enabled = value;
                latexMenuItem.Enabled = latexButton.Enabled = value;
                editMenuItem.Enabled = editStrip.Enabled = value;
                redoButton.Enabled = undoButton.Enabled = value;
                struktogramOptionsMenuItem.Enabled = value;
                categoryStrip.Enabled = value;
            }
        }

        /// <summary>
        /// Ha megjelent a főablak, akkor kiolvassa a 'data' nevű fájlból az előző
        /// futtatásnál használt opciókat.
        /// </summary>
        private void MainWindow_Shown(object sender, EventArgs e)
        {
            if (!File.Exists(ProgramDirectory + "data")) // Ha nem létezik, akkor létrehozzuk
            {
                writeAppData();
            }
            else // Egyébként kiolvassuk az adatokat
            {
                readAppData();
            }

            this.LocationChanged += new EventHandler(MainWindow_LocationChanged);
        }

        /// <summary>
        /// Az alkalmazás adatainak kiírása a "data" nevű fájlba.
        /// </summary>
        private void writeAppData()
        {
            try
            {
                // Ha a tálcán van az alkalmazás, akkor ilyen formában ne mentsük el az információkat
                if (this.WindowState != FormWindowState.Minimized)
                {

                    StreamWriter writer = new StreamWriter(ProgramDirectory + "data", false);

                    // Pozíció
                    writer.WriteLine(this.Location.X);
                    writer.WriteLine(this.Location.Y);

                    // Méret
                    writer.WriteLine(this.Size.Width);
                    writer.WriteLine(this.Size.Height);

                    // Ablak állapota, ami itt azt jelenti, hogy pl.: teljes képernyősre vettük-e
                    // az ablakot
                    writer.WriteLine(Convert.ToInt32(this.WindowState));

                    // Látszódnak-e a különböző sávok
                    writer.WriteLine(showItemsMenuItem.Checked);
                    writer.WriteLine(showEditMenuItem.Checked);
                    writer.WriteLine(categoryComboBox.SelectedIndex);

                    // A visszalépések száma
                    writer.WriteLine(NumberOfPossibleSavedStates);

                    // kiírás és végül bezárás.
                    writer.Flush();
                    writer.Close();
                }
            } catch (Exception) { }
        }

        /// <summary>
        /// Kiolvassa a "data" nevű fájlból, az oda elmentett alkalmazás információkat.
        /// </summary>
        private void readAppData()
        {
            StreamReader reader = new StreamReader(ProgramDirectory + "data");

            // Pozíció
            this.Location = new Point(Convert.ToInt32(reader.ReadLine()), Convert.ToInt32(reader.ReadLine()));

            // Méret
            this.Size = new Size(Convert.ToInt32(reader.ReadLine()), Convert.ToInt32(reader.ReadLine()));

            // Ablak állapota, ami itt azt jelenti, hogy pl.: teljes képernyősre vettük-e
            // az ablakot
            this.WindowState = ((FormWindowState)Convert.ToInt32(reader.ReadLine()));

            // Látszódnak-e a különböző sávok
            itemsStrip.Visible = showItemsMenuItem.Checked = Convert.ToBoolean(reader.ReadLine());
            editStrip.Visible = showEditMenuItem.Checked = Convert.ToBoolean(reader.ReadLine());
            categoryComboBox.SelectedIndex = Convert.ToInt32(reader.ReadLine());

            // Visszalépések száma
            NumberOfPossibleSavedStates = Convert.ToInt32(reader.ReadLine());
            
            // Elhelyezzük a dolgokat
            editStripPlacing();
            reader.Close();
        }

        /// <summary>
        ///  itemsStrip elrejtése, vagy mutatása.
        /// </summary>
        private void showItemsMenuItem_Click(object sender, EventArgs e)
        {
            itemsStrip.Visible = showItemsMenuItem.Checked = !showItemsMenuItem.Checked;
            editStripPlacing();
        }

        /// <summary>
        /// editStrip elrejtése, vagy mutatása.
        /// </summary>
        private void showEditMenuItem_Click(object sender, EventArgs e)
        {
            editStrip.Visible = showEditMenuItem.Checked = !showEditMenuItem.Checked;
            editStripPlacing();
        }

        /// <summary>
        /// editStrip objektum elhelyezése, valamint vele együtt
        /// a stmTabControl helyét és méretét is megváltoztatjuk.
        /// </summary>
        private void editStripPlacing()
        {
            //Ha mindkettő lászik
            if (categoryStrip.Visible && itemsStrip.Visible)
            {
                editStrip.Location = new Point(0, itemsStrip.Location.Y + itemsStrip.Size.Height + categoryStrip.Size.Height);

                // Közben változtassuk meg a stmTabControl méretét és helyét, hisz ilyenkor nagyobb területen elterülhet
                stmTabControl.Location = new Point(editStrip.Location.X + editStrip.Size.Width, menuStrip.Size.Height + categoryStrip.Size.Height + itemsStrip.Size.Height);
                stmTabControl.Size = new Size(this.Size.Width - editStrip.Size.Width - 8, this.Size.Height - menuStrip.Size.Height - categoryStrip.Size.Height - itemsStrip.Size.Height - 34);
            }

            //Ha csak az itemsStrip látszik
            if (!categoryStrip.Visible && itemsStrip.Visible)
            {
                editStrip.Location = new Point(0, itemsStrip.Location.Y + itemsStrip.Size.Height);

                // Közben változtassuk meg a stmTabControl méretét és helyét, hisz ilyenkor nagyobb területen elterülhet
                stmTabControl.Location = new Point(editStrip.Location.X + editStrip.Size.Width, menuStrip.Size.Height + itemsStrip.Size.Height);
                stmTabControl.Size = new Size(this.Size.Width - editStrip.Size.Width - 8, this.Size.Height - menuStrip.Size.Height - itemsStrip.Size.Height - 34);
            }

            //Ha csak az categoryStrip látszik
            if (categoryStrip.Visible && !itemsStrip.Visible)
            {
                editStrip.Location = new Point(0, categoryStrip.Location.Y + categoryStrip.Size.Height);

                // Közben változtassuk meg a stmTabControl méretét és helyét, hisz ilyenkor nagyobb területen elterülhet
                stmTabControl.Location = new Point(editStrip.Location.X + editStrip.Size.Width, menuStrip.Size.Height + categoryStrip.Size.Height);
                stmTabControl.Size = new Size(this.Size.Width - editStrip.Size.Width - 8, this.Size.Height - menuStrip.Size.Height - categoryStrip.Size.Height - 34);
            }

            //Ha egyik se látszik
            if (!categoryStrip.Visible && !itemsStrip.Visible)
            {
                editStrip.Location = new Point(0, menuStrip.Height);

                // Közben változtassuk meg a stmTabControl méretét és helyét, hisz ilyenkor nagyobb területen elterülhet
                stmTabControl.Location = new Point(editStrip.Location.X + editStrip.Size.Width, menuStrip.Size.Height);
                stmTabControl.Size = new Size(this.Size.Width - editStrip.Size.Width - 8, this.Size.Height - menuStrip.Size.Height - 34);
            }

            // Ha az editStip nem látszik
            if (!editStrip.Visible)
            {
                stmTabControl.Location = new Point(0, stmTabControl.Location.Y);
                stmTabControl.Size = new Size(this.Size.Width - 8, stmTabControl.Size.Height);
            }
            writeAppData();
        }

        /// <summary>
        /// Ha változik az ablak pozíciója, akkor kiírja a "data" fájlba az alkalmazás információit.
        /// </summary>
        private void MainWindow_LocationChanged(object sender, EventArgs e)
        {
            writeAppData();
        }

        /// <summary>
        ///  Ha megváltozik a főablak mérete, akkor a stmTabControl méretét is megváltoztatjuk.
        /// </summary>
        private void MainWindow_SizeChanged(object sender, EventArgs e)
        {


            stmTabControl.Size = new Size(this.Size.Width -  (editStrip.Visible ? editStrip.Width : 0) - 8,

                                          this.Size.Height - menuStrip.Size.Height -
                                           // Ha látszik az categoryStrip, akkor megint csökkentenünk kell a méretet,
                                           // mert nem akarjuk, hogy elfoglalja az categoryStrip helyét.
                                          (categoryStrip.Visible ? categoryStrip.Size.Height : 0) -
                                           // Ugyanaz, mint előbb, csak most az itemStrip méretével kell csökkentenünk
                                          (itemsStrip.Visible ? itemsStrip.Size.Height : 0)
                                          - 34);

            // Alkalmazás adatainak kiírása fájlba
            writeAppData();
        }

    #endregion

    #region Állapotváltozás események

        /// <summary>
        /// Az Esc gomb lenyomásakor hívódik meg ez a függvény.
        /// Visszaállítja az állapotot Normal-ra, valamint visszavonja a kijelöléseket.
        /// </summary>
        public static void EscKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape && (MainWindow.State == MainWindowState.MoveMode || MainWindow.State == MainWindowState.Moving ||
               (MainWindow.State >= MainWindowState.BlockInsert && MainWindow.State <= MainWindowState.MultipleBranchInsert)))
            {
                if (Stm != null)
                {
                    Stm.Refresh();
                    Stm.DeselectAll();
                }
                MainWindow.State = MainWindowState.Normal;
            }
        }

        /// <summary>
        /// Ez az eseménykezelő fut le mikor a State értéke megváltozik.
        /// Az állapotoktól függ, hogy milyen lesz a kurzor kinézete.
        /// </summary>
        protected void MainWindowStateChanged(object sender, EventArgs e)
        {
            switch (State)
            {
                case MainWindowState.Normal:
                    Cursor = Cursors.Default;
                    break;

                case MainWindowState.Moving:
                case MainWindowState.MoveMode:
                case MainWindowState.BlockInsert:
                case MainWindowState.LoopInsert:
                case MainWindowState.BranchInsert:
                case MainWindowState.MultipleBranchInsert:
                    Cursor = Cursors.Hand;
                    break;

                case MainWindowState.ShiftDown:
                case MainWindowState.ControlDown:
                    Cursor = Cursors.Arrow;
                    break;

                case MainWindowState.ResizeHorizontal:
                    Cursor = Cursors.SizeWE;
                    break;

                case MainWindowState.ResizeVertical:
                    Cursor = Cursors.SizeNS;
                    break;
            }
        }

        /// <summary>
        /// Ha a főablakra kattintottunk, akkor visszaállítjuk az állapotot Normal-ra.
        /// </summary>
        private void MainWindow_MouseClick(object sender, MouseEventArgs e)
        {
            if (!Equals(Stm, null))
            {
                State = MainWindowState.Normal;
                Stm.DeselectAll();
            }
        }

    #endregion

    #region Struktogram műveletek

        /// <summary>
        /// Blokk beszúrása.
        /// </summary>
        private void blockInsert(object sender, EventArgs e)
        {
            insertElement(MainWindowState.BlockInsert);
        }
        
        /// <summary>
        /// Ciklus beszúrása.
        /// </summary>
        private void loopInsert(object sender, EventArgs e)
        {
            insertElement(MainWindowState.LoopInsert);
        }

        /// <summary>
        /// Elágazás beszúrása.
        /// </summary>
        private void branchInsert(object sender, EventArgs e)
        {
            insertElement(MainWindowState.BranchInsert);
        }

        /// <summary>
        /// Többágú elágazás beszúrása
        /// </summary>
        private void multipleBranchInsert(object sender, EventArgs e)
        {
            insertElement(MainWindowState.MultipleBranchInsert);
        }

        /// <summary>
        /// Ez az eljárás végzi el az elemek beszúrásának eseménykezelését.
        /// </summary>
        /// <param name="insertionState"> Az állapot, mely jelzi, hogy milyen elemet szeretnénk beszúrni. </param>
        private void insertElement( MainWindowState insertionState )
        {
            //Az első klikkelés esetén
            if (State == MainWindowState.Normal)
            {
                State = insertionState;
                Stm.DeselectAll();
            }
            else //A második klikkelés esetén az éppen fókuszban lévő elem utánra szúrjuk be az elágazást
            {
                switch(insertionState)
                {
                    case MainWindowState.BlockInsert          : Stm.AddBlock();  break;
                    case MainWindowState.LoopInsert           : Stm.AddLoop();   break;
                    case MainWindowState.BranchInsert         : Stm.AddBranch(); break;
                    case MainWindowState.MultipleBranchInsert : Stm.AddMultipleBranch(); break;
                }

                State = MainWindowState.Normal;
                Stm.Refresh();
            }
        }

        /// <summary>
        /// Kijelöli, vagy a kijelölést visszavonja az éppen fokuszban lévő elemben.
        /// </summary>
        private void selectElement(object sender, EventArgs e)
        {
            Element whichHasFocus = Stm.WhichHasFocus();

            if ( !(whichHasFocus is Title) && !Equals(whichHasFocus,null) )
            {
                if (!((ProgramConstruction)whichHasFocus).Selected)
                {
                    ((ProgramConstruction)whichHasFocus).Select();
                }
                else
                {
                    ((ProgramConstruction)whichHasFocus).Deselect();
                }
            }
        }

        /// <summary>
        /// Rekurzívan kijelöli, vagy a kijelöléseket rekurzívan visszavonja az éppen fokuszban lévő elemben.
        /// </summary>
        private void selectElementRecursively(object sender, EventArgs e)
        {
            Element whichHasFocus = Stm.WhichHasFocus();

            if (!(whichHasFocus is Title) && !Equals(whichHasFocus, null))
            {
                if (!((ProgramConstruction)whichHasFocus).Selected)
                {
                    ((ProgramConstruction)whichHasFocus).SelectRecursively();
                }
                else
                {
                    ((ProgramConstruction)whichHasFocus).DeselectRecursively();
                }
            }
        }

        /// <summary>
        /// Átváltja a főablak állapotát áthelyező módra.
        /// </summary>
        private void moveMode(object sender, EventArgs e)
        {
            State = MainWindowState.MoveMode;
        }

        /// <summary>
        /// Az "Optimális méret" gombra kattintva beállítja az aktuális struktogram optimális méretét.
        /// </summary>
        private void optimalSize(object sender, EventArgs e)
        {
            // Ha esetleg nem volt Normal az állapot akkor átállítjuk, és visszavonjuk a kijelöléseket.
            State = MainWindowState.Normal;
            Stm.DeselectAll();

            Stm.SetOptimalSize();
        }

        /// <summary>
        /// Az újraméretezés gombra kattintva ez az eseménykezelő fut le.
        /// </summary>
        private void resizeElements(object sender, EventArgs e)
        {
            // Ha valamilyen állapotban voltunk, akkor visszállítjuk Normal-ra
            if (State != MainWindowState.Normal)
            {
                State = MainWindowState.Normal;
            }

            List<Element> selectedElementslist = Stm.SelectedElements;

            // Ha van kijelölt elem
            if (selectedElementslist.Count > 0)
            {
                ResizeDialog dialog = new ResizeDialog();

                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    Stm.ResizeElements(selectedElementslist, dialog.AdjustedSize.Width, dialog.AdjustedSize.Height);
                }
            }
            else // Ha nincs kijelölt elem, akkor hibaüzenetet dobunk
            {
                MessageBox.Show("Nincs elem kijelölve!", "Hiba!");
            }

        }

        /// <summary>
        /// A törlés gombra kattintva ez az eseménykezelő fut le.
        /// </summary>
        private void deleteElements(object sender, EventArgs e)
        {
            if (State != MainWindowState.Normal)
            {
                State = MainWindowState.Normal;
            }

            if (Stm.SelectedElements.Count > 0)
            {
                while (Stm.SelectedElements.Count > 0)
                {
                    Stm.RemoveElement(Stm.SelectedElements[0]);
                }
                Stm.SaveState();
            }
            else
            {
                MessageBox.Show(this, "Nincs elem kijelölve!", "Hiba!");
            }
        }

    #endregion

    #region Extra karakterek (categoryStrip)

        /// <summary>
        /// A categoryStrip-hez hozzáad egy gombot, amin a paraméterként megadott jel lesz.
        /// </summary>
        /// <param name="sign"> A jel, melyet a beszúrandó gombon szerenénk látni. </param>
        private void categoryAddItem(String sign)
        {
            categoryAddItem(sign,String.Empty);
        }

        /// <summary>
        /// A categoryStrip-hez hozzáad egy gombot, amin a paraméterként megadott jel lesz.
        /// </summary>
        /// <param name="sign"> A jel, melyet a beszúrandó gombon szerenénk látni. </param>
        /// <param name="toolTip"> A ToolTip, melyet a gombhoz szeretnénk csatolni. </param>
        private void categoryAddItem(String sign, String toolTip)
        {
            categoryStrip.Items.Add(new ToolStripButton(sign));
            if (toolTip != String.Empty)
            {
                categoryStrip.Items[categoryStrip.Items.Count - 1].ToolTipText = toolTip;
            }
            else
            {
                categoryStrip.Items[categoryStrip.Items.Count - 1].AutoToolTip = false;
            }
            categoryStrip.Items[categoryStrip.Items.Count - 1].MouseDown += new MouseEventHandler(categoryStrip_MouseDown);
        }

        /// <summary>
        /// Beszúrjuk a kívánt pozícióra, a categoryStrip-ben kiválasztott elemet.
        /// </summary>
        private void categoryStrip_MouseDown(object sender,MouseEventArgs e)
        {
            Element whichHasFocus = Stm.WhichHasFocus();

            if (whichHasFocus != null)
            {
                int old = whichHasFocus.SelectionStart;

                if (whichHasFocus.SelectionLength > 0)
                {
                    whichHasFocus.Text = whichHasFocus.Text.Replace(whichHasFocus.SelectedText, ((ToolStripButton)sender).Text);
                }
                else
                {
                    whichHasFocus.Text = whichHasFocus.Text.Insert(whichHasFocus.SelectionStart, ((ToolStripButton)sender).Text);
                }

                whichHasFocus.SelectionStart = old+1;
            }
        }

        /// <summary>
        /// Az egyszerűség kedvéért inkább belehelyeztük ezt a kódrészletet egy függvénybe,
        /// amit a categoryComboBox_SelectedIndexChanged eseménykezelő hívogat meg.
        /// </summary>    
        private void setAllCategoryMenuItemToFalse()
        {
            showCategoryMenuItem.Checked = basicMathSignsMenuItem.Checked = greekLettersMenuItem.Checked =
            operatorsMenuItem.Checked = arrowsMenuItem.Checked = negatedRelationSignsMenuItem.Checked = false;
        }

        /// <summary>
        /// Ha a Nézet menüpontban rákattintottunk a "Kategória" menüpontra,
        /// akkor tűnjön el a categoryStrip. Előjönni csak akkor jöhet elő,
        /// ha ezen a menüponton belűl rákattintunk az egyik menüpontra, vagy
        /// kiválasztjuk a categoryComboBox-ból.
        /// </summary>
        private void categoryMenuItem_Click(object sender, EventArgs e)
        {
            if (showCategoryMenuItem.Checked)
            {
                categoryComboBox.SelectedIndex = 0;
            }
        }

        private void basicMathSignsMenuItem_Click(object sender, EventArgs e)
        {
            setCategoryIndexByMenuItem(basicMathSignsMenuItem, 1);
        }

        private void greekLettersMenuItem_Click(object sender, EventArgs e)
        {
            setCategoryIndexByMenuItem(greekLettersMenuItem, 2);
        }

        private void operatorsMenuItem_Click(object sender, EventArgs e)
        {
            setCategoryIndexByMenuItem(operatorsMenuItem, 3);
        }

        private void arrowsMenuItem_Click(object sender, EventArgs e)
        {
            setCategoryIndexByMenuItem(arrowsMenuItem, 4);
        }

        private void negatedRelationSignsMenuItem_Click(object sender, EventArgs e)
        {
            setCategoryIndexByMenuItem(negatedRelationSignsMenuItem, 5);
        }

        /// <summary>
        /// Beállítja a CategoryComboBox indexét a megadott ToolStripMenuItem segítségével, a megadott indexre.
        /// Ennek az lenne a lényege, hogy ha rákattintunk Kategória menüponton belűl vmelyik menüpontra,
        /// akkor az annak megfelelő jelek jelenjenek meg.
        /// </summary>
        /// <param name="menuItem"> A ToolStipMenuItem, amit megválzsgál a metódus, hogy ki van-e pipálva. </param>
        /// <param name="index"> Az index melyre be szeretnénk állítani a categoryComboBox indexét. </param>
        private void setCategoryIndexByMenuItem(ToolStripMenuItem menuItem, int index)
        {
            if (!menuItem.Checked) // Ha a menuitem mellett nem volt pipa, akkor beállítjuk az indexet
            {
                categoryComboBox.SelectedIndex = index;
            }
            else // Ha pedig volt, akkor az indexet 0-ra állítjuk, és eltüntetjük a categoryStrip-et (de ezt már az eseménykezelő végzni el)
            {
                categoryComboBox.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Ez az eseménykezelő egy kényelmi funkciót szolgál : 
        /// Ha a felhasználó kiválasztja az egyik kategóriát, és a categoryComboBox bezárul,
        /// akkor visszahelyezi a fokuszt arra az elemre, mely legutóbb volt fokuszban a struktogramban.
        /// </summary>
        private void categoryComboBox_DropDownClosed(object sender, EventArgs e)
        {
            try
            {
                Stm.LatestFocusedElement.Focus();
            }
            catch (Exception ex) { }
        }

        /// <summary>
        /// A categoryStrip-re ez a függvény rakja rá a gombokat.
        /// </summary>
        private void categoryComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            // A categoryComboBox-ban választhatjuk ki, hogy milyen kategórájú jelek legyenek
            // a categoryStrip-en.
            switch (categoryComboBox.SelectedIndex)
            {
                // (üres) esetén eltüntetjük a categoryStrip-et
                case 0:
                    categoryStrip.Items.Clear();
                    categoryStrip.Visible = false;

                    setAllCategoryMenuItemToFalse();
                break;

                // ALAPVETŐ MATEMATIKAI JELEK //////////////////////////
                case 1:
                    categoryStrip.Items.Clear();
                    categoryStrip.Visible = true;

                    setAllCategoryMenuItemToFalse();

                    showCategoryMenuItem.Checked = true;
                    basicMathSignsMenuItem.Checked = true;

                    categoryAddItem("±", "Plusz-mínusz");
                    categoryAddItem("∓", "Mínusz-plusz");
                    categoryAddItem("×", "Szorzat");
                    categoryAddItem("∙", "Szorzásjel");
                    categoryAddItem("⌈", "Felső egészrész nyitó");
                    categoryAddItem("⌉", "Felső egészrész csukó");
                    categoryAddItem("⌊", "Alsó egészrész nyitó");
                    categoryAddItem("⌋", "Alsó egészrész csukó");
                    categoryAddItem("∞", "Végtelen");
                    categoryAddItem("≠", "Nem egyenlő");
                    categoryAddItem("≅", "Egybevágó");
                    categoryAddItem("≈", "Körülbelül");
                    categoryAddItem("≡", "Azonosan egyenlő");
                    categoryAddItem("∝", "Arányosság");
                    categoryAddItem("≪", "Jóval kisebb");
                    categoryAddItem("≫", "Jóval nagyobb");
                    categoryAddItem("≤", "Kisebb, vagy egyenlő");
                    categoryAddItem("≥", "Nagyobb, vagy egyenlő");
                    categoryAddItem("∀", "Minden");
                    categoryAddItem("∃", "Létezik");
                    categoryAddItem("∄", "Nem létezik");
                    categoryAddItem("¬", "Negálás");
                    categoryAddItem("∂", "Parciális differenciál");
                    categoryAddItem("∪", "Unió");
                    categoryAddItem("∩", "Metszet");
                    categoryAddItem("∈", "Eleme");
                    categoryAddItem("∉", "Nem eleme");
                    categoryAddItem("∅", "Üreshalmaz");
                    categoryAddItem("°", "Fok");
                    categoryAddItem("℉", "Fahrenheit-fok");
                    categoryAddItem("℃", "Celsius-fok");
                    categoryAddItem("∆");
                    categoryAddItem("∇");
                break;

                // GÖRÖG BETŰK /////////////////////////////////
                case 2:
                    categoryStrip.Items.Clear();
                    categoryStrip.Visible = true;
                   
                    setAllCategoryMenuItemToFalse();

                    showCategoryMenuItem.Checked = true;
                    greekLettersMenuItem.Checked = true;

                    categoryAddItem("α", "Alpha");
                    categoryAddItem("β", "Béta");
                    categoryAddItem("γ", "Gamma");
                    categoryAddItem("δ", "Omega");
                    categoryAddItem("ε", "Epszilon");
                    categoryAddItem("ζ", "Zéta");
                    categoryAddItem("η", "Éta");
                    categoryAddItem("θ", "Théta");
                    categoryAddItem("ϑ", "Théta");
                    categoryAddItem("ι", "Ióta");
                    categoryAddItem("κ", "Kappa");
                    categoryAddItem("λ", "Lambda");
                    categoryAddItem("μ", "Mű");
                    categoryAddItem("ν", "Nű");
                    categoryAddItem("ξ", "Kszi");
                    categoryAddItem("π", "Pi");
                    categoryAddItem("ρ", "Rhó");
                    categoryAddItem("ϱ", "Rhó");
                    categoryAddItem("σ", "Szigma");
                    categoryAddItem("ς", "Szigma");
                    categoryAddItem("τ", "Tau");
                    categoryAddItem("υ", "Üpszilon");
                    categoryAddItem("ϕ", "Fi");
                    categoryAddItem("φ", "Fi");
                    categoryAddItem("χ", "Khi");
                    categoryAddItem("ψ", "Pszi");
                    categoryAddItem("ω", "Omega");
                    categoryStrip.Items.Add(new ToolStripSeparator());
                    categoryAddItem("Γ", "Nagy gamma");
                    categoryAddItem("Δ", "Nagy delta");
                    categoryAddItem("Θ", "Nagy théta");
                    categoryAddItem("Λ", "Nagy lambda");
                    categoryAddItem("Ξ", "Nagy kszi");
                    categoryAddItem("Π", "Nagy pi");
                    categoryAddItem("Σ", "Nagy szigma");
                    categoryAddItem("Υ", "Nagy üpszilon");
                    categoryAddItem("Φ", "Nagy fi");
                    categoryAddItem("Ψ", "Nagy pszi");
                    categoryAddItem("Ω", "Nagy omega");
                break;

                // OPERÁTOROK ///////////////////////////////////
                case 3 :
                    categoryStrip.Items.Clear();
                    categoryStrip.Visible = true;

                    setAllCategoryMenuItemToFalse();

                    showCategoryMenuItem.Checked = true;
                    operatorsMenuItem.Checked = true;

                    categoryAddItem("∧", "Konjunkció");
                    categoryAddItem("∨", "Diszjunkció");
                    categoryAddItem("¬", "Negálás");
                    categoryAddItem("⊢", "Implikáció");
                    categoryStrip.Items.Add(new ToolStripSeparator());
                    categoryAddItem("∪", "Unió");
                    categoryAddItem("∩", "Metszet");
                    categoryAddItem("⊂", "Része");
                    categoryAddItem("⊃", "Része");
                    categoryAddItem("⊆", "Része, vagy egyenlő");
                    categoryAddItem("⊇", "Része, vagy egyenlő");
                    categoryAddItem("∈", "Eleme");
                    categoryAddItem("∋", "Eleme");
                    categoryStrip.Items.Add(new ToolStripSeparator());
                    categoryAddItem("≡", "Azonosan egyenlő");
                    categoryAddItem("≈", "Körülbelül");
                    categoryAddItem("≤", "Kisebb, vagy egyenlő");
                    categoryAddItem("≥", "Nagyobb, vagy egyenlő");
                    categoryAddItem("≦", "Kisebb, vagy egyenlő");
                    categoryAddItem("≧", "Nagyobb, vagy egyenlő");
                    categoryAddItem("≪", "Jóval kisebb");
                    categoryAddItem("≫", "Jóval nagyobb");
                    categoryAddItem("≺", "Megelőzi");
                    categoryAddItem("≻", "Követi");
                    categoryAddItem("≼", "Megelőzi, vagy ugyanaz");
                    categoryAddItem("≽", "Követi, vagy ugyanaz");
                    categoryStrip.Items.Add(new ToolStripSeparator());
                    categoryAddItem("±", "Plusz-mínusz");
                    categoryAddItem("∓", "Mínusz-plusz");
                    categoryAddItem("×", "Szorzat");
                    categoryAddItem("∘", "Kompozíció");
                    categoryStrip.Items.Add(new ToolStripSeparator());
                    categoryAddItem("∑", "Szumma");
                    categoryAddItem("∫", "Integrál");
                    categoryAddItem("∬", "Kettős integrál");
                    categoryAddItem("∭", "Hármas integrál");
                    categoryAddItem("∏", "Produktum");
                    categoryAddItem("∐", "Koproduktum");
                    categoryStrip.Items.Add(new ToolStripSeparator());
                    categoryAddItem("∥", "Párhuzamos");
                    categoryAddItem("⊥", "Merőleges");
                    categoryAddItem("⋈", "Összekapcsolás");
                    categoryAddItem("⊕");
                    categoryAddItem("⊖");
                    categoryAddItem("⊗");
                    categoryAddItem("⊘");
                    categoryAddItem("⊙");
                    categoryAddItem("⊛");
                    categoryAddItem("⊚");
                break;

                // NYILAK ////////////////////////////////////////////
                case 4:
                    categoryStrip.Items.Clear();
                    categoryStrip.Visible = true;

                    setAllCategoryMenuItemToFalse();

                    showCategoryMenuItem.Checked = true;
                    arrowsMenuItem.Checked = true;

                    categoryAddItem("←");
                    categoryAddItem("→");
                    categoryAddItem("↑");
                    categoryAddItem("↓");
                    categoryAddItem("↔");
                    categoryAddItem("↗");
                    categoryAddItem("↖");
                    categoryAddItem("↘");
                    categoryAddItem("↙");
                    categoryAddItem("↚");
                    categoryAddItem("↛");
                    categoryAddItem("↮");
                    categoryStrip.Items.Add(new ToolStripSeparator());
                    categoryAddItem("⇐");
                    categoryAddItem("⇒");
                    categoryAddItem("⇑");
                    categoryAddItem("⇓");
                    categoryAddItem("⇔");
                    categoryAddItem("⇕");
                    categoryAddItem("⇍");
                    categoryAddItem("⇏");
                    categoryAddItem("⇎");
                    categoryStrip.Items.Add(new ToolStripSeparator());
                    categoryAddItem("↼");
                    categoryAddItem("↽");
                    categoryAddItem("⇀");
                    categoryAddItem("⇁");
                    categoryAddItem("↿");
                    categoryAddItem("↾");
                    categoryAddItem("⇃");
                    categoryAddItem("⇂");
                    categoryAddItem("⇋");
                    categoryAddItem("⇌");
                    categoryStrip.Items.Add(new ToolStripSeparator());
                    categoryAddItem("↦");
                    categoryAddItem("↩");
                    categoryAddItem("↪");
                break;

                // NEGÁLT RELÁCIÓS JELEK /////////////////////////////
                case 5:
                    categoryStrip.Items.Clear();
                    categoryStrip.Visible = true;

                    setAllCategoryMenuItemToFalse();

                    showCategoryMenuItem.Checked = true;
                    negatedRelationSignsMenuItem.Checked = true;

                    categoryAddItem("≠", "Nem egyenlő");
                    categoryAddItem("≮", "Nem kisebb");
                    categoryAddItem("≯", "Nem nagyobb");
                    categoryAddItem("≰", "Nem kisebb, vagy egyenlő");
                    categoryAddItem("≱", "Nem nagyobb, vagy egyenlő");
                    categoryAddItem("≨", "Nem kisebb, vagy egyenlő");
                    categoryAddItem("≩", "Nem nagyobb, vagy egyenlő");
                    categoryAddItem("≢", "Nem azonosan egyenlő");
                    categoryAddItem("≉");
                    categoryAddItem("≁");
                    categoryAddItem("≇");
                    categoryAddItem("⊀", "Nem előzi meg");
                    categoryAddItem("⊁", "Nem követi");
                    categoryAddItem("⋠", "Nem előzi meg és nem ugyanaz");
                    categoryAddItem("⋡", "Nem követi és nem ugyanaz");
                    categoryStrip.Items.Add(new ToolStripSeparator());
                    categoryAddItem("⊄", "Nem része");
                    categoryAddItem("⊅", "Nem része");
                    categoryAddItem("⊈", "Nem része, vagy egyenlő");
                    categoryAddItem("⊉", "Nem része, vagy egyenlő");
                    categoryAddItem("⊊", "Nem része, vagy egyenlő");
                    categoryAddItem("⊋", "Nem része, vagy egyenlő");
                    categoryAddItem("∉", "Nem eleme");
                    categoryStrip.Items.Add(new ToolStripSeparator());
                    categoryAddItem("∄", "Nem létezik");
                    categoryAddItem("∤", "Nem osztója");
                    categoryAddItem("∦", "Nem párhuzamos");
                    categoryAddItem("⊬", "Nem implikálja");
                    categoryAddItem("⊭");
                    categoryAddItem("⊮");
                    categoryAddItem("⊯");
                break;
            }

            // Ha esetleg eltűnne, vagy előjönne a categoryStip, akkor
            // máshova kell pozícionálni az editStip-et
            editStripPlacing();
        }

    #endregion

    #region Exportálás

        /// <summary>
        /// Ez a függvény valósítja meg az aktuális Struktogram elmentését képformátumba.
        /// </summary>
        private void saveToImage(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();

            dialog.Title = "Exportálás képformátumba";
            dialog.Filter = "BMP (*.bmp)|*.bmp|" +
                            "GIF (*.gif)|*.gif|" +
                            "JPEG (*.jpg, *.jpeg)|*.jp*g|" +
                            "PNG (*.png)|*.png|" +
                            "TIFF (*.tif)|*.tif|" +
                            "Összes fájl|*.*";
            dialog.FilterIndex = 6;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                MainWindow.State = MainWindowState.Normal;
                Stm.DeselectAll();

                Bitmap image = new Bitmap(Stm.Size.Width, Stm.Size.Height);
                Graphics g = Graphics.FromImage(image);

                Stm.DrawToBitmap(image, new Rectangle(0, 0, Stm.Size.Width, Stm.Size.Height));

                // Sajnos a Bitmap-be nem kerülnek bele az elágazás-feltétlek sarkában lévő
                // vonalak, és ezért ezeket bele kell rajzolni a Bitmap-be.
                branchCornerCorrection(Stm, g, new Point(0, 0));

                // A 6-os index az, mikor nincs megadva a Filter-ben semmilyen formátum
                if (dialog.FilterIndex == 6)
                {
                    if (dialog.FileName.EndsWith(".bmp", true, System.Globalization.CultureInfo.CurrentCulture))
                        image.Save(dialog.FileName, System.Drawing.Imaging.ImageFormat.Bmp);
                    else
                        if (dialog.FileName.EndsWith(".gif", true, System.Globalization.CultureInfo.CurrentCulture))
                            image.Save(dialog.FileName, System.Drawing.Imaging.ImageFormat.Gif);
                        else
                            if (dialog.FileName.EndsWith(".jpg", true, System.Globalization.CultureInfo.CurrentCulture) || dialog.FileName.EndsWith(".jpeg", true, System.Globalization.CultureInfo.CurrentCulture))
                                image.Save(dialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                            else
                                if (dialog.FileName.EndsWith(".png", true, System.Globalization.CultureInfo.CurrentCulture))
                                    image.Save(dialog.FileName, System.Drawing.Imaging.ImageFormat.Png);
                                else
                                    if (dialog.FileName.EndsWith(".tif", true, System.Globalization.CultureInfo.CurrentCulture))
                                        image.Save(dialog.FileName, System.Drawing.Imaging.ImageFormat.Tiff);
                                    else  // Ha egyikre se végződik, akkor .png formátumba mentjük el
                                        image.Save(dialog.FileName + ".png", System.Drawing.Imaging.ImageFormat.Png);
                }
                else
                {
                    switch (dialog.FilterIndex)
                    {
                        case 1: image.Save(dialog.FileName, System.Drawing.Imaging.ImageFormat.Bmp); break;
                        case 2: image.Save(dialog.FileName, System.Drawing.Imaging.ImageFormat.Gif); break;
                        case 3: image.Save(!(dialog.FileName.EndsWith(".jpg", true, System.Globalization.CultureInfo.CurrentCulture) ||
                                             dialog.FileName.EndsWith(".jpeg", true, System.Globalization.CultureInfo.CurrentCulture)) ? dialog.FileName + ".jpg" : dialog.FileName, System.Drawing.Imaging.ImageFormat.Jpeg); break;
                        case 4: image.Save(dialog.FileName, System.Drawing.Imaging.ImageFormat.Png); break;
                        case 5: image.Save(dialog.FileName, System.Drawing.Imaging.ImageFormat.Tiff); break;
                    }
                }

            }

        }

        /// <summary>
        /// Mivel képformátumba való mentéskor eltűnnek az elágazás-feltételek sarkánál lévő vonalak,
        /// ezért elmentés előtt ezeket bele kell rajzolni a Bitmap-be.
        /// </summary>
        /// <param name="elementParent"> A Control, amit épppen vizsgálunk. </param>
        /// <param name="parentLocation"> A szülő abszolút pozíciója a Bitmap-ben. </param>
        private void branchCornerCorrection(Control elementParent, Graphics g, Point parentLocation)
        {
            foreach (Element element in elementParent.Controls)
            {
                // Ez azért kell, hogy tudjuk a Bitmap-beli abszolút pozícióját,
                Point locationPoint = new Point(parentLocation.X + element.Location.X, parentLocation.Y + element.Location.Y);

                // Branch és SimpleBranch esetén a bal oldali vonal minkét esetben kell
                if ((element is Branch) || (element is SimpleBranch && !(elementParent is Branch)))
                {
                    g.DrawLine(new Pen(Color.Black), new Point(locationPoint.X, locationPoint.Y + ((AbstractBranch)element).ConditionHeight - element.Font.Height - 1),
                               new Point(locationPoint.X + Convert.ToInt32(element.Font.Size), locationPoint.Y + ((AbstractBranch)element).ConditionHeight));
                }

                // A jobboldali vonal pedig csak Branch esetén
                if (element is Branch)
                {
                    g.DrawLine(new Pen(Color.Black), new Point(locationPoint.X + element.Size.Width - 1, locationPoint.Y + ((AbstractBranch)element).ConditionHeight - element.Font.Height - 1),
                               new Point(locationPoint.X + element.Size.Width - Convert.ToInt32(element.Font.Size) - 1, locationPoint.Y + ((AbstractBranch)element).ConditionHeight));
                }

                // Rekurzívan meghívogatjuk a függvényt, így az összes elemen végigmegyünk
                if (element.Controls.Count > 0) branchCornerCorrection(element, g, locationPoint);
            }
        }

        /// <summary>
        /// A LaTeX gombra kattintva létrehozzuk az aktuális struktogramból a LaTeX kódot.
        /// </summary>
        private void saveToLatex(object sender, EventArgs e)
        {
            LatexOptionsDialog dialog = new LatexOptionsDialog(this);

            // Ha már egyszer megcsinálta a program a LaTeX kódot, akkor visszatöltjük
            // az előző beállításokat
            if (StmTabPage.OldLatexOptions != null)
            {
                dialog.LatexOptions = StmTabPage.OldLatexOptions;
            }
            else
            {
                dialog.ShowTitle = Stm.ShowTitle;
                dialog.OnlyStruktogramCode = true;
            }

            if (StmTabPage.OldLatexFileName != null)
            {
                dialog.FileName = StmTabPage.OldLatexFileName;
            }

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                StreamWriter sw;
                LatexCodeGenerator generator = new LatexCodeGenerator();

                // Ha nem ".tex"-re végződik a fájl neve, akkor hozzáírjuk
                if (dialog.FileName.EndsWith(".tex"))
                {
                    sw = new StreamWriter(dialog.FileName);
                }
                else
                {
                    sw = new StreamWriter(dialog.FileName + ".tex");
                }

                // Egy LatexCodeGenerator osztály hozza létre a Latex kódot
                // a megadott struktogramból, és opciókból
                sw.Write(generator.CreateLatexCode(Stm, dialog.LatexOptions));
                sw.Flush();

                StmTabPage.OldLatexOptions = dialog.LatexOptions;
                StmTabPage.OldLatexFileName = dialog.FileName;
                sw.Close();
            }
        }

    #endregion

    #region Fájl műveletek

        /// <summary>
        /// Ez az eseménykezelő létrehoz egy új struktogrammot, egy új StruktogramPage-re.
        /// </summary>
        private void newStruktogram(object sender, EventArgs e)
        {
            StruktogramOptionsDialog dialog = new StruktogramOptionsDialog();

            // Beállítja az opciókat az aktuális struktogram opcióira
            if (Stm != null)
            {
                dialog.StruktogramOptions = Stm.StruktogramOptions;
            }

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                // Ekkor már legalább egy struktogram lesz, és így elérhetővé tesszük az eddig nem elérhető menüpontokat
                setMenusEnabled(true);

                stmTabControl.Controls.Add(new StruktogramPage());

                ((StruktogramPage)stmTabControl.TabPages[stmTabControl.TabPages.Count - 1]).Stm = new Struktogram();
                stmTabControl.SelectedTab = stmTabControl.TabPages[stmTabControl.TabPages.Count - 1];

                ((StruktogramPage)stmTabControl.TabPages[stmTabControl.TabPages.Count - 1]).Stm.StruktogramOptions = new StruktogramOptions(dialog.Font, dialog.FontColor, dialog.ShowTitle);
                ((StruktogramPage)stmTabControl.TabPages[stmTabControl.TabPages.Count - 1]).Stm.SaveState();
                ((StruktogramPage)stmTabControl.TabPages[stmTabControl.TabPages.Count - 1]).FileName = "";

                // Az új lesz az aktuális struktogram.
                stm = ((StruktogramPage)stmTabControl.Controls[stmTabControl.SelectedIndex]).Stm;
            }
        }

        /// <summary>
        /// Az aktuális StruktogramPage-en lévő Struktogram elmentése másként.
        /// </summary>
        private void saveAs(object sender, EventArgs e)
        {
            StmTabPage.SaveStruktogramAs(); // Ha nincs betöltve struktogram akkor ez az opció nem elérhető
        }

        /// <summary>
        /// Az aktuális StruktogramPage-en lévő Struktogram elmentése.
        /// </summary>
        private void save(object sender, EventArgs e)
        {
            StmTabPage.SaveStruktogram(); // Ha nincs betöltve struktogram akkor ez az opció nem elérhető
        }

        /// <summary>
        /// Az összes StruktogramPage-en lévő Struktogram elmentése.
        /// </summary>
        private void saveAll(object sender, EventArgs e)
        {
            Element element = Stm.WhichHasFocus();
            StruktogramPage actualStruktogramPage = StmTabPage;

            foreach (StruktogramPage sp in stmTabControl.TabPages)
            {
                sp.SaveStruktogram();
            }

            stmTabControl.SelectedIndex = stmTabControl.TabPages.IndexOf(actualStruktogramPage);
            if (!Equals(element, null))
            {
                element.Focus();
            }
        }

        /// <summary>
        /// Ez az eseménykezelő, megnyit egy fájlból (vagy többől) egy (vagy több) struktogrammot.
        /// </summary>
        private void open(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            bool alreadyLoaded;
            int alreadyLoadedPageIndex = 0;

            // Több struktogrammot is beötlhetünk egyszerre
            dialog.Multiselect = true;
            dialog.Filter = "K kiterjesztásű fájlok (*.k)|*.k|" +
                            "Összes fájl|*.*";

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                // Betöltjük az összes kiválasztott fájlt
                foreach (String fileName in dialog.FileNames)
                {
                    alreadyLoaded = false;

                    // Meghatározzuk, hogy be van-e töltve már a fájl
                    foreach (StruktogramPage sp in stmTabControl.TabPages)
                    {
                        if (sp.FileName == fileName)
                        {
                            alreadyLoadedPageIndex = stmTabControl.TabPages.IndexOf(sp);
                            alreadyLoaded = true;
                            break; // Ha be van töltve akkor kilépünk a ciklusból
                        }
                    }

                    if (alreadyLoaded) // Ha be van töltve, akkor átállítjuk rá az indexet
                    {
                        stmTabControl.SelectedIndex = alreadyLoadedPageIndex;
                    }
                    else // Egyébként betöltjük
                    {
                        try
                        {
                            FileStream s = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.None);
                            BinaryFormatter bf = new BinaryFormatter();
                            Struktogram loadStm;

                            State = MainWindowState.Deserializing;
                            loadStm = ((Struktogram)bf.Deserialize(s));
                            State = MainWindowState.Normal;

                            // Ekkor már legalább egy struktogram lesz, és így elérhetővé tesszük az eddig nem elérhető menüpontokat
                            setMenusEnabled(true);

                            stmTabControl.Controls.Add(new StruktogramPage());
                            stmTabControl.SelectedTab = stmTabControl.TabPages[stmTabControl.TabPages.Count - 1];

                            ((StruktogramPage)stmTabControl.TabPages[stmTabControl.TabPages.Count - 1]).Stm = loadStm;
                            ((StruktogramPage)stmTabControl.TabPages[stmTabControl.TabPages.Count - 1]).Stm.SaveState();
                            ((StruktogramPage)stmTabControl.TabPages[stmTabControl.TabPages.Count - 1]).FileName = fileName;

                            s.Close();
                        }
                        catch (System.Reflection.TargetInvocationException ex) { MessageBox.Show("Hibás formátum : " + fileName, "Hiba!"); }
                        catch (Exception ex) { MessageBox.Show("Fájl megnyitási hiba : " + fileName, "Hiba!"); }
                    }

                }
         
                // Beállítijuk az aktuális struktogramot a betötöttre
                if (stmTabControl.TabCount > 0)
                {
                    stm = ((StruktogramPage)stmTabControl.TabPages[stmTabControl.SelectedIndex]).Stm;
                }
            }
        }

        /// <summary>
        /// Megvalósítja az elmentett állapotok közötti visszalépést.
        /// Természetesen, csak akkor lép vissza, ha lehet.
        /// </summary>
        private void undo(object sender, EventArgs e)
        {
            if (Stm.TmpFileNumber > 1)
            {
                --Stm.TmpFileNumber;
                restoreState(StmTabPage.TmpFilesDirectory + "\\" + Stm.TmpFileNumber.ToString());
            }
        }

        /// <summary>
        /// Megvalósítja az elmentett állapotok közötti előrelépést.
        /// Csak abban az esetben lép előre, ha tud mire.
        /// </summary>
        private void redo(object sender, EventArgs e)
        {
            if (Stm.TmpFileNumber < NumberOfPossibleSavedStates && File.Exists(StmTabPage.TmpFilesDirectory + "\\" + Convert.ToString(Stm.TmpFileNumber + 1)))
            {
                ++Stm.TmpFileNumber;
                restoreState(StmTabPage.TmpFilesDirectory + "\\" + Stm.TmpFileNumber.ToString());
            }
        }

        /// <summary>
        /// Állapot visszaállítása.
        /// </summary>
        /// <param name="statePath"> A betöltendő állapot elérési útja. </param>
        private void restoreState(String statePath)
        {
            try
            {
                if (File.Exists(statePath))
                {
                    int oldTmpFileNumber = Stm.TmpFileNumber;
                    Point oldAutoScrollPosition = StmTabPage.AutoScrollPosition;
                    
                    FileStream s = new FileStream(statePath, FileMode.Open, FileAccess.Read, FileShare.None);
                    BinaryFormatter bf = new BinaryFormatter();
                    Struktogram loadStm;

                    MainWindow.State = MainWindowState.Deserializing;
                    loadStm = ((Struktogram)bf.Deserialize(s));
                    MainWindow.State = MainWindowState.Normal;

                    StmTabPage.Stm = loadStm;
                    StmTabPage.Stm.TmpFileNumber = oldTmpFileNumber;
                    StmTabPage.Modified = true;
                    stm = StmTabPage.Stm;
                    s.Close();

                    // Visszatöltjük a régi scroll pozíciókat, mert egyébként visszaállítódnának az elejére
                    StmTabPage.AutoScrollPosition = new Point(0-oldAutoScrollPosition.X,0-oldAutoScrollPosition.Y);
                }
            }
            catch (Exception ex) { } // Ha esetleg hiba lépne fel, akkor nem kell vele foglalkoznunk.
                                     // De elvileg hibának nem szabadna fellépnie.
        }

        /// <summary>
        /// Az aktuális StruktogramPage bezárása.
        /// </summary>
        private void close(object sender, EventArgs e)
        {
            stopClosing = false;
            stopClosing = StmTabPage.CloseStruktogram();

            if (!stopClosing)
            {
                StmTabPage.Dispose();
            }

            setMenusEnabled(false);
            MainWindow.State = MainWindowState.Normal;
        }

        /// <summary>
        /// Az összes StruktogramPage bezárása.
        /// </summary>
        private void closeAll(object sender, EventArgs e)
        {
            stopClosing = false;
            foreach (StruktogramPage sp in stmTabControl.TabPages)
            {
                stopClosing = sp.CloseStruktogram();
                if (stopClosing)
                {
                    break;
                }
                else
                {
                    sp.Dispose();
                }
            }

            setMenusEnabled(false);
            MainWindow.State = MainWindowState.Normal;
        }

    #endregion

    #region Opciók

        private void struktogramOptions(object sender, EventArgs e)
        {
            StruktogramOptionsDialog dialog = new StruktogramOptionsDialog();

            dialog.StruktogramOptions = Stm.StruktogramOptions;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                MainWindow.State = MainWindowState.Normal;
                Stm.DeselectAll();

                // Csak akkor állítjuk át a fontot, ha szükséges
                if (dialog.Font != Stm.Font)
                {
                    Stm.Font = dialog.Font;
                }
                Stm.FontColor = dialog.FontColor;
                Stm.ShowTitle = dialog.ShowTitle;
                Stm.SaveState();
            }
        }

        private void optionsMenuItem_Click(object sender, EventArgs e)
        {
            OptionsDialog dialog = new OptionsDialog();

            dialog.NumberOfBackSteps = NumberOfPossibleSavedStates - 1;

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                int numberOfBackSteps = dialog.NumberOfBackSteps;

                // Azért adunk hozzá 1-et, mert ha pl. 1-szer szeretnénk visszalépni
                // akkor valójában két állapotot kell tárolnunk.
                ++numberOfBackSteps;

                // Ha kevesebbet állított be a felhasználó mint ami volt,
                // akkor ebből gondok lehetnek, ezért ekkor töröljük az állapotokat
                // és a végén elmentjük az aktuálisat. Ha a szám nagyobb, akkor semmi gond nincs.
                if (Stm != null && numberOfBackSteps < NumberOfPossibleSavedStates)
                {
                    // Végigmegyünk az összes lapon
                    foreach (StruktogramPage sp in stmTabControl.TabPages)
                    {
                        try
                        {
                            foreach (String s in Directory.GetFiles(sp.TmpFilesDirectory))
                            {
                                File.Delete(s);
                            }

                            sp.Stm.TmpFileNumber = 0;
                            sp.Stm.SaveState();
                        }
                        catch (Exception ex) { }
                    }
                }

                NumberOfPossibleSavedStates = numberOfBackSteps;
                writeAppData();
            }
        }

    #endregion

    #region Szöveg műveletek

        /// <summary>
        /// A kijelölt szöveget átmásolja a vágólapra.
        /// </summary>
        private void copyText(object sender, EventArgs e)
        {
            try
            {
                Clipboard.SetText(Stm.WhichHasFocus().SelectedText, TextDataFormat.UnicodeText);
            }
            catch (Exception ex) { }
        }

        /// <summary>
        /// A vágólapon lévő szöveget beilleszti a kurzorpozíció helyére, vagy
        /// ha van kijelölt szöveg, akkor felülírja azt a vágólapon lévő szöveggel.
        /// </summary>
        private void insertText(object sender, EventArgs e)
        {
            try
            {
                Element whichHasFocus = Stm.WhichHasFocus();
                int oldSelectionStart = whichHasFocus.SelectionStart;

                // Beszúrjuk a szöveget
                if (whichHasFocus.SelectionLength > 0) // Ha van kijelölt szöveg, akkor felülírjuk azt
                {
                    whichHasFocus.Text = whichHasFocus.Text.Remove(whichHasFocus.SelectionStart, whichHasFocus.SelectionLength);
                    whichHasFocus.Text = whichHasFocus.Text.Insert(oldSelectionStart, Clipboard.GetText());
                }
                else // Egyébként a kurzorpozíció helyére szúrjuk be
                {
                    whichHasFocus.Text = whichHasFocus.Text.Insert(whichHasFocus.SelectionStart, Clipboard.GetText());
                }

                // Alapból a kurzor a szöveg elejére ugrana a változtatás után, ezért
                // a beszúrt szöveg végére pozícionáljuk a kurzort
                whichHasFocus.SelectionStart = oldSelectionStart + Clipboard.GetText().Length;
            }
            catch (Exception ex) { }
        }

        /// <summary>
        /// A kijelölt szöveg áthelyezése a vágólapra, majd a kijelölt szöveg törlése.
        /// </summary>
        private void cutText(object sender, EventArgs e)
        {
            try
            {
                Clipboard.SetText(Stm.WhichHasFocus().SelectedText, TextDataFormat.UnicodeText);
                deleteText(sender,e);
            }
            catch (Exception ex) { }
        }

        /// <summary>
        /// A kijelölt szöveg törlése.
        /// </summary>
        private void deleteText(object sender, EventArgs e)
        {
            try
            {
                Element whichHasFocus = Stm.WhichHasFocus();
                int oldSelectionStart = whichHasFocus.SelectionStart;

                whichHasFocus.Text = whichHasFocus.Text.Remove(whichHasFocus.SelectionStart, whichHasFocus.SelectionLength);

                // Alapból a kurzor a szöveg elejére ugrana a változtatás után, ezért
                // a törölt szöveg elejére pozícionáljuk a kurzort
                whichHasFocus.SelectionStart = oldSelectionStart;
            }
            catch (Exception ex) { }
        }

    #endregion

    #region Alkalmazás bezárása

        /// <summary>
        /// A főablak bezárásakor törli a 'tmp' könyvtárat.
        /// </summary>
        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            closeAll(sender, e);
            if (stopClosing)
            {
                e.Cancel = true;
            }
            else
            {
                try
                {
                    Directory.Delete(ProgramDirectory + "tmp", true);
                }
                catch (Exception ex) { }
            }
        }

        private void exitMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    #endregion

    #region Súgó

        /// <summary>
        /// Ez az eseménykezelő megnyitja a súgót (help.pdf).
        /// </summary>
        private void helpMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(programDirectory + "\\" + "help.pdf");
            }
            catch (Exception ex)
            {
                MessageBox.Show("A súgót nem sikerült megnyitni!\n\n" +
                                "Ennek az lehet az oka, hogy nem található a program\n" +
                                "könyvtárában a súgó fájl (help.pdf),\n" +
                                "vagy nincs telepítve PDF fájlokat támogató alkalmazás!");
            }
        }

    #endregion

    }

}
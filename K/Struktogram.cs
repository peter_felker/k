using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace K
{

#region StruktogramOptions oszt�ly

    /// <summary>
    /// A Struktogram opci�it t�rolja ez az oszt�ly.
    /// </summary>
    public class StruktogramOptions
    {
        private Font font;
        private Color fontColor;
        private bool showTitle;

        /// <summary>
        /// Megadja, vagy be�ll�tja a fontot.
        /// </summary>
        public Font Font
        {
            get { return font; }
            set { font = value; }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja a font sz�n�t.
        /// </summary>
        public Color FontColor
        {
            get { return fontColor; }
            set { fontColor = value; }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja, hogy a struktogramnak legyen-e c�me.
        /// </summary>
        public bool ShowTitle
        {
            get { return showTitle; }
            set { showTitle = value; }
        }

        /// <summary>
        /// A StruktogramOptions konstruktora.
        /// </summary>
        public StruktogramOptions() : this(null, Color.Black, true) { }

        /// <summary>
        /// A StruktogramOptions copy-konstruktora.
        /// </summary>
        /// <param name="struktogramOptions"> A m�soland� StruktogramOptions. </param>
        public StruktogramOptions(StruktogramOptions struktogramOptions) : this(struktogramOptions.Font, struktogramOptions.FontColor, struktogramOptions.ShowTitle) { }

        /// <summary>
        /// A StruktogramOptions konstruktora.
        /// </summary>
        /// <param name="font"> A be�ll�tand� font. </param>
        /// <param name="fontColor"> A be�ll�tand� font sz�n. </param>
        /// <param name="showTitle"> Legyen-e c�me a struktogramnak. </param>
        public StruktogramOptions(Font font, Color fontColor, bool showTitle)
        {
            this.Font = font;
            this.FontColor = fontColor;
            this.ShowTitle = showTitle;
        }

    }

#endregion

    /// <summary>
    /// A struktogrammot megval�s�t� oszt�ly.
    /// Az eg�sz struktogrammot egy panelbe helyezz�k el,
    /// mert �gy az eg�sz struktogrammot k�nyelmesen lehet m�s helyre poz�cion�lni,
    /// valamint a k�pform�tumba val� elment�st is megk�nny�ti.
    /// </summary>
    [Serializable]
    public class Struktogram : Panel, ISerializable
    {

    #region V�ltoz�k, property-k, konstruktor, (plusz a hozz�juk tartoz� f�ggv�nyek)

        private bool showTitle;
        private int tabIndex, serNumber, tmpFileNumber, widthOfStm;
        private Element latestFocusedElement;
        private Font font;
        private Color fontColor;

        /// <summary>
        /// Megadja, vagy be�ll�tja a struktogram elemeiben haszn�lt fontot.
        /// </summary>
        public Font Font
        {
            get { return font; }
            set
            {
                font = value;
                treeWalkChangeFonts(this);
                correctBadSizes();
            }
        }

        /// <summary>
        /// Bej�rja a struktogrammot �s �t�ll�tja az elemek fontj�t.
        /// </summary>
        /// <param name="elementParent"> A m�dos�tand� elemek sz�l�je. </param>
        private void treeWalkChangeFonts(Control elementParent)
        {
            foreach (Element e in elementParent.Controls)
            {
                if (e.Controls.Count > 0)
                {
                    treeWalkChangeFonts(e);
                }
                e.Font = this.Font;

                // Ha �ppen deszerializ�lunk, akkor nem szabad lefutnia ennek a r�sznek
                // mert ekkor a m�retek, poz�ci�k a deszerializ�l�sb�l meghat�roz�dnak
                if (MainWindow.State != MainWindowState.Deserializing)
                {
                    Alignment(e, true);
                }
            }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja a struktogramban haszn�lt font sz�n�t.
        /// </summary>
        public Color FontColor
        {
            get { return fontColor; }
            set
            {
                fontColor = value;
                treeWalkChangeFontColor(this);
            }
        }

        /// <summary>
        /// Bej�rja a struktogrammot �s �t�ll�tja az elemek bet�sz�n�t.
        /// </summary>
        /// <param name="elementParent"> A m�dos�tand� elemek sz�l�je. </param>
        private void treeWalkChangeFontColor(Control elementParent)
        {
            foreach (Element e in elementParent.Controls)
            {
                if (e.Controls.Count > 0)
                {
                    treeWalkChangeFontColor(e);
                }
                e.ForeColor = this.FontColor;
            }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja, hogy a struktogramnak legyen-e c�me.
        /// </summary>
        public bool ShowTitle
        {
            get { return showTitle; }
            set
            {
                showTitle = this.Controls[0].Visible = value;
                if (MainWindow.State != MainWindowState.Deserializing)
                {
                    Alignment(this.Controls[1]);
                }
            }
        }

        /// <summary>
        /// �sszegy�jti �s visszaadja, vagy be�ll�tja az �sszes struktogram opci�t.
        /// </summary>
        public StruktogramOptions StruktogramOptions
        {
            get { return new StruktogramOptions(this.Font, this.FontColor, this.ShowTitle); }
            set
            {
                this.Font = value.Font;
                this.FontColor = value.FontColor;
                this.ShowTitle = value.ShowTitle;
            }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja, azt az elemet, melyen legutolj�ra volt a Focus.
        /// </summary>
        public Element LatestFocusedElement
        {
            get { return latestFocusedElement; }
            set { latestFocusedElement = value; }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja a struktogram sz�less�g�t.
        /// </summary>
        public int WidthOfStm
        {
            get { return widthOfStm; }
            set { widthOfStm = value; }
        }

        /// <summary>
        /// Megadja, vagy be�ll�tja annak az ideiglenes f�jlnak a sz�m�t,
        /// melyben a struktogram aktu�lis �llapot�t t�roljuk.
        /// </summary>
        public int TmpFileNumber
        {
            get { return tmpFileNumber; }
            set { tmpFileNumber = value; }
        }

        /// <summary>
        /// Megadja, hogy a struktogramban van-e kijel�lve elem.
        /// </summary>
        public bool HasSelectedElement
        {
            get { return SelectedElements.Count > 0; }
        }

        /// <summary>
        /// Megadja a kijel�lt elemek list�j�t.
        /// </summary>
        public List< Element > SelectedElements
        {
            get
            {
                List<Element> list = new List<Element>();

                treeWalkSelectedElementsDetermination(ref list, Controls);

                return list;
            }
        }

        /// <summary>
        /// Fabej�r�ssal rekurz�van bej�rja a struktogrammot, �s meghat�rozza a kijel�lt elemeket.
        /// </summary>
        /// <param name="list"> A lista, melybe a kijel�lt elemek ker�lnek. </param>
        /// <param name="elements"> A vizsg�land� elemek. </param>
        private void treeWalkSelectedElementsDetermination(ref List<Element> list, Control.ControlCollection elements)
        {
            // V�gigmegy�nk az elemeken
            foreach (Element e in elements)
            {
                // Itt h�vjuk meg rekurz�van a f�ggv�nyt, felt�ve, ha az
                // elemnek vannak gyerekei
                if (e.Controls.Count > 0)
                {
                    treeWalkSelectedElementsDetermination(ref list, e.Controls);
                }

                // Ha ki van jel�lve az elem, akkor bepakoljuk a list�ba
                if (e.Selected)
                {
                    list.Add(e);
                }
            }
        }

        /// <summary>
        /// A konstruktor v�gzi el az �j struktogram l�trehoz�s�t
        /// </summary>
        public Struktogram() : base()
        {
            createSurface();
            this.Show();
            this.GotFocus += new EventHandler(Struktogram_GotFocus);

            // A c�m hozz�ad�sa a struktogramhoz
            this.Controls.Add(new Title());
            this.Controls[0].Show();

            // Egy blokk hozz�ad�sa a struktogramhoz
            this.AddBlock(this,1,false);

            // Rendezz�k az eddigi struktogrammot, mely most k�t elemet tartalmaz :
            // A c�met, �s egy blokkot.
            Alignment(this.Controls[1]);
        }

        /// <summary>
        /// L�trehozza a struktogram fel�let�t
        /// </summary>
        private void createSurface()
        {
            this.Paint += new PaintEventHandler(Struktogram_Paint);
            this.MouseClick += new MouseEventHandler(Struktogram_MouseClick);

            this.Location = new Point(10, 5);
            // A Panel automatikusan n�lj�n �s menjen �ssze, ha sz�ks�ges
            this.AutoSize = true;
            this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.Margin = new Padding(0, 0, 0, 0);
            this.BorderStyle = BorderStyle.None;

            this.tmpFileNumber = 0;
        }

    #endregion

    #region Szerializ�l�s

        /// <summary>
        /// Ez a f�ggv�ny h�v�dik meg szerializ�l�skor.
        /// </summary>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            treeWalkGetObjectData(ref info, Controls);
        }

        /// <summary>
        /// Ez a f�ggv�ny hozza l�tre a SerializationInfo-t.
        /// </summary>
        /// <param name="elements"> Az elemek, melyeket a SerializationInfo-ba akarunk ki�rni. </param>
        private void treeWalkGetObjectData(ref SerializationInfo info, Control.ControlCollection elements)
        {
            if (elements[0].Parent is Struktogram)
            {
                info.AddValue("StmWidth", this.WidthOfStm, typeof(int));
                info.AddValue("Font", this.Font, typeof(Font));
                info.AddValue("FontColor", this.FontColor, typeof(Color));
                info.AddValue("ShowTitle", this.ShowTitle, typeof(bool));
                info.AddValue("ElementsCount_0", elements.Count, typeof(int));
                serNumber = 1;
            }

            for (int i = 0; i < elements.Count; ++i)
            {
                info.AddValue("Type_" + serNumber.ToString(), elements[i].GetType().ToString(), typeof(String));
                info.AddValue("Text_" + serNumber.ToString(), elements[i].Text, typeof(String));
                info.AddValue("Width_" + serNumber.ToString(), elements[i].Size.Width, typeof(int));
                info.AddValue("Height_" + serNumber.ToString(), elements[i].Size.Height, typeof(int));
                info.AddValue("LocationX_" + serNumber.ToString(), elements[i].Location.X, typeof(int));
                info.AddValue("LocationY_" + serNumber.ToString(), elements[i].Location.Y, typeof(int));

                switch (elements[i].GetType().ToString())
                {
                    case "K.Loop":
                    case "K.Branch":
                    case "K.SimpleBranch":
                    case "K.MultipleBranch":
                        info.AddValue("ElementsCount_" + serNumber.ToString(), elements[i].Controls.Count, typeof(int));
                        break;
                }

                ++serNumber;

                if (elements[i].Controls.Count > 0)
                {
                    treeWalkGetObjectData(ref info, elements[i].Controls);
                }
            }
        }

    #endregion

    #region Deszerializ�l�s

        /// <summary>
        /// A deszerializ�l� konstruktor.
        /// </summary>
        public Struktogram(SerializationInfo info, StreamingContext ctxt) : base()
        {
            createSurface();

            serNumber = 0;
            treeWalkConstructStruktogram(ref info, this);
            treeWalkDetermineRates(this);

            // A friss�t�s csak a c�m miatt kell, hogy kirajzol�djon a lekerek�tett sark� t�glalap
            Refresh();
            SaveState();
        }

        /// <summary>
        /// A SerializationInfo alapj�n fel�p�ti a struktogrammot.
        /// </summary>
        /// <param name="info"> A SerializationInfo, melyb�l az adatokat olvassuk. </param>
        /// <param name="elementParent"> Az elemek sz�l�je. </param>
        private void treeWalkConstructStruktogram(ref SerializationInfo info, Control elementParent)
        {
            // L�p�ssz�m meghat�roz�sa
            int steps = ((int)info.GetValue("ElementsCount_" + serNumber.ToString(), typeof(int)));

            ++serNumber;

            for (int i = 0; i < steps; ++i)
            {
                String type = ((String)info.GetValue("Type_" + serNumber.ToString(), typeof(String)));
                switch (type)
                {
                    case "K.Title":
                        createElementFromSerializationInfo(ref info, "Title", ref elementParent);
                        ++serNumber;
                        break;
                    case "K.Block":
                        createElementFromSerializationInfo(ref info, "Block", ref elementParent);
                        ++serNumber;
                        break;
                    case "K.Loop":
                        createElementFromSerializationInfo(ref info, "Loop", ref elementParent);
                        treeWalkConstructStruktogram(ref info, elementParent.Controls[i]);
                        break;
                    case "K.Branch":
                        createElementFromSerializationInfo(ref info, "Branch", ref elementParent);
                        treeWalkConstructStruktogram(ref info, elementParent.Controls[i]);
                        break;
                    case "K.SimpleBranch":
                        createElementFromSerializationInfo(ref info, "SimpleBranch", ref elementParent);
                        treeWalkConstructStruktogram(ref info, elementParent.Controls[i]);
                        break;
                    case "K.MultipleBranch":
                        createElementFromSerializationInfo(ref info, "MultipleBranch", ref elementParent);
                        treeWalkConstructStruktogram(ref info, elementParent.Controls[i]);
                        break;
                }
            }

            // A v�g�n kiolvassuk a struktogrammhoz tartoz� egy�b inform�ci�kat is
            if (elementParent is Struktogram)
            {
                this.WidthOfStm = (((int)info.GetValue("StmWidth", typeof(int))));

                Font font = ((Font)info.GetValue("Font", typeof(Font)));
                this.Font = new Font( font.FontFamily, font.Size, font.Style, font.Unit, 238);

                this.FontColor = ((Color)info.GetValue("FontColor", typeof(Color)));
                this.ShowTitle = ((bool)info.GetValue("ShowTitle", typeof(bool)));
            }
        }

        /// <summary>
        /// Az elemet l�trehozza a SerializationInfo alapj�n.
        /// </summary>
        /// <param name="elementParent"> A l�trehozand� elem sz�l�je. A t�pusa az�rt Control, mert ez a param�ter
        ///                              lehet maga a Struktogram is, ami pedig nem Element. </param>
        private void createElementFromSerializationInfo(ref SerializationInfo info, String type, ref Control elementParent)
        {
            String text = ((String)info.GetValue("Text_" + serNumber.ToString(), typeof(String)));
            Size size = new Size(((int)info.GetValue("Width_" + serNumber.ToString(), typeof(int))), ((int)info.GetValue("Height_" + serNumber.ToString(), typeof(int))));
            Point location = new Point(((int)info.GetValue("LocationX_" + serNumber.ToString(), typeof(int))), ((int)info.GetValue("LocationY_" + serNumber.ToString(), typeof(int))));

            switch (type)
            {
                case "Title": elementParent.Controls.Add(new Title(text, size, location)); elementParent.Controls[elementParent.Controls.Count - 1].Show(); break;
                case "Block": elementParent.Controls.Add(new Block(text, size, location)); elementParent.Controls[elementParent.Controls.Count - 1].Show(); break;
                case "Loop": elementParent.Controls.Add(new Loop(text, size, location)); elementParent.Controls[elementParent.Controls.Count - 1].Show(); break;
                case "Branch": elementParent.Controls.Add(new Branch(text, size, location)); elementParent.Controls[elementParent.Controls.Count - 1].Show(); break;
                case "SimpleBranch": elementParent.Controls.Add(new SimpleBranch(text, size, location)); elementParent.Controls[elementParent.Controls.Count - 1].Show(); break;
                case "MultipleBranch": elementParent.Controls.Add(new MultipleBranch(size, location)); elementParent.Controls[elementParent.Controls.Count - 1].Show(); break;
            }
        }

        /// <summary>
        /// A SerializationInfo-ban nem t�roljuk el az ar�ny�rt�keket, hanem ezzel a f�ggv�nnyel
        /// kisz�moltatjuk ezeket.
        /// </summary>
        /// <param name="elementParent"> Az elemek sz�l�je, melyeknek meg akarjuk hat�rozni az ar�ny�rt�keiket. </param>
        private void treeWalkDetermineRates(Control elementParent)
        {
            foreach (Element e in elementParent.Controls)
            {
                // Csak Loop �s SimpleBranch t�pus� sz�l�k eset�n l�nyeges a magass�gar�ny
                if (e.Parent is Loop || e.Parent is SimpleBranch)
                {
                    ((ProgramConstruction)e).HeightRate = Convert.ToDouble(e.Size.Height) / Convert.ToDouble(e.Parent.Size.Height - ((ProgramConstructionWithCondition)e.Parent).ConditionHeight);
                }
                // Csak SimpleBranch-nek van sz�less�gar�nya
                if (e is SimpleBranch)
                {
                    ((SimpleBranch)e).WidthRate = Convert.ToDouble(e.Size.Width) / Convert.ToDouble(e.Parent.Size.Width);
                }

                if (e.Controls.Count > 0)
                {
                    treeWalkDetermineRates(e);
                }
            }
        }

    #endregion

    #region Az Alignment met�dus, (plusz a hozz� tartoz� f�ggv�nyek)

        /// <summary>
        /// Ez az egyik legfontosabb f�ggv�ny az eg�sz programban.
        /// Feladata az, hogy meghat�rozza az �sszes elem m�ret�t �s hely�t a struktogramban.
        /// </summary>
        /// <param name="sender"> A k�ld� objektum, melynek megv�ltozott a m�rete. </param>
        public void Alignment(object sender)
        {
            Alignment(sender, false);
        }

        /// <summary>
        /// Ez az egyik legfontosabb f�ggv�ny az eg�sz programban.
        /// Feladata az, hogy meghat�rozza az �sszes elem m�ret�t �s hely�t a struktogramban.
        /// </summary>
        /// <param name="sender"> A k�ld� objektum, melynek megv�ltozott a m�rete. </param>
        /// <param name="resize"> �tm�retez�s miatt h�vjuk-e meg a f�ggv�nyt. �tm�retez�skor bizonyos k�dr�szeknek nem szabad lefutniuk. </param>
        public void Alignment(object sender, bool resize)
        {
            // Ebben az el�gaz�sban hat�roz�dnak meg a m�retek,
            // �s nem szeretn�nk, hogy a c�m m�ret�vel egy�tt v�ltozzon a t�bbi elem.
            // Ez�rt van itt ez az el�gaz�s.
            if ( !Equals(sender,null) && !((Element)sender).Equals(this.Controls[0]) )
            {
                Element element = ((Element)sender);

                // Ezek csak az�rt kellenek, hogy elker�lj�k, hogy bizonyos
                // esetekben kisz�mol�djon t�bbsz�r ugyanaz
                int parentConditionHeight;
                Size elementsSize, calculatedSize;
                double devider;

                // El�sz�r kifel� haladva megv�ltoztatjuk a sz�l�k m�reteit,
                // m�g a sz�l� nem lesz a Panel
                while ( !(element.Parent is Struktogram) )
                {
                    // Egy switch szerkezettel el�gazva hat�rozzuk meg a sz�l� objektumok m�reteit
                    switch(element.Parent.GetType().ToString())
                    {
                        // Ha a sz�l� ciklus
                        case "K.Loop" :
                            parentConditionHeight = ((Loop)element.Parent).ConditionHeight;
                            elementsSize = ((Loop)element.Parent).ElementsSize;

                            calculatedSize = new Size(resize ?                          // Sz�less�g kisz�m�t�sa
                                                            element.Size.Width + ((Loop)element.Parent).Font.Height + 8 :
                                                            elementsSize.Width + ((Loop)element.Parent).Font.Height + 8 < element.Parent.Size.Width ?
                                                                element.Parent.Size.Width :
                                                                elementsSize.Width + ((Loop)element.Parent).Font.Height + 8,
                                                            
                                                            // Magass�g kisz�m�t�sa
                                                            parentConditionHeight + elementsSize.Height);

                            element.Parent.Size = new Size(calculatedSize.Width, calculatedSize.Height);

                            devider = Convert.ToDouble(element.Parent.Size.Height - parentConditionHeight);
                            //Mev�ltoztatjuk a magass�g-ar�nyokat, �gy minden elem ugyanolyan m�ret� marad, kiv�ve amelyik megn�tt.
                            foreach (ProgramConstruction pc in element.Parent.Controls)
                            {
                                pc.HeightRate = Convert.ToDouble(pc.Size.Height) / devider;
                            }
                        break;

                        // Ha a sz�l� egy egyszer� el�gaz�s �g.
                        case "K.SimpleBranch":
                            parentConditionHeight = ((SimpleBranch)element.Parent).ConditionHeight;
                            elementsSize = ((SimpleBranch)element.Parent).ElementsSize;

                            calculatedSize = new Size( resize ?                 // Sz�less�g kisz�m�t�sa
                                                            element.Size.Width :
                                                            elementsSize.Width < element.Parent.Size.Width ?
                                                                element.Parent.Size.Width :
                                                                elementsSize.Width,

                                                       // Magass�g kisz�m�t�sa
                                                       resize || element.Parent.Size.Height < parentConditionHeight + elementsSize.Height ?
                                                            parentConditionHeight + elementsSize.Height :
                                                            element.Parent.Size.Height );

                            element.Parent.Size = new Size(calculatedSize.Width, calculatedSize.Height);

                            devider = Convert.ToDouble(element.Parent.Size.Height - parentConditionHeight);
                            // Itt is, ha ide jutott az algoritmus, akkor meg kell v�ltoztatni a magass�g-ar�nyokat.
                            foreach (ProgramConstruction pc in element.Parent.Controls)
                            {
                                pc.HeightRate = Convert.ToDouble(pc.Size.Height) / devider;
                            }
                        break;

                        // Ha a sz�l� egy el�gaz�s.
                        case "K.Branch":
                            element.Parent.Size = new Size( ((Branch)element.Parent).ElementsSize.Width, ((Branch)element.Parent).ConditionHeight + element.Size.Height );

                            // Mivel a Branch-en bel�l SimpleBranch-ek vannak, ez�rt ezek sz�less�g-ar�ny�t �t kell �ll�tanunk,
                            // pont �gy mint a magass�g-ar�nyok v�ltoz�sakor.
                            ((SimpleBranch)element.Parent.Controls[0]).WidthRate = Convert.ToDouble(element.Parent.Controls[0].Size.Width) / element.Parent.Size.Width;
                            ((SimpleBranch)element.Parent.Controls[1]).WidthRate = Convert.ToDouble(element.Parent.Controls[1].Size.Width) / element.Parent.Size.Width;
                        break;

                        // Ha a sz�l� egy t�bb�g� el�gaz�s.
                        case "K.MultipleBranch":
                            element.Parent.Size = new Size(((MultipleBranch)element.Parent).ElementsSize.Width, element.Size.Height);

                            // Mivel a MultipleBranch-en bel�l SimpleBranch-ek vannak, ez�rt ezek sz�less�g-ar�ny�t �t kell �ll�tanunk,
                            // pont �gy mint a magass�g-ar�nyok v�ltoz�sakor.
                            foreach (SimpleBranch sb in element.Parent.Controls)
                            {
                                sb.WidthRate = Convert.ToDouble(sb.Size.Width) / element.Parent.Size.Width;
                            }
                        break;
                    }

                    // Fentebb megy�nk egy szintet.
                    element = ((Element)element.Parent);
                }

                // A struktogram m�ret�t is �t�ll�tjuk, ha sz�ks�ges, elk�pzelhet�, hogy ez is megn�tt.
                if (resize)
                {
                    this.WidthOfStm = element.Size.Width;
                }
                else // A m�s esetekhez tartozik pl. az �j elem hozz�ad�sa.
                {
                    if (element.Size.Width > this.WidthOfStm)
                    {
                        this.WidthOfStm = element.Size.Width;
                    }
                }

                // Itt egy fabej�r�ssal bej�rjuk a struktogrammot.
                // Itt az elemek m�reteit hat�rozzuk meg a sz�l�k f�ggv�ny�ben,
                // ez�rt kellett az el�bb a sz�l�k m�ret�t v�ltoztatni kifel�!
                treeWalkSizeChange(this.Controls);
            }

            // Ha a c�m sz�less�ge a nagyobb, mint az els� elem sz�less�ge,
            // akkor a c�m vertik�lis k�z�pvonala seg�ts�g�vel elhelyezz�k az els� programkonstrukci�t.
            // A Panel k�z�pvonal�t nem �rdemes venni.
            if (this.Controls[0].Size.Width >= this.Controls[1].Size.Width)
            {
                this.Controls[0].Location = new Point(3, 10);
            }

            // Itt pedig egy fabej�r�ssal meghat�rozzuk az minden elem hely�t.
            treeWalkLocationChange(this.Controls);

            // Ha a c�m sz�less�ge kisebb, mint az els� programkonstrukci� sz�less�ge,
            // akkor eddigre m�r elhelyez�d�tt az els� programkonstrukci� (mivel a treeWalkLocationChange lefutott),
            // �s �gy a kisebb sz�less�g� c�met, az els� programkonstrukci� vertik�lis k�z�pvonala
            // seg�ts�g�vel m�r el tudjuk helyezni.
            if (this.Controls[0].Size.Width < this.Controls[1].Size.Width)
            {
                // Ha a c�m nagyobb,mint az els� elem
                this.Controls[0].Location = new Point((this.Controls[1].Location.X + this.Controls[1].Size.Width / 2) - this.Controls[0].Size.Width / 2, 10);
            }

            // A v�g�n nem �rt �jrarajzolni a Panelt, m�rcsak a c�m miatt is
            this.Refresh();

            // Ez annak �rdek�ben van itt, hogy a TabIndex-eket helyrehozza egy m�dos�t�s ut�n.
            // Teh�t, hogy a TAB gombbal sz�pen, egym�s ut�n lehessen l�pkedni az egyes
            // programkonstrukci�k k�z�tt.
            // Sz�val, hogy ne az legyen, hogy �ssze-vissza dob�lja a kurzort, a program,
            // a TAB gomb lenyom�s�ra.
            TabIndexCorrection(this);

            // Megjegyz�s : Esetleg a TabIndexCorrection f�ggv�nyt �sszeolvaszthatn�nk a treeWalkLocationChange
            // vagy esetleg a treeWalkSizeChange, de �gy olvashat�bb a k�d.
        }

        /// <summary>
        ///  Ez a f�ggv�ny bej�rja a struktogrammot, �s meghat�rozza az elemek M�RETEIT.
        /// </summary>
        /// <param name="elements"> Az �ppen m�dos�tand� elemek. </param>
        private void treeWalkSizeChange(Control.ControlCollection elements)
        {
            Size elementsSize;

            // V�gigmegy�nk az elemeken
            for (int i = 0; i < elements.Count; ++i)
            {
                if ( !(elements[i] is Title) ) //C�mmel itt nem kell foglalkoznunk
                {
                    // M�shogy kell elj�rnunk, ha a legfels� szinten vagyunk
                    // valamint ha pl. egy cikluson, vagy egy el�gaz�son bel�l vagyunk
                    // teh�t egy elem m�ret�t befoly�solja, hogy mi a sz�l�je
                    switch(elements[i].Parent.GetType().ToString())
                    {
                        // Ha az aktu�lis elem SZ�L�JE Panel
                        case "K.Struktogram" :
                            elements[i].Size = new Size(this.WidthOfStm, elements[i].Size.Height);
                        break;

                        // Ha az aktu�lis elem SZ�L�JE Loop
                        case "K.Loop" :
                            try{
                                elements[i].Size = new Size(elements[i].Parent.Size.Width - (((Loop)elements[i].Parent).Font.Height + 8), Convert.ToInt32(Math.Round((((Loop)elements[i].Parent).Size.Height - ((Loop)elements[i].Parent).ConditionHeight) * ((ProgramConstruction)elements[i]).HeightRate)));
                            } catch (Exception ex)
                            {
                                setElementRecommendedSize( ((Element)elements[i]) );
                            }

                            // Ha az utols� poz�ci�n l�v� elem m�rete kicsivel nagyobb,
                            // vagy kisebb mint kellene (ez az ar�nyos m�retmeghat�roz�s miatt elk�pzelhet�),
                            // akkor kis igaz�t�sra van sz�ks�g.
                            if (i == elements[i].Parent.Controls.Count - 1)
                            {
                                elementsSize = ((Loop)elements[i].Parent).ElementsSize;

                                if (elementsSize.Height != ((Loop)elements[i].Parent).Size.Height - ((Loop)elements[i].Parent).ConditionHeight)
                                {
                                    elements[i].Size = new Size(elements[i].Size.Width, elements[i].Size.Height + (((Loop)elements[i].Parent).Size.Height - ((Loop)elements[i].Parent).ConditionHeight - elementsSize.Height));
                                    ((ProgramConstruction)elements[i]).HeightRate = Convert.ToDouble(elements[i].Size.Height) / Convert.ToDouble(((Loop)elements[i].Parent).Size.Height - ((Loop)elements[i].Parent).ConditionHeight);
                                }
                            }
                        break;

                        // Ha az aktu�lis elem SZ�L�JE Branch
                        case "K.Branch":
                            try{
                                elements[i].Size = new Size(Convert.ToInt32(Math.Round((((Branch)elements[i].Parent).Size.Width) * ((SimpleBranch)elements[i]).WidthRate)), elements[i].Parent.Size.Height - ((Branch)elements[i].Parent).ConditionHeight);
                            }catch (Exception ex)
                            {
                                setElementRecommendedSize( ((Element)elements[i]) );
                            }

                            // Az utols� elem sz�less�g-ar�ny�nak korrig�l�sa, ha sz�ks�ges (Ugyan�gy, mint a magass�g-ar�nyn�l).
                            if (i == elements[i].Parent.Controls.Count - 1)
                            {
                                elementsSize = ((Branch)elements[i].Parent).ElementsSize;

                                if (elementsSize.Width != ((Branch)elements[i].Parent).Size.Width)
                                {
                                    elements[i].Size = new Size(elements[i].Size.Width + (((Branch)elements[i].Parent).Size.Width - elementsSize.Width), elements[i].Size.Height);
                                    ((SimpleBranch)elements[i]).WidthRate = Convert.ToDouble(elements[i].Size.Width) / Convert.ToDouble(((Branch)elements[i].Parent).Size.Width);
                                }
                            }
                        break;

                        // Ha az aktu�lis elem SZ�L�JE SimpleBranch
                        case "K.SimpleBranch" :
                            try{
                                elements[i].Size = new Size(elements[i].Parent.Size.Width, Convert.ToInt32(Math.Round((((SimpleBranch)elements[i].Parent).Size.Height - ((SimpleBranch)elements[i].Parent).ConditionHeight) * ((ProgramConstruction)elements[i]).HeightRate)) );
                            }catch (Exception ex)
                            {
                                setElementRecommendedSize( ((Element)elements[i]) );
                            }

                            // Az utols� elem magass�g-ar�ny�nak korrig�l�sa, ha sz�ks�ges. 
                            if (i == elements[i].Parent.Controls.Count - 1)
                            {
                                elementsSize = ((SimpleBranch)elements[i].Parent).ElementsSize;

                                if (elementsSize.Height != ((SimpleBranch)elements[i].Parent).Size.Height - ((SimpleBranch)elements[i].Parent).ConditionHeight)
                                {
                                    elements[i].Size = new Size(elements[i].Size.Width, elements[i].Size.Height + (((SimpleBranch)elements[i].Parent).Size.Height - ((SimpleBranch)elements[i].Parent).ConditionHeight - elementsSize.Height));
                                    ((ProgramConstruction)elements[i]).HeightRate = Convert.ToDouble(elements[i].Size.Height) / Convert.ToDouble(((SimpleBranch)elements[i].Parent).Size.Height - ((SimpleBranch)elements[i].Parent).ConditionHeight);
                                }
                            }
                        break;

                        // Ha az aktu�lis elem SZ�L�JE MultipleBranch.
                        case "K.MultipleBranch":
                            try{
                                elements[i].Size = new Size(Convert.ToInt32(Math.Round((((MultipleBranch)elements[i].Parent).Size.Width) * ((SimpleBranch)elements[i]).WidthRate)), elements[i].Parent.Size.Height);
                            } catch (Exception ex)
                            {
                                setElementRecommendedSize( ((Element)elements[i]) );
                            }

                            // Az utols� elem sz�less�g-ar�ny�nak korrig�l�sa, ha sz�ks�ges.
                            if (i == elements[i].Parent.Controls.Count - 1)
                            {
                                elementsSize = ((MultipleBranch)elements[i].Parent).ElementsSize;

                                if (elementsSize.Width != ((MultipleBranch)elements[i].Parent).Size.Width)
                                {
                                    elements[i].Size = new Size(elements[i].Size.Width + (((MultipleBranch)elements[i].Parent).Size.Width - elementsSize.Width), elements[i].Size.Height);
                                    ((SimpleBranch)elements[i]).WidthRate = Convert.ToDouble(elements[i].Size.Width) / Convert.ToDouble(((MultipleBranch)elements[i].Parent).Size.Width);
                                }
                            }
                        break;
                    }

                    //A struktogram bej�r�s�hoz rekurz�van megh�vogatjuk a f�ggv�nyt
                    if (elements[i].Controls.Count > 0) treeWalkSizeChange(elements[i].Controls);
                }
            }
        }

        /// <summary>
        /// Be�ll�tja az elem aj�nlott m�ret�t.
        /// </summary>
        /// <param name="element"> Az elem, melynek az aj�nlott m�ret�t be szeretn�nk �ll�tani. </param>
        private void setElementRecommendedSize(Element element)
        {
            // Kiv�tel dob�dhat bizonyos esetekben (az egyik �rt�k t�l kicsi vagy t�l nagy lesz)
            // Kiv�tel eset�n be�ll�tjuk az aj�nlott m�retet.
            Size elementRecommendedSize = element.RecommendedSize;

            element.Size = new Size(elementRecommendedSize.Width, elementRecommendedSize.Height);
        }

        /// <summary>
        /// Ez a f�ggv�ny bej�rja a struktogrammot, �s meghat�rozza az elemek HELYZETEIT.
        /// </summary>
        /// <param name="elements"> Az �ppen m�dos�tand� elemek. </param>
        private void treeWalkLocationChange(Control.ControlCollection elements)
        {
            // V�gigmegy�nk az elemeken.
            for (int i = 0; i < elements.Count; ++i)
            {
                if ( !(elements[i] is Title) ) //C�mmel itt nem kell foglalkoznunk
                {
                    //A bej�r�s el�r�s�hez rekurz�van megh�vogatjuk a treeWalkLocationChange f�ggv�nyt
                    if (elements[i].Controls.Count > 0) treeWalkLocationChange(elements[i].Controls);

                    //M�shogy kell elj�rnunk, ha a legfels� szinten vagyunk
                    //valamint ha pl. egy cikluson bel�l vagyunk
                    switch(elements[i].Parent.GetType().ToString())
                    {
                        // Mindig az els� elemeket poz�con�ljuk valaholva, �s ezut�n a t�bbi lentebb l�v� elemet
                        // az el�z�h�z ut�nra pakoljuk, �gy az els� elem ut�n l�v� elemeknek a hely�t m�r k�nnyen meghat�rozhatjuk
                        // az el�z� elem poz�ci�j�b�l.
                        case "K.Struktogram" :
                            if (i == 1) // Az els� elem elhelyez�se
                            {
                                if (showTitle)
                                {
                                    if (this.Controls[0].Size.Width >= this.Controls[1].Size.Width)
                                    {
                                        // Ha a c�m sz�less�ge nagyobb, akkor
                                        // a c�mhez k�pest kell elhelyezni az els� elemet
                                        elements[1].Location = new Point((this.Controls[0].Location.X + this.Controls[0].Size.Width / 2) - elements[1].Size.Width / 2, this.Controls[0].Location.Y + this.Controls[0].Size.Height + 15);
                                    }
                                    else
                                    {
                                        // Ha pedig a c�m sz�less�ge kisebb, mint az els� els� elemnek
                                        // akkor pedig ford�tva j�runk el
                                        elements[1].Location = new Point(3, this.Controls[0].Location.Y + this.Controls[0].Size.Height + 15);
                                    }
                                }
                                else
                                {
                                    elements[1].Location = new Point(3, 10);
                                }
                            }
                            else
                            {
                                elements[i].Location = new Point(elements[i - 1].Location.X, elements[i - 1].Location.Y + elements[i - 1].Size.Height - 1);
                            }
                        break;

                        // Az elem sz�l�je LOOP
                        case "K.Loop" :
                            if (i == 0)
                            {
                                elements[elements.Count - 1].Location = new Point(elements[elements.Count - 1].Parent.Size.Width - elements[elements.Count - 1].Size.Width, elements[elements.Count - 1].Parent.Size.Height - elements[elements.Count - 1].Size.Height);
                            }
                            else
                            {
                                elements[elements.Count - 1 - i].Location = new Point(elements[elements.Count - i].Location.X, elements[elements.Count - i].Location.Y - elements[elements.Count - 1 - i].Size.Height + 1);
                            }
                        break;

                        // Az elem sz�l�je BRANCH
                        case "K.Branch" :
                           if (i == 0)
                            {
                                elements[i].Location = new Point(0, (((Branch)elements[i].Parent).ConditionHeight));
                            }
                            else
                            {
                                elements[i].Location = new Point(elements[i - 1].Location.X + elements[i - 1].Size.Width - 1, elements[i - 1].Location.Y);
                            }
                        break;

                        // Az elem sz�l�je SIMPLEBRANCH
                        case "K.SimpleBranch":
                            if (i == 0)
                            {
                                elements[elements.Count - 1].Location = new Point(0, elements[elements.Count - 1].Parent.Size.Height - elements[elements.Count - 1].Size.Height);
                            }
                            else
                            {
                                elements[elements.Count - 1 - i].Location = new Point(elements[elements.Count - i].Location.X, elements[elements.Count - i].Location.Y - elements[elements.Count - 1 - i].Size.Height + 1);
                            }
                        break;

                        // Az elem sz�l�je MULTIPLEBRANCH
                        case "K.MultipleBranch":
                            if (i == 0)
                            {
                                elements[i].Location = new Point(0, 0);
                            }
                            else
                            {
                                elements[i].Location = new Point(elements[i-1].Location.X + elements[i-1].Size.Width - 1, 0);
                            }
                        break;
                    }
                }

            }
        }

        /// <summary>
        /// Ez a f�ggv�ny �ll�tja helyre a TabIndex-eket.
        /// </summary>
        /// <param name="control"> Az �ppen m�dos�tand� elemek. </param>
        private void TabIndexCorrection(Control control)
        {
            // Az elej�n lenull�zzuk a tabIndex �rt�k�t.
            if (control is Struktogram)
            {
                tabIndex = 0;
            }

            // V�gigmegy�nk az elemeket.
            for (int i = 0; i < control.Controls.Count; ++i)
            {
                // Vannak elemek, melyekbe nem szeretn�nk bel�pni (Pl. MultipleBranch),
                // de ezekn�l hamisra �ll�tjuk a TabStop v�ltoz� �rt�k�t, �gy ezekbe
                // nem fogunk belel�pni a TAB gombbal.
                control.Controls[i].TabIndex = tabIndex;
                ++tabIndex;

                // Rekurz�van megh�vogatjuk a f�ggv�nyt, �gy minden elemet bej�runk
                if (control.Controls[i].Controls.Count > 0)
                {
                    TabIndexCorrection(control.Controls[i]);
                }
            }
        }

    #endregion

    #region A lekerek�tett sark� t�glalap kirajzol�sa

        /// <summary>
        /// Ennek a Paint met�dusnak az a feladata,
        /// hogy kirajzolja a Struktogram c�me k�r� a t�glalapot, valamint a vonalat
        /// a c�m �s az els� programkonstrukci� k�z�tt.
        /// </summary>
        protected void Struktogram_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            // Nyilv�n csak akkor kell kirajzolnunk, ha a c�m l�tsz�dik
            if (showTitle)
            {
                drawRoundRect(g, new Pen(Color.Black), this.Controls[0].Location.X - 2, this.Controls[0].Location.Y - 4, this.Controls[0].Size.Width + 4, this.Controls[0].Size.Height + 7, 8);
                g.DrawLine(new Pen(Color.Black), new Point(this.Controls[1].Location.X + this.WidthOfStm / 2, this.Controls[0].Location.Y + this.Controls[0].Size.Height + 3), new Point(this.Controls[1].Location.X + this.WidthOfStm / 2, this.Controls[0].Location.Y + this.Controls[0].Size.Height + 14));
            }
        }

        /// <summary>
        /// A struktogram c�m�hez rajzolja ki a lekerek�tett sark� t�glalapot.
        /// </summary>
        private void drawRoundRect(Graphics g, Pen p, float x, float y, float width, float height, float radius)
        {
            GraphicsPath gp = new GraphicsPath();
            gp.AddLine(x + radius, y, x + width - (radius * 2), y); // Vonal
            gp.AddArc(x + width - (radius * 2), y, radius * 2, radius * 2, 270, 90); // Sarok
            gp.AddLine(x + width, y + radius, x + width, y + height - (radius * 2)); // Vonal
            gp.AddArc(x + width - (radius * 2), y + height - (radius * 2), radius * 2, radius * 2, 0, 90); // Sarok
            gp.AddLine(x + width - (radius * 2), y + height, x + radius, y + height); // Vonal
            gp.AddArc(x, y + height - (radius * 2), radius * 2, radius * 2, 90, 90); // Sarok
            gp.AddLine(x, y + height - (radius * 2), x, y + radius); // Vonal
            gp.AddArc(x, y, radius * 2, radius * 2, 180, 90); // Sarok
            gp.CloseFigure();
            g.DrawPath(p, gp);
            gp.Dispose();
        }

    #endregion

    #region Elem hozz�ad�s met�dusok

        /// <summary>
        /// A WhichHasFocus() f�ggv�ny haszn�lja :
        /// Rekurz�van meghat�rozza, hogy melyik elemen van a f�kusz,
        /// �s a lefut�s v�g�n a m�sodik param�terben kapott mutat� fogja megadni a f�kuszban l�v� elemet
        /// </summary>
        private void treeWalkFocusDetection(Control.ControlCollection elements, ref Element hasFocus)
        {
            foreach (Element e in elements)
            {
                if (e.Controls.Count > 0)
                {
                    treeWalkFocusDetection(e.Controls, ref hasFocus);
                }
                if (e.Focused)
                {
                    hasFocus = e;
                }
            }
        }

        /// <summary>
        /// Blokk besz�r�sa az �ppen f�kuszban l�v� elem ut�nra.
        /// </summary>
        public void AddBlock()
        {
            Element whichHasFocus = WhichHasFocus();

            // Ha a c�m van f�kuszban, akkor ne sz�rjunk be sehova
            if (whichHasFocus != null && !(whichHasFocus is Title))
            {
                // MultipleBranch t�pus� sz�l� eset�n m�gegy m�lys�get feljebb kell l�pn�nk, mint a t�bbi esetben
                if ( whichHasFocus.Parent is MultipleBranch )
                {
                    AddBlock(whichHasFocus.Parent.Parent, whichHasFocus.Parent.Parent.Controls.IndexOf(whichHasFocus.Parent) + 1);
                }
                else
                {
                    AddBlock(whichHasFocus.Parent, whichHasFocus.Parent.Controls.IndexOf(whichHasFocus) + 1);
                }
            }
        }

        /// <summary>
        /// Blokk besz�r�sa egy megadott pontra.
        /// </summary>
        /// <param name="elementParent"> Az sz�l� objektum, melyhez hozz� szeretn�nk adni a blokkot. </param>
        /// <param name="where"> A poz�ci�, ahova szeretn�nk tenni az elemet. </param>
        public void AddBlock(Control elementParent, int where)
        {
            AddBlock(elementParent, where, true);
        }

        /// <summary>
        /// Blokk besz�r�sa egy megadott pontra.
        /// </summary>
        /// <param name="elementParent"> Az sz�l� objektum, melyhez hozz� szeretn�nk adni a blokkot. </param>
        /// <param name="where"> A poz�ci�, ahova szeretn�nk tenni az elemet. </param>
        /// <param name="refresh"> Itt kell be�ll�tani, hogy �jraigaz�tsuk-e a struktogrammot az elem besz�r�sa ut�n. </param>
        public void AddBlock(Control elementParent, int where, bool refresh)
        {
            //Most a control-ok v�g�re sz�r�dik be a blokk...
            elementParent.Controls.Add(new Block());

            //... viszont ezzel a f�ggv�nnyel el�rj�k, hogy a neki megfelel� indexen foglaljon helyet
            //m�g a t�bbi elem lentebb cs�szik egy indexet
            elementParent.Controls.SetChildIndex(elementParent.Controls[elementParent.Controls.Count - 1], where);

            elementParent.Controls[where].ForeColor = this.FontColor;
            elementParent.Controls[where].Font = this.Font;
            elementParent.Controls[where].Show();
            elementParent.Controls[where].Focus();

            // �jrarendezz�k a struktogrammot, ha sz�ks�ges
            if (refresh)
            {
                Alignment(elementParent.Controls[where]);
                if (!Equals(Parent, null))
                {
                    SaveState();
                }
            }

            // Vissza�ll�tjuk az �llapotot "Normal"-ra
            MainWindow.State = MainWindowState.Normal;
        }

        /// <summary>
        /// Egy megadott Blokk fel�l�r�sa.
        /// </summary>
        /// <param name="elementParent"> Az sz�l� objektum, melyben fel�l szeretn�nk �rni a blokkot. </param>
        /// <param name="which"> A poz�ci�, melyet fel�l szeretn�nk �rni. </param>
        public void OverWriteBlock(Control elementParent, int which)
        {
            // T�bb�g� el�gaz�s besz�r�sakor, a megjelen� dial�gusablak elt�n�se ut�n
            // lenne �rdemes t�r�lni a blokkot.
            if (MainWindow.State != MainWindowState.MultipleBranchInsert)
            {
                elementParent.Controls.RemoveAt(which);
            }
            
            switch (MainWindow.State)
            {
                case MainWindowState.BlockInsert            : this.AddBlock(elementParent, which); break;
                case MainWindowState.LoopInsert             : this.AddLoop(elementParent, which); break;
                case MainWindowState.BranchInsert           : this.AddBranch(elementParent, which); break;
                case MainWindowState.MultipleBranchInsert   : this.AddMultipleBranch(elementParent, which, true); break;
            }
        }

        /// <summary>
        /// Ciklus besz�r�sa az �ppen f�kuszban l�v� elem ut�nra.
        /// </summary>
        public void AddLoop()
        {
            Element whichHasFocus = WhichHasFocus();

            // Ha a c�m van f�kuszban, akkor ne sz�rjunk be sehova
            if (whichHasFocus != null && !(whichHasFocus is Title))
            {
                if ( whichHasFocus.Parent is MultipleBranch )
                {
                    AddLoop(whichHasFocus.Parent.Parent, whichHasFocus.Parent.Parent.Controls.IndexOf(whichHasFocus.Parent) + 1);
                }
                else
                {
                    AddLoop(whichHasFocus.Parent, whichHasFocus.Parent.Controls.IndexOf(whichHasFocus) + 1);
                }
            }
        }

        /// <summary>
        /// Ciklus besz�r�sa egy megadott pontra.
        /// </summary>
        /// <param name="elementParent"> Az sz�l� objektum, melyhez hozz� szeretn�nk adni a ciklust. </param>
        /// <param name="where"> A poz�ci�, ahova szeretn�nk tenni az elemet. </param>
        public void AddLoop(Control elementParent, int where)
        {
            elementParent.Controls.Add(new Loop());
            elementParent.Controls.SetChildIndex(elementParent.Controls[elementParent.Controls.Count - 1], where);

            elementParent.Controls[where].ForeColor = this.FontColor;
            elementParent.Controls[where].Font = this.Font;
            elementParent.Controls[where].Show();

            //Egy blokk besz�r�sa a ciklusba
            AddBlock(elementParent.Controls[where], 0);

            elementParent.Controls[where].Focus();
        }

        /// <summary>
        /// El�gaz�s besz�r�sa az �ppen f�kuszban l�v� elem ut�nra.
        /// </summary>
        public void AddBranch()
        {
            Element whichHasFocus = WhichHasFocus();

            // Ha a c�m van f�kuszban, akkor ne sz�rjunk be sehova
            if (whichHasFocus != null && !(whichHasFocus is Title) )
            {
                if ( whichHasFocus.Parent is MultipleBranch )
                {
                    AddBranch(whichHasFocus.Parent.Parent, whichHasFocus.Parent.Parent.Controls.IndexOf(whichHasFocus.Parent) + 1);
                }
                else
                {
                    AddBranch(whichHasFocus.Parent, whichHasFocus.Parent.Controls.IndexOf(whichHasFocus) + 1);
                }
            }
        }

        /// <summary>
        /// El�gaz�s besz�r�sa egy megadott pontra.
        /// </summary>
        /// <param name="elementParent"> Az sz�l� objektum, melyhez hozz� szeretn�nk adni az el�gaz�st. </param>
        /// <param name="where"> A poz�ci�, ahova szeretn�nk tenni az elemet. </param>
        public void AddBranch(Control elementParent, int where)
        {
            elementParent.Controls.Add(new Branch());
            elementParent.Controls.SetChildIndex(elementParent.Controls[elementParent.Controls.Count - 1], where);

            elementParent.Controls[where].ForeColor = this.FontColor;
            elementParent.Controls[where].Font = this.Font;
            elementParent.Controls[where].Show();

            // Beletesz�nk az el�gaz�s k�t �g�ba 1-1 blokkot
            AddBlock(elementParent.Controls[where].Controls[0], 0, false);
            AddBlock(elementParent.Controls[where].Controls[1], 0, true);

            elementParent.Controls[where].Focus();
        }

        /// <summary>
        /// El�gaz�s besz�r�sa az �ppen f�kuszban l�v� elem ut�nra. 
        /// </summary>
        public void AddMultipleBranch()
        {
            Element whichHasFocus = WhichHasFocus();

            // Ha a c�m van f�kuszban, akkor ne sz�rjunk be sehova
            if (whichHasFocus != null && !(whichHasFocus is Title) )
            {
                if ( whichHasFocus.Parent is MultipleBranch )
                {
                    AddMultipleBranch(whichHasFocus.Parent.Parent, whichHasFocus.Parent.Parent.Controls.IndexOf(whichHasFocus.Parent) + 1);
                }
                else
                {
                    AddMultipleBranch(whichHasFocus.Parent, whichHasFocus.Parent.Controls.IndexOf(whichHasFocus) + 1);
                }
            }
        }

        /// <summary>
        /// T�bb�g� el�gaz�s besz�r�sa egy megadott pontra.
        /// </summary>
        /// <param name="elementParent"> Az sz�l� objektum, melyhez hozz� szeretn�nk adni az t�bb�g� el�gaz�st. </param>
        /// <param name="where"> A poz�ci�, ahova szeretn�nk tenni az elemet. </param>
        public void AddMultipleBranch(Control elementParent, int where)
        {
            AddMultipleBranch(elementParent, where, false);
        }

        /// <summary>
        /// T�bb�g� el�gaz�s besz�r�sa egy megadott pontra.
        /// </summary>
        /// <param name="elementParent"> Az sz�l� objektum, melyhez hozz� szeretn�nk adni az t�bb�g� el�gaz�st. </param>
        /// <param name="where"> A poz�ci�, ahova szeretn�nk tenni az elemet. </param>
        /// <param name="overwrite"> Fel�l�r�s eset�n itt t�r�lj�k a blokkot. </param>
        public void AddMultipleBranch(Control elementParent, int where, bool overwrite)
        {
            NumberOfBranchesDialog dialog = new NumberOfBranchesDialog();

            if (dialog.ShowDialog() == DialogResult.OK)
            {
                int branches = Convert.ToInt32(dialog.NumberOfBranches);

                // A dial�gus ablak elt�n�se ut�n t�r�lj�k az blokkot, ha sz�ks�ges.
                if (overwrite)
                {
                    elementParent.Controls.RemoveAt(where);
                }

                elementParent.Controls.Add(new MultipleBranch(branches));
                elementParent.Controls.SetChildIndex(elementParent.Controls[elementParent.Controls.Count - 1], where);
                elementParent.Controls[where].Show();

                for (int i = 0; i < branches - 1; ++i)
                {
                    elementParent.Controls[where].Controls[i].ForeColor = this.FontColor;
                    elementParent.Controls[where].Controls[i].Font = this.Font;
                    AddBlock(elementParent.Controls[where].Controls[i], 0, false);
                }

                elementParent.Controls[where].Controls[branches - 1].ForeColor = this.FontColor;
                elementParent.Controls[where].Controls[branches - 1].Font = this.Font;
                AddBlock(elementParent.Controls[where].Controls[branches - 1], 0, true);

                elementParent.Controls[where].Controls[0].Focus();
            }
        }

    #endregion

    #region Szerkeszt�s m�veletek

        /// <summary>
        /// Egy megadott elem �tpakol�sa, egy megadott helyre.
        /// </summary>
        /// <param name="elementParent"> A sz�l� melyre �t szeretn�nk pakolni az elemet. </param>
        /// <param name="what"> Az elemek list�ja melyet �t szeretn�nk pakolni. </param>
        /// <param name="where"> Az index, melyre �t szeretn�nk pakolni az elemet. </param>
        public void MoveElement(Control elementParent, List<Element> what, int where)
        {
            MoveElement(elementParent, what, where, false);
        }

        /// <summary>
        /// Egy megadott elem �tpakol�sa, egy megadott helyre.
        /// </summary>
        /// <param name="elementParent"> A sz�l� melyre �t szeretn�nk pakolni az elemet. </param>
        /// <param name="what"> Az elemek list�ja melyet �t szeretn�nk pakolni. </param>
        /// <param name="where"> Az index, melyre �t szeretn�nk pakolni az elemet. </param>
        /// <param name="overwrite"> Fel�l szeretn�nk-e �rni az index poz�ci�n l�v� elemet. </param>
        public void MoveElement(Control elementParent, List<Element> what, int where, bool overwrite)
        {
            // Bizonyos esetben kicsit m�shogy kell elj�rnunk
            if (Equals(elementParent, what[what.Count - 1].Parent) && elementParent.Controls.IndexOf(what[what.Count - 1]) < where)
            {
                --where;
            }

            // Kisebb ellen�rz�sek, hogy ne l�pj�nk t�l a minim�lis �s maxim�lis indexeken
            if (elementParent is Struktogram && where == 0)
            {
                where = 1;
            }
            else
            {
                if (where == -1)
                {               
                    where = 0;                   
                }
                else
                {
                    if (Equals(elementParent, what[what.Count - 1].Parent) && where >= elementParent.Controls.Count - 1)
                    {
                        where = elementParent.Controls.Count - 1;
                    }
                }
            }

            // Ha elfogyn�nak az elemek onnan ahonnan �tpakoljuk az elemek(et), akkor oda besz�runk egy blokkot
            if (what[what.Count - 1].Parent.Controls.Count == 1)
            {
                AddBlock(what[what.Count - 1].Parent, 0, false);
            }

            elementParent.Controls.Add(what[what.Count - 1]);

            // Fel�l�r�s eset�n fel�l�rjuk elt�ntetj�k a r�gi elemet
            if (overwrite)
            {
                elementParent.Controls.RemoveAt(where);
            }

            // �thelyez�s
            elementParent.Controls.SetChildIndex(elementParent.Controls[elementParent.Controls.Count - 1], where);
            elementParent.Controls[where].Show();

            // A kijel�l�seket elt�ntetj�k
            DeselectAll();
            MainWindow.State = MainWindowState.MoveMode;

            // V�g�l rendez�s �s �llapot elment�se
            Alignment(elementParent.Controls[where]);
            SaveState();
        }

        /// <summary>
        /// A megadott elem t�rl�se.
        /// </summary>
        /// <param name="element"> Az elem, melyet t�r�lni k�v�nunk. </param>
        public void RemoveElement(Element element)
        {
            // SimpleBranch eset�n a sz�l�t kell t�r�ln�nk
            if (element is SimpleBranch)
            {
                RemoveElement( ((Element)element.Parent) );
            }
            else
            {
                Control parent = element.Parent;
                ProgramConstruction tmpPc = null;
                bool parentHasNoElement = false;

                // Ha nem maradna egy elem sem a programkonstrukci�ban (�s nem egy blokkal van dolgunk),
                // akkor beletesz�nk az elembe egy blokkot.
                if ( (parent is Struktogram && parent.Controls.Count == 2) ||
                     (parent is Loop && parent.Controls.Count == 1) ||
                     (parent is SimpleBranch && parent.Controls.Count == 1))
                {
                    int i = parent.Controls.IndexOf(element);

                    AddBlock(parent, parent.Controls.IndexOf(element), false);
                    tmpPc = ((ProgramConstruction)parent.Controls[i]);
                    parentHasNoElement = true;
                }

                element.Parent.Controls.Remove(element);

                // V�g�l �jrarendezz�k a struktogrammot.
                if (!parentHasNoElement) // Ha a kivett elem volt az utols� a programkonstrukci�ban
                {
                    Alignment(parent.Controls[0]);
                }
                else // Ha volt m�s elem is
                {
                    Alignment(tmpPc);
                }

                // A rossz�l m�retezett elemek kijav�t�sa lehet nem kell ide, de ittmaradt a k�s�bbi fejleszt�sek sor�n.
                // Biztos, ami biztos alapon itthagytam, baj nem fog sz�rmazni bel�le.
                correctBadSizes();
            }
        }

        /// <summary>
        /// A kijel�lt elemek �rm�retez�se.
        /// </summary>
        /// <param name="elementsList"> Az �tm�retezend� elemek. </param>
        /// <param name="width"> A k�v�nt sz�less�g. </param>
        /// <param name="height"> A k�v�nt magass�g. </param>
        public void ResizeElements(List<Element> elementsList, int width, int height)
        {
            foreach (Element e in elementsList)
            {
                // A m�retbe�ll�t�s ut�n rendezz�k a struktogrammot
                e.Size = new Size(width, height);
                this.Alignment(e, true);
            }
            this.DeselectAll();

            // Ha lenne hib�san m�retezett elem, akkor azt kijav�tjuk, �s v�g�l elmentj�k az �llapotot
            correctBadSizes();
            SaveState();
        }

        /// <summary>
        /// Be�ll�tja a struktogramnak az optim�lis m�ret�t.
        /// </summary>
        public void SetOptimalSize()
        {
            treeWalkSetOptimalSize(this);

            // A v�g�n helyre�ll�tjuk a rossz�l m�retezett elemeket, �s elmentj�k az �llapotot
            correctBadSizes();
            SaveState();
        }

        /// <summary>
        /// Rekurz�van bej�rja a struktogrammot, �s be�ll�tja minden elem m�ret�t az aj�nlott m�retre.
        /// </summary>
        /// <param name="elementParent"> A m�dos�tand� elemek sz�l�je. </param>
        private void treeWalkSetOptimalSize(Control elementParent)
        {
            foreach (Element e in elementParent.Controls)
            {
                if (e.Controls.Count > 0)
                {
                    treeWalkSetOptimalSize(e);
                }
                e.Size = e.RecommendedSize;
                Alignment(e,true);
            }
        }
   
        /// <summary>
        /// Kijav�tja a rossz�l m�retezett elemeket.
        /// </summary>
        protected void correctBadSizes()
        {
            if (MainWindow.State != MainWindowState.Deserializing)
            {
                // Az rendez�s ut�n meghat�rozzuk, hogy lett- rosszul m�retezett elem
                List<Element> badSizedElementsList = this.badSizedElements;

                // Ha vannak rossz�l m�retezett elemek, akkor a m�reteiket �t�ll�tjuk
                // az aj�nlott m�retre
                if (badSizedElementsList.Count > 0)
                {
                    foreach (ProgramConstruction pc in badSizedElementsList)
                    {
                        Size pcOldSize = pc.Size, recommendedSize = pc.RecommendedSize;

                        if (pc.Width < recommendedSize.Width)
                        {
                            pc.Size = new Size(recommendedSize.Width, pc.Size.Height);
                        }
                        if (pc.Height < recommendedSize.Height)
                        {
                            pc.Size = new Size(pc.Size.Width, recommendedSize.Height);
                        }

                        // V�g�l, ha v�ltoz�s t�rt�nt, akkor �jrarendezz�k a struktogrammot.
                        if (pcOldSize != pc.Size)
                        {
                            Alignment(pc, true);
                        }
                    }

                }
            }
        }

        /// <summary>
        /// Megadja a rosszul m�retezett elemek list�j�t.
        /// </summary>
        protected List<Element> badSizedElements
        {
            get
            {
                List<Element> list = new List<Element>();

                treeWalkBadSizedElementsDetermination(ref list, Controls);

                return list;
            }
        }

        /// <summary>
        /// Fabej�r�ssal rekurz�van bej�rja a struktogrammot, �s meghat�rozza a rosszul m�retezett elemeket.
        /// </summary>
        /// <param name="list"> A lista, melybe a rosszul m�retezett elemek ker�lnek. </param>
        /// <param name="elements"> A vizsg�land� elemek. </param>
        private void treeWalkBadSizedElementsDetermination(ref List<Element> list, Control.ControlCollection elements)
        {
            Size recommendedSize;

            // V�gigmegy�nk az elemeken
            foreach (Element e in elements)
            {
                if (!(e is Title)) // A c�mmel nem kell foglalkoznunk
                {
                    // Itt h�v�dik meg rekurz�van a f�ggv�ny, felt�ve, ha a vizsg�lt elemnek
                    // vannak gyerekei
                    if (e.Controls.Count > 0)
                    {
                        treeWalkBadSizedElementsDetermination(ref list, e.Controls);
                    }

                    recommendedSize = e.RecommendedSize;

                    // Ha nem megfelel� a m�ret, akkor beletessz�k a list�ba
                    if (e.Size.Width < recommendedSize.Width || e.Size.Height < recommendedSize.Height)
                    {
                        list.Add(e);
                    }
                }
            }
        }

    #endregion

    #region Aktu�lis �llapot elment�se, (plusz NumberOfSavedStates met�dus)

        /// <summary>
        /// Elmenti a struktogram aktu�lis �llapot�t.
        /// </summary>
        public void SaveState()
        {
            try
            {
                if (File.Exists(((StruktogramPage)Parent).TmpFilesDirectory + "\\" + Convert.ToString(1)))
                {
                    ((StruktogramPage)Parent).Modified = true;
                }
                ++TmpFileNumber;

                SaveState(((StruktogramPage)Parent).TmpFilesDirectory + "\\" + TmpFileNumber.ToString());

                if (File.Exists(((StruktogramPage)Parent).TmpFilesDirectory + "\\" + Convert.ToString(TmpFileNumber + 1)))
                {
                    for (int i = TmpFileNumber + 1; i <= MainWindow.NumberOfPossibleSavedStates && File.Exists(((StruktogramPage)Parent).TmpFilesDirectory + "\\" + i.ToString()); ++i)
                    {
                        File.Delete(((StruktogramPage)Parent).TmpFilesDirectory + "\\" + i.ToString());
                    }
                }

                if (TmpFileNumber > MainWindow.NumberOfPossibleSavedStates)
                {
                    File.Delete(((StruktogramPage)Parent).TmpFilesDirectory + "\\" + Convert.ToString(1));
                    for (int i = 2; i <= MainWindow.NumberOfPossibleSavedStates + 1; ++i)
                    {
                        File.Move(((StruktogramPage)Parent).TmpFilesDirectory + "\\" + i.ToString(), ((StruktogramPage)Parent).TmpFilesDirectory + "\\" + Convert.ToString(i - 1));
                    }

                    TmpFileNumber = MainWindow.NumberOfPossibleSavedStates;
                }
            }
            catch (Exception ex) { }
        }

        /// <summary>
        /// Elmenti a struktogram aktu�lis �llapot�t a megadott f�jlba.
        /// </summary>
        /// <param name="path"> A f�jl melybe el szeretn�nk menteni az �llapotot. </param>
        public void SaveState(String path)
        {
            try
            {
                Stream s = File.OpenWrite(path);
                BinaryFormatter bf = new BinaryFormatter();

                bf.Serialize(s, this);
                s.Close();
            }
            catch (System.IO.IOException ex) { }
        }

        /// <summary>
        /// Megadja, hogy a struktogram h�ny, el�z� �llapota lett eddig elmentve.
        /// </summary>
        public int NumberOfSavedStates
        {
            get
            {
                int counter = 0;

                while (true)
                {
                    if (File.Exists(((StruktogramPage)Parent).TmpFilesDirectory + "\\" + Convert.ToString(counter + 1)))
                    {
                        ++counter;
                    }
                    else
                    {
                        break;
                    }
                }
                return counter;
            }
        }

    #endregion

    #region Egy�b met�dusok : GotFocus, MouseClick esem�nykezel�k �s WhichHasFocus, DeselectAll f�ggv�nyek

        /// <summary>
        /// Fokusz�l�s eset�n, fokuszba helyezi a MainWindow-ot.
        /// (Erre az�rt van sz�ks�g, hogy egy helyen kezelj�k le ugyanazokat a dolgokat.)
        /// </summary>
        private void Struktogram_GotFocus(object sender, EventArgs e)
        {
            ((MainWindow)((TabControl)((StruktogramPage)this.Parent).Parent).Parent).Focus();
        }

        /// <summary>
        /// Klikkel�s eset�n vissz�ll�tja az �llapotot "Normal"-ra.
        /// </summary>
        private void Struktogram_MouseClick(object sender, MouseEventArgs e)
        {
            MainWindow.State = MainWindowState.Normal;
            DeselectAll();
        }

        /// <summary>
        /// Megadja, hogy melyik elemen van a f�kusz.
        /// </summary>
        /// <returns> A f�kuszban l�v� elem. </returns>
        public Element WhichHasFocus()
        {
            Element hasFocus = null;

            treeWalkFocusDetection(this.Controls, ref hasFocus);

            return hasFocus;
        }

        /// <summary>
        /// Az �sszes elem kijel�l�s�t visszavonja.
        /// </summary>
        public void DeselectAll()
        {
            if (SelectedElements.Count > 0)
            {
                while (this.SelectedElements.Count > 0)
                {
                    ((ProgramConstruction)this.SelectedElements[0]).Deselect();
                }
            }
        }

    #endregion

    }
}
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace K
{
    /// <summary>
    /// A Ciklus programkonstrukci�t val�s�tja meg ez az oszt�ly.
    /// </summary>
    public class Loop : ProgramConstructionWithCondition
    {

    #region Konstruktorok

        /// <summary>
        /// A Loop oszt�ly konstruktora.
        /// </summary>
        public Loop() : base() { }

        /// <summary>
        /// Ezt a konstruktor szerializ�l�skor h�v�dik meg.
        /// </summary>
        /// <param name="text"> Az elem kezd� Text-je. </param>
        /// <param name="size"> Az elem kezd�m�rete. </param>
        /// <param name="location"> Az elem kezd�poz�ci�ja. </param>
        public Loop(String text, Size size, Point location) : base(text, size, location) { }

    #endregion

    #region A z�ld sz�n� t�glalapok megjelen�t�s�vel kapcsolatos met�dusok, property-k

        /// <summary>
        /// Kirajzolja a z�ld t�glalapokat, ha sz�ks�ges.
        /// </summary>
        public override void RepaintRectangles(Graphics g)
        {
            // Ezzekkel a logikai v�ltoz�kkal jelezz�k, hogy al�l vagy fel�l van-e jelz�s
            upRect = downRect = false;

            //El�sz�r �jrarajzoljuk a vez�rl�t
            this.Refresh();

            // Fels�, z�ld sz�n� jelz�s, mely a besz�r�sn�l jelzi, hogy �ppen hova sz�r�dna be a kiv�lasztott elem
            if (MainWindow.State != MainWindowState.Normal && !Leaving && CursorInTheUpperRegion)
            {
                g.DrawLine(new Pen(rectColor), new Point(2, 2), new Point(2, 9));
                g.DrawLine(new Pen(rectColor), new Point(2, 2), new Point(this.Size.Width - 3, 2));
                g.DrawLine(new Pen(rectColor), new Point(this.Size.Width - 3, 2), new Point(this.Size.Width - 3, 9));

                upRect = true;
            }

            // Als�, z�ld sz�n� jelz�s, mely a besz�r�sn�l jelzi, hogy �ppen hova sz�r�dna be a kiv�lasztott elem
            if (MainWindow.State != MainWindowState.Normal && !Leaving && CursorInTheLowerRegion)
            {
                g.DrawLine(new Pen(rectColor), new Point(2, this.Size.Height - 9), new Point(2, this.Size.Height - 3));
                g.DrawLine(new Pen(rectColor), new Point(2, this.Size.Height - 3), new Point(this.Size.Width - this.Controls[this.Controls.Count - 1].Size.Width - 2, this.Size.Height - 3));
                g.DrawLine(new Pen(rectColor), new Point(this.Size.Width - this.Controls[this.Controls.Count - 1].Size.Width - 2, this.Size.Height - 3), new Point(this.Size.Width - this.Controls[this.Controls.Count - 1].Size.Width - 2, this.Size.Height - 9));

                downRect = true;
            }

            Leaving = false;
        }

        /// <summary>
        /// Megadja, hogy a kurzor, az elem tetej�n (FELS� r�sz�ben) van-e.
        /// </summary>
        public override bool CursorInTheUpperRegion
        {
            get
            {
                Point absPos = this.PointToClient(Cursor.Position);
                return ((absPos.X >= 1) && (absPos.X <= this.Size.Width - 1)) &&
                       ((absPos.Y >= -6) && (absPos.Y <= 12));
            }
        }

        /// <summary>
        /// Megadja, hogy a kurzor, az elem alj�n (ALS� r�sz�ben) van-e.
        /// </summary>
        public override bool CursorInTheLowerRegion
        {
            get
            {
                Point absPos = this.PointToClient(Cursor.Position);
                return ((absPos.X >= 1) && (absPos.X <= this.Size.Width - 1)) &&
                       ((absPos.Y >= this.Size.Height - 12) && (absPos.Y <= this.Size.Height + 6));
            }
        }

    #endregion

    #region M�retet megad� met�dusok

        /// <summary>
        /// Megadja a programkonstrukci� aj�nlott m�ret�t.
        /// </summary>
        public override Size RecommendedSize
        {
            get
            {
                int recommendedHeight = 0, recommendedWidth = 0, minWidth, prefAndMargWidth, calculatedWidth;
                Size recommendedSize;

                for (int i = 0; i < this.Controls.Count; ++i)
                {
                    recommendedSize = ((ProgramConstruction)this.Controls[i]).RecommendedSize;

                    // Mivel �ssze�rnek a programkonstrukci�k, ez�rt a hat�rol� fekete vonalakat
                    // k�tszer sz�moljuk bele, ez�rt vonunk ki egyet, minden alkalommal.
                    recommendedHeight += recommendedSize.Height - 1;

                    if (recommendedWidth < recommendedSize.Width)
                    {
                        recommendedWidth = recommendedSize.Height;
                    }
                }

                // Az utols� elem nem �rintkezik lefele semmivel, ez�rt ebb�l nem szabad kivonnunk 1-et.
                recommendedHeight += 1;
                recommendedWidth += this.FontHeight + 8;

                // A minim�lis sz�less�g
                minWidth = Convert.ToInt32(this.Font.Size) * 10 + this.FontHeight + 8;

                // A sz�m�tott sz�less�g
                prefAndMargWidth = this.PreferredSize.Width + LeftRightMargin;
                calculatedWidth = prefAndMargWidth < recommendedWidth ?
                                  recommendedWidth : prefAndMargWidth;

                // A PrefferedSize a sz�veg alapj�n adja meg az aktu�lis m�retet.
                // Ha ez nagyobb, akkor ezzel a sz�less�ggel t�r�nk vissza, ha pedig az
                // elemek aj�nlott m�retei a nagyobbak akkor azzal.
                return new Size(calculatedWidth < minWidth ? minWidth : calculatedWidth, this.ConditionHeight + recommendedHeight);

            }
        }

    #endregion

    }

}
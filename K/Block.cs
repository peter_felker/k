using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace K
{
    /// <summary>
    /// A Blokkot megval�s�t� oszt�ly
    /// </summary>
    public class Block : ProgramConstruction
    {

    #region Konstruktorok, (plusz a hozz�juk tartoz� f�ggv�nyek)

        /// <summary>
        /// A Block oszt�ly konstruktora.
        /// </summary>
        public Block(): base()
        {
            defaultProperties();
        }

        /// <summary>
        /// Ezt a konstruktor szerializ�l�skor h�v�dik meg.
        /// </summary>
        /// <param name="text"> Az elem kezd� Text-je. </param>
        /// <param name="size"> Az elem kezd�m�rete. </param>
        /// <param name="location"> Az elem kezd�poz�ci�ja. </param>
        public Block(String text, Size size, Point location) : base(text, size, location)
        {
            defaultProperties();
        }

        /// <summary>
        /// A konstruktorok k�z�s tulajdons�gait �ll�tja be ez az elj�r�s.
        /// </summary>
        private void defaultProperties()
        {
            this.TextChanged += new EventHandler(Block_TextChanged);
        }

    #endregion

    #region A z�ld sz�n� t�glalapok megjelen�t�s�vel kapcsolatos met�dusok, property-k

        /// <summary>
        /// Megadja, hogy a kurzor, az elem tetej�n (FELS� r�sz�ben) van-e.
        /// </summary>
        public override bool CursorInTheUpperRegion
        {
            get
            {
                Point absPos = this.PointToClient(Cursor.Position);
                return (((absPos.X >= 1) && (absPos.X <= this.Size.Width - 1)) &&
                        ((absPos.Y >= -6) && (absPos.Y <= this.Size.Height - 8)));
            }
        }

        /// <summary>
        /// Megadja, hogy a kurzor, az elem alj�n (ALS� r�sz�ben) van-e.
        /// </summary>
        public override bool CursorInTheLowerRegion
        {
            get
            {
                Point absPos = this.PointToClient(Cursor.Position);
                return (((absPos.X >= 1) && (absPos.X <= this.Size.Width - 1)) &&
                         ((absPos.Y >= 8) && (absPos.Y <= this.Size.Height + 6)));
            }
        }

        /// <summary>
        /// Kirajzolja a z�ld t�glalapokat, ha sz�ks�ges.
        /// </summary>
        public override void RepaintRectangles(Graphics g)
        {
            // Ezzekkel a logikai v�ltoz�kkal jelezz�k, hogy al�l vagy fel�l van-e jelz�s
            upRect = downRect = false;

            //El�sz�r �jrarajzoljuk a vez�rl�t
            this.Refresh();
                
            // Fels�, z�ld sz�n� jelz�s, mely a besz�r�sn�l jelzi, hogy �ppen hova sz�r�dna be a kiv�lasztott elem
            if (MainWindow.State != MainWindowState.Normal && !Leaving && CursorInTheUpperRegion)
            {
                g.DrawLine(new Pen(rectColor), new Point(2, 2), new Point(2, 9));
                g.DrawLine(new Pen(rectColor), new Point(2, 2), new Point(this.Size.Width - 3, 2));
                g.DrawLine(new Pen(rectColor), new Point(this.Size.Width - 3, 2), new Point(this.Size.Width - 3, 9));

                upRect = true;
             }

             // Als�, z�ld sz�n� jelz�s, mely a besz�r�sn�l jelzi, hogy �ppen hova sz�r�dna be a kiv�lasztott elem
             if (MainWindow.State != MainWindowState.Normal && !Leaving && CursorInTheLowerRegion)
             {
                g.DrawLine(new Pen(rectColor), new Point(2, this.Size.Height - 9), new Point(2, this.Size.Height - 3));
                g.DrawLine(new Pen(rectColor), new Point(2, this.Size.Height - 3), new Point(this.Size.Width - 3, this.Size.Height - 3));
                g.DrawLine(new Pen(rectColor), new Point(this.Size.Width - 3, this.Size.Height - 3), new Point(this.Size.Width - 3, this.Size.Height - 9));

                downRect = true;
             }

             Leaving = false;
        }

    #endregion

    #region M�retet megad� met�dusok

        /// <summary>
        /// Megadja a block aj�nlott m�ret�t.
        /// </summary>
        public override Size RecommendedSize
        {
            get
            {
                Size preferredSize = this.PreferredSize;
                int multiplier = (Parent is Loop || Parent is Struktogram) ? 10 : 6,
                    prefAndMargWidth = preferredSize.Width + LeftRightMargin,
                    minWidth = Convert.ToInt32(this.Font.Size) * multiplier;

                return new Size(prefAndMargWidth < minWidth ?
                                 minWidth : prefAndMargWidth,
                                 preferredSize.Height);
            }
        }

    #endregion

    #region Egy�b esem�nykezel�k : A ProgramConstruction_MouseClick fel�ldefini�l�sa, �s TextChanged

        /// <summary>
        /// Ha az aktu�lis blokkra r�klikkel�nk, akkor ez az esem�nykezel� fut let.
        /// Blokk eset�n elk�pzelhet�, hogy fel�l kell �rnunk a blokkot.
        /// </summary>
        protected override void ProgramConstruction_MouseClick(object sender, MouseEventArgs e)
        {
            if (MainWindow.State != MainWindowState.Normal)
            {
                if (UpRect && DownRect)  //Ekkor fel�l�r�dik az aktu�lis blokk
                {

                    switch (MainWindow.State)
                    {
                        case MainWindowState.BlockInsert: MainWindow.Stm.OverWriteBlock(Parent, Parent.Controls.IndexOf(this)); break;
                        case MainWindowState.LoopInsert: MainWindow.Stm.OverWriteBlock(Parent, Parent.Controls.IndexOf(this)); break;
                        case MainWindowState.BranchInsert: MainWindow.Stm.OverWriteBlock(Parent,Parent.Controls.IndexOf(this)); break;
                        case MainWindowState.MultipleBranchInsert: MainWindow.Stm.OverWriteBlock(Parent, Parent.Controls.IndexOf(this)); break;
                        case MainWindowState.Moving: MainWindow.Stm.MoveElement(Parent, MainWindow.Stm.SelectedElements, Parent.Controls.IndexOf(this), true); break;
                    }
                }
                else   //Minden m�s esetben besz�r�dik a megadott pontra
                {
                    base.ProgramConstruction_MouseClick(sender, e);
                }
            }
            else
            {
                base.ProgramConstruction_MouseClick(sender, e);
            }

        }

        /// <summary>
        /// Ha megv�ltozik a Text v�ltoz� tartalma, �s nem f�rne bele a sz�veg
        /// az aktu�lis helyre, akkor megn�veli a m�retet.
        /// </summary>
        protected void Block_TextChanged(object sender, EventArgs e)
        {
            Size oldSize = this.Size;
            Size preferredSize = this.PreferredSize;
            int prefAndMargWidth = preferredSize.Width + LeftRightMargin;

            this.Size = new Size(prefAndMargWidth > this.Size.Width ? prefAndMargWidth : this.Size.Width,
                                 preferredSize.Height > this.Size.Height ? preferredSize.Height : this.Size.Height);

            // Ha m�retv�ltoz�s t�rt�nt, akkor �jrarendezz�k a struktogrammot
            if (oldSize != this.Size)
            {
                MainWindow.Stm.Alignment(this, true);
            }

            MainWindow.Stm.SaveState();
        }

    #endregion

    }
}